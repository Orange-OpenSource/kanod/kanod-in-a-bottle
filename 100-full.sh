#! /bin/bash

#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

export KANOD_USER=admin
export KANOD_PASSWORD=secret

from=0

while getopts "hf:?" opt; do
    case "$opt" in
    h|\?)
        echo "$0 [-h][-f n]"
        echo '  -h this help'
        echo '  -f n starts from step n.'
        exit 0
        ;;
    f)
        from="${OPTARG}"
	shift;shift;
    esac
done


if [ -v no_proxy ]; then
    no_proxy=192.168.133.0/24,.kanod.home.arpa,$no_proxy
fi

if [ -v NO_PROXY ]; then
    NO_PROXY=192.168.133.0/24,.kanod.home.arpa,$NO_PROXY
fi

export PATH="${HOME}/.local/bin:$PATH"

print_info() {
    echo "################### STEP starting at $(date) ########"
    echo "$1"
}

if [ "$from " -le 0 ]; then
    print_info 'Template generation'
    time ./00-mk-templates.sh "${@}"
fi

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

if [ "$from " -le 1 ]; then
    print_info 'Base projects'
    time ./01-basic-projects.sh
fi
if [ "$from " -le 2 ]; then
    print_info 'Networks'
    time ./02-mk-network.sh
fi
if [ "$from " -le 3 ]; then
    print_info 'Stores'
    time ./03-mk-store.sh
fi
if [ "$from " -le 4 ]; then
    print_info 'Data center emulation'
    time ./04-minions.sh -s
fi
if [ "$from " -le 5 ]; then
    print_info 'BMC emulation'
    time ./05-bmc.sh
fi
if [ "$from " -le 6 ]; then
    print_info 'Service'
    time ./06-launch-service.sh -s
fi
if [ "$from " -le 7 ]; then
    print_info 'Life-Cycle Manager'
    time ./07-launch-lcm.sh -s
fi

if [ "$from " -le 8 ]; then
    print_info 'Broker VM'
    time ./08-launch-brokerdef.sh -s
fi

# 09-netdef incorporated in bmh

if [ "$from " -le 10 ]; then
    print_info 'Bare-metal Hosts'
    time ./10-bmh.sh
fi

if [ "$from " -le 11 ]; then
    print_info 'Meta project'
    time ./11-meta.sh
fi
if [ "$from " -le 12 ]; then
    print_info 'Cluster creation'
    time ./12-cluster.sh
fi

if [ "$from " -le 13 ]; then
    print_info 'BYOH hosts'
    time ./13-byohost.sh
fi

if [ "$from " -le 14 ]; then
    print_info 'Final synchronization'
    time ./wait.sh
    echo 'Result'
    ./view-deployment.sh -l -n -b
fi

if [ "${PIVOT_ENABLED:-}" == 1 ]; then
    if [ "$from " -le 20 ]; then
        print_info 'Pivoting'
        time ./30-pivot-cluster.sh
        echo 'Result after pivoting'
        ./view-deployment.sh -l -n -t -b
    fi
fi


if [ "${K8S_UPGRADE_ENABLED:-}" == 1 ]; then
    if [ "$from " -le 21 ]; then
        print_info 'Upgrading kubernetes version for target cluster'
        time ./31-upgrade-k8s.sh
        echo 'Result after upgrading target cluster'
        if [ "${PIVOT_ENABLED:-}" == 1 ]; then
            ./view-deployment.sh -l -n -t -b
        else
            ./view-deployment.sh -l -n
        fi
    fi
fi


if [ "${SCALING_ENABLED:-}" == 1 ]; then
    if [ "$from " -le 22 ]; then
        print_info 'Scaling target cluster'
        time ./32-scale-cluster.sh
        echo 'Result after scaling target cluster'
        if [ "${PIVOT_ENABLED:-}" == 1 ]; then
            ./view-deployment.sh -l -n -t -b
        else
            ./view-deployment.sh -l -n
        fi
    fi
fi
