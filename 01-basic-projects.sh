#!/bin/bash
set -eu
#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a
export PATH="$HOME/.local/bin:$PATH"
export DIB_REGISTRY_MIRROR="${REGISTRY_MIRROR:-}"
unset TOOLING_REGISTRY

mkdir -p "${LAB_DIR}/build/logs"
if ! [ -d "${LAB_DIR}/build/kanod-boot" ]; then
    echo '- Building kanod-boot'
    git clone "${GIT_URL}/kanod-boot" "${LAB_DIR}/build/kanod-boot"
    (
        cd "${LAB_DIR}/build/kanod-boot"
        git checkout "$KANOD_BOOT_REF"
        ${PYTHON:-python3} -m pip install --user . &> "${LAB_DIR}/build/logs/kanod-boot.log"
    )
fi


if ! [ -d "${LAB_DIR}/build/image-builder" ] && [ "${BUILDER_IN_CONTAINER:-0}" == "0" ]; then
    echo "- Building image-builder (version ${KANOD_BUILDER_REF})"
    git clone "${GIT_URL}/image-builder" "${LAB_DIR}/build/image-builder"
    (
        cd "${LAB_DIR}/build/image-builder"
        git checkout "${KANOD_BUILDER_REF}"
        ./build.sh &> "${LAB_DIR}/build/logs/builder.log"
    )
fi

if ! [ -d "${LAB_DIR}/build/kanod-node" ]; then
    echo "- Building service machine (version $KANOD_NODE_REF)"
    git clone "${GIT_URL}/kanod-node" "${LAB_DIR}/build/kanod-node"
    (
        cd "${LAB_DIR}/build/kanod-node"
        git checkout "$KANOD_NODE_REF"
        ${PYTHON:-python3} -m pip install --user .  &> "${LAB_DIR}/build/logs/kanod-node.log"
        kanod-image-builder kanod_node -s "k8s_release=${K8S_REF}" -s target=ubuntu -s "release=${NODE_OS}" -s "os_name=${NODE_OS}" -s "os_version=${KANOD_NODE_REF}" -s container_runtime=containerd &> "${LAB_DIR}/build/logs/service.log"
    )
fi

if [ "$KANOD_SWITCH" = 1 ] && ! [ -d "${LAB_DIR}/build/kanod-switch" ]; then
    echo '- Building switch image'
    git clone "${GIT_URL}/kanod-switch" "${LAB_DIR}/build/kanod-switch"
    (
        cd "${LAB_DIR}/build/kanod-switch"
        git checkout "$KANOD_SWITCH_REF"
        ${PYTHON:-python3} -m pip install --user .  &> "${LAB_DIR}/build/logs/kanod-switch.log"
        kanod-image-builder kanod_switch -s target=ubuntu -s release=jammy &> "${LAB_DIR}/build/logs/switch.log"
    )
fi
