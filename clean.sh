#!/bin/bash

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

set -eu

for domain in $(virsh list --name --state-running | grep '^vmok-[0-9]*$'); do
    echo "- Delete vm ${domain}"
    virsh destroy "${domain}"
done

for domain in $(virsh list --name --all | grep '^vmok-[0-9]*$'); do
    echo "- Undefine vm ${domain}"
    virsh undefine "${domain}"
done

for domain in $(virsh list --name --state-running | grep '^byohost-[0-9]*$'); do
    echo "- Delete vm ${domain}"
    virsh destroy "${domain}"
done

for domain in $(virsh list --name --all | grep '^byohost-[0-9]*$'); do
    echo "- Undefine vm ${domain}"
    virsh undefine "${domain}"
done

for vol in $(virsh vol-list "${STORAGE_POOL}" | awk '{print $1}' | grep '^vol-[0-9]*$'); do
    echo "- Delete volume ${vol}"
    virsh vol-delete "${vol}" "${STORAGE_POOL}"
done

for vmname in lcm broker switch; do
    echo "check ${vmname} for deletion"
    for domain in $(virsh list --name --state-running | grep "^${vmname}$"); do
        echo "Delete vm ${domain}"
        virsh destroy "${domain}"
    done
    for domain in $(virsh list --name --all | grep "^${vmname}$"); do
        echo "Undefine vm ${domain}"
        virsh undefine "${domain}"
    done
done

echo "remove git config for gogs projects"
rm -fr "${LAB_DIR}/netdef/.git"
rm -fr "${LAB_DIR}/cluster/.git"
rm -fr "${LAB_DIR}/meta/.git"
rm -fr "${LAB_DIR}/bmh/.git"

echo "terminate sushy-emulator instances"
set +e
if pgrep -a sushy-emulator > /dev/null; then 
    killall sushy-emulator
fi

set -e

echo "End"
