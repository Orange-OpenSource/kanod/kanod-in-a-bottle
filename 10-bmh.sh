#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

REPO=hosts

cd "${LAB_DIR}/bmh" || { echo "error during cd bmh" ; exit; }
git init -b main
git config --local user.name "${INFRA_USER}"
git config --local user.email "${INFRA_USER}@localhost.localdomain"
git config --local http.sslVerify false
git remote add origin "https://${INFRA_USER}:${INFRA_PASSWORD}@${GOGS_IP}/${INFRA_USER}/${REPO}.git" || true
git add .
git commit -m 'Initial setup' || true
git push -f origin main
