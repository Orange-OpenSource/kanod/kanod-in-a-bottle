#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


# 00-mk-template.sh generates templates for 'kanod in a bottle' from
# a single file params.yml that contains parameters that cannot be guessed
# by the system.
#
# This file can also be used to replace some defaults if the end-user wishes.

# !!WARNING!!: when editing, preserve tabs for indented here-documents

set -eu

params="${1:-params.yml}"

ROOT_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")
export ROOT_DIR
lab_dir="$(yq '.root // (strenv(ROOT_DIR) + "/lab")' "$params")"

# ensure lab_dir exists and make it absolute
mkdir -p "${lab_dir}"
lab_dir="$(readlink -f "$lab_dir")"

if ! type yq &> /dev/null; then
    echo "Command 'yq' is needed in your path (https://github.com/mikefarah/yq)"
    exit 1
fi

yesno() {
    case "$(yq "$1" "$params")" in
        null) echo "$2";;
        [YyTt]* ) echo 1;;
        [FfNn]* ) echo 0;;
        *) echo "$2";;
    esac
}

echo '- Getting the release parameters'
PARAMS="$(cat "${ROOT_DIR}/templates/versions.yaml")"
export PARAMS

proxy_enabled="$(yesno '.proxy.enabled' 0)"
ssh_default="$(yesno '.ssh.default' 1)"
uefi="$(yesno '.uefi' 0)"

vault="$(yesno '.vault.enabled' 0)"
tpm="$(yesno '.vault.tpm' 0)"

redfish_broker="$(yesno '.redfish_broker.broker_enabled' 0)"
broker_auth="$(yesno '.redfish_broker.broker_auth' 0)"
switch="$(yesno '.switch' 0)"

pivot_enabled="$(yesno '.pivot_enabled' 0)"
k8s_upgrade_enabled="$(yesno '.k8s_upgrade.k8s_upgrade_enabled' 0)"
scaling_enabled="$(yesno '.scaling_enabled' 0)"
multi_cluster="$(yesno '.mode_multi_cluster' 0)"

autoscale="$(yesno '.autoscale' 0)"
ipam="$(yesno '.ipam' 0)"
hybrid="$(yesno '.hybrid.enabled' 0)"
hybrid_kubevirt="$(yesno '.hybrid.kubevirt' 0)"
openstack="$(yesno '.hybrid.openstack' 0)"
hybrid_mono_namespace="$(yesno '.hybrid.mono_namespace' 0)"
mode_multi_tenant="$(yesno '.mode_multi_tenant' 0)"
airgap="$(yesno ".airgap" 0)"
ha_cluster="$(yesno ".ha" 0)"
kamaji_enabled="$(yesno '.kamaji.enabled' 0)"
byoh_enabled="$(yesno '.byoh_provider.enabled' 0)"
byoh_provider_kubevirt="$(yesno '.byoh_provider.kubevirt' 0)"
nb_byoh_hosts="$(yq '.byoh_provider.nb_host // "2"' "$params")"
byoh_provider_cp="$(yesno '.byoh_provider.cp' 0)"
byoh_provider_worker="$(yesno '.byoh_provider.worker' 0)"
byoh_in_pool="$(yesno '.byoh_provider.byoh_in_pool' 0)"

os_debug="$(yesno '.os_debug' 0)"
ubuntu="$(yesno .ubuntu 1)"
centos="$(yesno '.centos' 0)"
opensuse="$(yesno .opensuse 0)"
rke2="$(yesno .rke2 0)"

kanod_boot_ref="$(yq '.components."kanod-boot" // ""' - <<< "$PARAMS")"
kanod_builder_ref="$(yq '.components."image-builder" // ""' - <<< "$PARAMS")"
stack_ref="$(yq '.components.stack // ""' - <<< "$PARAMS")"
node_ref="$(yq '.components."kanod-node" // ""' - <<< "$PARAMS")"
switch_ref="$(yq '.components."kanod-switch" // ""' - <<< "$PARAMS")"
tpm_ref="$(yq '.components.tpm-registrar // ""' - <<< "$PARAMS")"
vault_tpm_ref="$(yq '.vault-tpm-auth // ""' - <<< "$PARAMS")"
rke_machine_ref="$(yq '.components.rke-machine // ""' - <<< "$PARAMS")"
cluster_config="$(yq '."cluster-config" // ""' "$params")"
minions="$(yq '.minions // 3' "$params")"
minion_size="$(yq '.minion_size // "10G"' "$params")"
service_size="$(yq '.service_size // "25G"' "$params")"
bmc_protocol="$(yq '.bmc_protocol // "ipmi"' "$params")"
storage_pool="$(yq '.storage_pool // "okstore"' "$params")"
burner_build="$(yq '.burner_build // "0"' "$params")"
node_os="jammy"
kanod_http_proxy="$(yq '.proxy.http // ""' "$params")"
kanod_https_proxy="$(yq '.proxy.https // ""' "$params")"
kanod_no_proxy="$(yq '.proxy.no_proxy // ""' "$params")"
git_url="$(yq '.git // "https://gitlab.com/Orange-OpenSource/kanod"' "$params")"
ntp_config="$(yq '.ntp // []' "$params")"
routes="$(yq '.routes // ""' "$params")"
additional_certs="$(yq '.certificates // {}' "$params")"
# kubevirt activated
kubevirt=0
if [ "$hybrid_kubevirt" = 1 ] || [ "$byoh_provider_kubevirt" = 1 ]; then
    kubevirt=1
    echo '- kubevirt is activated'
fi
# Kubernetes versions
k8s_versions="$(yq -o json '."k8s_versions"' "${ROOT_DIR}/templates/versions.yaml")"
k8s_ref="$(yq '."k8s_versions"[0]' "${ROOT_DIR}/templates/versions.yaml")"
# SSH keys to accept
if [ "$ssh_default" = 1 ]; then
    ssh_keys="$(KEY="$(cat "$HOME/.ssh/id_rsa.pub")" yq -o json '(.ssh.keys // []) + [strenv(KEY)]' "$params")"
else
    ssh_keys="$(yq -o json '.ssh.keys // []' "$params")"
fi
# Header to add when adding the Kanod CR in a config.yaml
if [ "$vault" = 1 ]; then
    kanod_vault='@vault:yaml:'
else
    kanod_vault=''
fi
# Version of the operator
kanod_operator=https://maven.service.kanod.home.arpa/repository/kanod/kanod/stack/${stack_ref}/stack-${stack_ref}-operator.yml
# Registry mirrors
mirrors="$(yq '.mirror[0]' "$params")"
if [ "$mirrors" = 'null' ]; then
    mirrors="$(yq '.mirror // ""'  "$params")"
fi
mirrors=${mirrors#https://}

echo '- Check if kubernetes version upgrade is activated'
if [ "$k8s_upgrade_enabled" = 1 ]; then
    k8s_last_version="$(yq '."k8s_versions"[-1]' "${ROOT_DIR}/templates/versions.yaml")"
    export K8S_LAST_VERSION="${k8s_last_version}"
    k8s_upgraded_version="$(yq '(select(fileIndex == 0).k8s_upgrade.k8s_upgraded_version // strenv(K8S_LAST_VERSION))' "$params")"
    if [ "$k8s_last_version" = "$k8s_ref" ]; then
        echo "cannot configure k8s version upgrade (only one version: $k8s_last_version)"
        exit 1
    fi
fi

echo '- Check support for ironic in DHCP less mode'
if [ "$ipam" = 1 ]; then
    flavour_kind=ipam
    if [ "$bmc_protocol" == 'ipmi' ] || [ "$bmc_protocol" == 'redfish' ]; then
        echo 'IPAM requires virtualmedia (no pxe)'
        exit 1
    fi
else
    if [ "$switch" = 1 ] && [ "$pivot_enabled" == 1 ]; then
        flavour_kind=ipam
    else
        flavour_kind=dhcp
    fi
fi

echo '- Compute proxy configuration'

prepare_proxy() {
    if [ -n "$kanod_http_proxy" ]; then
        echo "http: ${kanod_http_proxy}"
    fi
    if [ -n "$kanod_https_proxy" ]; then
        echo "https: ${kanod_https_proxy}"
    fi
    if [ -n "$kanod_no_proxy" ]; then
        echo "no_proxy: ${kanod_no_proxy}"
    fi
}

proxy_host=''
proxy_port=''

if [ "$proxy_enabled" = 1 ]; then
    echo '- Proxy handling'
    kanod_no_proxy=".svc.cluster.local,.svc.service.local,.kanod.home.arpa,192.168.133.0/24,$kanod_no_proxy"
    proxy_config="$(prepare_proxy)"
    # We start from the HTTPS proxy for maven proxy. Maven does not distinguish http and https proxy but performs
    # everything over https.
    proxy_url=$(echo "$kanod_https_proxy" | sed -e's!^.*://!!')
    # extract the user and password (if any)
    userpass=$(echo "$proxy_url" | grep @ | cut -d@ -f1)
    proxy_password=$(echo "$userpass" | grep : | cut -d: -f2)
    if [ -n "$proxy_password" ]; then
        proxy_user=$(echo "$userpass" | grep : | cut -d: -f1)
    else
        proxy_user=$userpass
    fi

    hostport=$(echo "$proxy_url" | sed -e "s,$userpass@,,g" | cut -d/ -f1)
    proxy_port=$(echo "$hostport" | grep : | cut -d: -f2)
    if [ -n "$proxy_port" ]; then
        proxy_host=$(echo "$hostport" | grep : | cut -d: -f1)
    else
        proxy_host=$hostport
    fi
fi

echo '- Check if multi-cluster is activated'
if [ "$multi_cluster" = 1 ]; then
    if [ "$mode_multi_tenant" = 0 ]; then
        echo "mode_multi_tenant must be enabled with mode_multi_cluster"
        exit
    fi
fi

if [ "$hybrid_mono_namespace" = 1 ] && [ "$mode_multi_tenant" = 1 ]; then
    echo "mode_multi_tenant and hybrid_mono_namespace are incompatible"
    exit
fi

if [ "$byoh_enabled" = 1 ] && [ "$byoh_in_pool" = 0 ] && [ "$multi_cluster" == 1 ]; then
    echo "mode_multi_cluster must be disabled when byoh_provider is enabled"
    exit
fi

if [ "$ha_cluster" = 1 ]; then
    if [ "$minions" -le 5 ]; then minions=5; fi
fi

adjust_resources() {
    tag=$1
    file=$2
    for res in ram vcpus disk_size; do        
        value="$(TAG="$tag" RES="$res" yq '.resources[strenv(TAG)][strenv(RES)] // "-"' "$params")"
        if [ "$value" != '-' ]; then
            echo "  -> setting ${res} to ${value} in ${file}"
            VALUE="$value" RES="$res" yq '.vm[strenv(RES)] = env(VALUE)' -i "$file"
        fi
    done
}

base_template() {
    vm=$1
    vm_file="${lab_dir}/${vm}/config.yaml"
    mkdir -p "${lab_dir}/${vm}"
    cp "${ROOT_DIR}/templates/${vm}.yaml" "$vm_file"
    adjust_resources "$vm" "$vm_file"
    # inject ssh keys
    KEYS="$ssh_keys" yq ".admin.keys = env(KEYS)" -i "$vm_file"
    # inject the root certificate in the configuration
    yq '.certificates."kanod_ca" = "'"$(< "${ROOT_DIR}/certs/ca.cert")"'"' -i "$vm_file"
    # inject proxy configuration
    if [ "$proxy_enabled" = 1 ]; then
        PROXY=${proxy_config} yq '.proxy = env(PROXY)' -i "$vm_file"
    fi
    # inject registry mirrors
    if [ -n "$mirrors" ]; then
        MIRRORS="$mirrors" yq '.containers.registry_mirrors = [ "https://" + env(MIRRORS) ]' -i  "$vm_file"
    fi
    # ntp configuration
    NTP="$ntp_config" yq '.ntp.servers=env(NTP)' -i  "$vm_file"
    # trusted certificates
    CERTS="$additional_certs" yq '.certificates = (.certificates // {}) * env(CERTS)' -i  "$vm_file"
    # additional routes
    if [ -n "$routes" ]; then
        ROUTES="$routes" yq '.network.ethernets.ens3.routes = env(ROUTES)' -i "${vm_file}"
    fi
    if [ "$switch" = 1 ]; then
        if [ "$vm" == 'lcm' ]; then
            yq 'with(.network.bridges.br0; .routes = (.routes // []) + [{"via": "192.168.133.2", "to": "192.168.0.0/16"}])' -i "${vm_file}"
        else
            yq 'with(.network.ethernets.ens3; .routes = (.routes // []) + [{"via": "192.168.133.2", "to": "192.168.0.0/16"}])' -i "${vm_file}"
        fi
    fi
}

configure_vault() {
    vm=$1
    vm_file="${lab_dir}/${vm}/config.yaml"
    if [ "$vault" == '1' ]; then
        # Url to Vault apiserver
        yq '.vault.url = "https://vault.service.kanod.home.arpa"' -i "${vm_file}"
        # Role for vault
        VM="$vm" yq '.vault.role = strenv(VM)' -i "${vm_file}"
        # Role id for vault
        VM="$vm" yq '.vault.role_id = ("kanod-" + strenv(VM) + "-id")' -i "${vm_file}"
        # Adds the root CA to Vault config
        CA="$(cat "${ROOT_DIR}/certs/ca.cert")" yq ".vault.ca = strenv(CA)" -i "${vm_file}"
        # And remove it from the certificates (unnecessary)
        yq 'del(.certificates."kanod_ca")' -i "${vm_file}"
    else
        # Remove vault configuration
        yq 'del(.vault)' -i "${vm_file}"
    fi
}

######################################
#  Service machine
######################################

echo '- Configuration of service machine'
base_template service
# inject storage pool name
KANOD_DISK="${storage_pool}/service" yq '.vm.disks[0] = strenv(KANOD_DISK)' -i "${lab_dir}/service/config.yaml"
# cleanup
if [ -d "${lab_dir}/service/manifests" ]; then
    rm -r "${lab_dir}/service/manifests"
fi
# Generate manifests
cp -r "${ROOT_DIR}/templates/manifests" "${lab_dir}/service"
# Semantic versions map for workflow
echo -e "$(yq '.components | to_entries | map(.key + "=" + .value) | join("\n")' "${ROOT_DIR}/templates/versions.yaml" )" > "${lab_dir}/service/manifests/workflow/versions.env"
# Specific parts of versions for OS and internalized manifests in relevant folders
for section in ubuntu centos opensuse rke2 internalized; do
    yq -o json ".${section}" "${ROOT_DIR}/templates/versions.yaml" > "${lab_dir}/service/manifests/workflow/${section}.json"
done
# Kubernetes versions to support
yq -o json '."k8s_versions" | map({"release": .})' <<< "$PARAMS" > "${lab_dir}/service/manifests/workflow/k8s-release.json"
# Inject the root CA in the service machine known certificates
yq '.certificates."kanod_ca" = "'"$(< "${ROOT_DIR}/certs/ca.cert")"'"' -i "${lab_dir}/service/config.yaml"
# Gitea configuration
cp "${ROOT_DIR}/templates/gitea-config.yaml" "${lab_dir}/service/manifests/gitea/"
# Vault configuration
cp "${ROOT_DIR}/templates/vault-init.yaml" "${lab_dir}/service/manifests/vault/"
# Inject root CA into Vault configuration
CERTS=$(cat "${ROOT_DIR}/certs/ca.key" "${ROOT_DIR}/certs/ca.cert") yq -i '.ca=strenv(CERTS)' "${lab_dir}/service/manifests/vault/vault-init.yaml"
# Proxy configuration
if [ "$proxy_enabled" = 1 ]; then
    # Proxy configuration
    cat > "${lab_dir}/service/manifests/proxies.env" <<-EOF
		http_proxy=${kanod_http_proxy}
		https_proxy=${kanod_https_proxy}
		no_proxy=${kanod_no_proxy}
	EOF
    # Proxy configuration for Vault
    cp "${lab_dir}/service/manifests/proxies.env" "${lab_dir}/service/manifests/vault/proxies.env"
else
    # No proxy defined but files must exist
    touch "${lab_dir}/service/manifests/vault/proxies.env"
    touch "${lab_dir}/service/manifests/proxies.env"
fi
# Registry mirrors for workflow
if [ -n "$mirrors" ]; then
    cat >> "${lab_dir}/service/manifests/proxies.env" <<-EOF
		REGISTRY_MIRROR=${mirrors}
		DIB_REGISTRY_MIRROR=${mirrors}
	EOF
fi
# Adds other variables to proxies.env
echo -e "$(yq '.variables | to_entries | map(.key + "=" + .value) | join("\n")' "$params" )" >> "${lab_dir}/service/manifests/proxies.env"

# Maven configuration (uses yq in xml roundtrip mode)
if [ "$proxy_enabled" = 1 ]; then
    PROXY_HOST=${proxy_host} PROXY_PORT=${proxy_port} yq '
      .settings.proxies.proxy.host=strenv(PROXY_HOST) |
      .settings.proxies.proxy.port=strenv(PROXY_PORT)
    ' -i "${lab_dir}/service/manifests/workflow/settings.xml"
    if [ "${proxy_user}" != '' ]; then
        PROXY_USER="$proxy_user" yq '.settings.proxies.proxy.username=strenv(PROXY_USER)' -i "${lab_dir}/service/manifests/workflow/settings.xml"
    fi
    if [ "${proxy_password}" != '' ]; then
        PROXY_PASSWORD="$proxy_password" yq '.settings.proxies.proxy.password=strenv(PROXY_PASSWORD)' -i "${lab_dir}/service/manifests/workflow/settings.xml"
    fi
else
    yq 'del(.settings.proxies)' -i "${lab_dir}/service/manifests/workflow/settings.xml"
fi

# Adds git users and projects in multi cluster case
if [ "$multi_cluster" == 1 ]; then
    yq '.users += {"vault": "cluster2_admin"}' -i "${lab_dir}/service/manifests/gitea/gitea-config.yaml"
    yq '.projects += {"name": "config", "description": "Cluster2 Configuration Repository", "owner": "cluster2_admin", "webhooks": [{"url": "https://lcm.kanod.home.arpa/argocd/api/webhook"}] }' -i "${lab_dir}/service/manifests/gitea/gitea-config.yaml"
    yq '.kv1 += {"path": "cluster2_admin/git", "value": {"username": "cluster2_admin", "password": "secret2"}}' -i "${lab_dir}/service/manifests/vault/vault-init.yaml"
fi
# In switch mode
if [ "$switch" = 1 ]; then
    yq '.ntp.clients[0] = "192.168.0.0/16"' -i "${lab_dir}/service/config.yaml"
fi

# Add file controlling what is built
cat > "${lab_dir}/service/manifests/workflow/build-options.env" <<EOF
tpm=${tpm}
broker=${redfish_broker}
rke2=${rke2}
centos=${centos}
opensuse=${opensuse}
ubuntu=${ubuntu}
byoh_ubuntu=${byoh_enabled}
burner=${burner_build}
os_debug=${os_debug}
no_dhcp=${ipam}
kubevirt=${kubevirt}
EOF

# nfs-server configuration for Kubevirt
if [ "$kubevirt" = 1 ]; then
    echo '  configuring nfs server on service'
    yq '
        .runcmd = ["sudo apt update -y && sudo apt install nfs-kernel-server -y"] |
        .runcmd += "echo \"Configuring nfs export directory for kubevirt\"" |
        .runcmd += "sudo mkdir /kanod" |
        .runcmd += "sudo chown -R nobody:nogroup /kanod" |
        .runcmd += "sudo chmod 777 /kanod" |
        .runcmd += "sudo mkdir -p /etc/exports.d" |
        .runcmd += "echo \"/kanod 192.168.133.0/24(rw,sync,no_wdelay,no_root_squash,insecure,fsid=0,no_subtree_check)\" | sudo tee  /etc/exports.d/kanod.exports" |
        .runcmd += "sudo exportfs -arv" |
        .runcmd += "sudo systemctl restart nfs-kernel-server"
    ' -i "${lab_dir}/service/config.yaml"
fi


######################################
#  Life Cycle Manager machine
######################################

echo '- Configuration of LCM machine'
base_template lcm
configure_vault lcm
# Remove filter that will prevent external communication if not in airgap mode
if [ "$airgap" == 0 ]; then
    yq 'del .vm.networks[].filter' -i "${lab_dir}/lcm/config.yaml"
fi
# Inject Kanod operator version.
KANOD_OPERATOR="$kanod_operator" yq '.kubernetes.manifests=[{"url": strenv(KANOD_OPERATOR)}]' -i "${lab_dir}/lcm/config.yaml"
# Refer to the regular image in LCM configuration
OS_VERSION="${node_os}" NODE_REF="${node_ref}" K8S="${k8s_ref}" yq '
	.os = strenv(OS_VERSION) | .version = strenv(NODE_REF) | .kubernetes.version = strenv(K8S)
' -i "${lab_dir}/lcm/config.yaml"
# Base template for Kanod Operator custom resource
cp "${ROOT_DIR}/templates/lcm-kanod.yaml" "${lab_dir}/lcm/kanod.yaml"
if [ "$vault" = 1 ]; then
    # Replace with references to Vault if it is enabled
    yq '
        .nexus.certificate = "@vault:ca" |
        .vault.certificates[0].ip[1] = "192.168.133.11" |
        .admin.passwd = "@vault:kv1:admin:password"
    ' -i "${lab_dir}/lcm/config.yaml"
    yq  '
        select(document_index==0).spec.ingress.key.value = "@vault:pki-key:lcm" |
        select(document_index==0).spec.ingress.certificate.value = "@vault:pki-chain:lcm" |
        select(document_index==0).spec.argocd.gitcerts=[{"host": "git.service.kanod.home.arpa", "value": "@vault:ca"}] |
        select(document_index==1).stringData.password = "@vault:kv1:../infra_admin/git:password" |
        select(document_index==1).stringData.username = "@vault:kv1:../infra_admin/git:username"
    ' -i "${lab_dir}/lcm/kanod.yaml"
    if [ "$tpm" = 1 ]; then
        yq '
            select(document_index==0).spec.ironic.tpm.authUrl = "https://tpm-auth.service.kanod.home.arpa" |
            select(document_index==0).spec.ironic.tpm.authCa.value = "@vault:ca"
        ' -i "${lab_dir}/lcm/kanod.yaml"
    fi
else
    # Manual insertion of ca for nexus
    yq '.nexus.certificate = "'"$(< "${ROOT_DIR}/certs/ca.cert")"'"' -i "${lab_dir}/lcm/config.yaml"
    # Register ca for git
    yq 'select(document_index==0).spec.argocd.gitcerts=[{"host": "git.service.kanod.home.arpa"}]' -i "${lab_dir}/lcm/kanod.yaml"
    yq 'select(document_index==0).spec.argocd.gitcerts[0].value = "'"$(< "${ROOT_DIR}/certs/ca.cert")"'"' -i "${lab_dir}/lcm/kanod.yaml"
    # Define private key and cert chain for ingress
    yq 'select(document_index==0).spec.ingress.key.value = "'"$(< "${ROOT_DIR}/certs/lcm.key")"'"' -i "${lab_dir}/lcm/kanod.yaml"
    yq 'select(document_index==0).spec.ingress.certificate.value = "'"$(< "${ROOT_DIR}/certs/lcm.chain")"'"' -i "${lab_dir}/lcm/kanod.yaml"
fi
# Debug mode inject a password on IPA kernel command line
if [ "$os_debug" == 1 ]; then
    # todo: provide an option for the password
    ipa_password=$(openssl passwd -6 secret | sed 's/\$/$$/g')
    PASSWORD="$ipa_password" yq 'select(document_index==0).spec.ironic.ipaKernelParams = "-rootpwd=\"" + strenv(PASSWORD) + "\""' -i "${lab_dir}/lcm/kanod.yaml"
fi
if [ "$redfish_broker" == 1 ]; then
    # Force containerd as engine (weird behaviour with cri-o)
    yq '.kubernetes.container_engine = "containerd"' -i "${lab_dir}/lcm/config.yaml"
    # Remove the BMH application, add a broker section and inject broker IP in kanod custom resource
    yq '
        del(select(document_index==0).spec.argocd.baremetal) |
        select(document_index==0).spec.broker.ip = "broker.kanod.home.arpa"
    ' -i "${lab_dir}/lcm/kanod.yaml"
fi
# Activate RKE2 if used
if [ "$rke2" = 1 ]; then
    yq 'select(document_index==0).spec.rke2 = true' -i "${lab_dir}/lcm/kanod.yaml"
fi
if [ "${ipam}" = 1 ]; then
    yq 'select(document_index==0).spec.noDhcp = true' -i "${lab_dir}/lcm/kanod.yaml"
fi

# Enable kamaji
if [ "$kamaji_enabled" = 1 ]; then
    echo '- configuring for using kamaji'
    yq '.vm.disk_size = 25 | .vm.vcpus = 8 | .kubernetes.cluster_name = "cluster"' -i "${lab_dir}/lcm/config.yaml"
    yq 'select(document_index==0).spec.kamaji = true' -i "${lab_dir}/lcm/kanod.yaml"
fi
# Enable byoh
if [ "$byoh_enabled" = 1 ]; then
    yq 'select(document_index==0).spec.byoh.enabled = true' -i "${lab_dir}/lcm/kanod.yaml"
    if [ "$byoh_in_pool" = 1 ]; then
        yq 'select(document_index==0).spec.byoh.byohInPool = true' -i "${lab_dir}/lcm/kanod.yaml"
    fi
fi

if [ "$hybrid" = 1 ]; then
    yq 'select(document_index==0).spec.capiHosts = true' -i "${lab_dir}/lcm/kanod.yaml"
    if [ "$hybrid_kubevirt" = 1 ]; then
        yq 'select(document_index==0).spec.capiHostKubevirt = true' -i "${lab_dir}/lcm/kanod.yaml"
    fi
    if [ "$openstack" = 1 ]; then
        yq 'select(document_index==0).spec.capiHostsOpenstack = true' -i "${lab_dir}/lcm/kanod.yaml"
    fi
fi

if [ "$kubevirt" = 1 ]; then
    echo '- configuring kubevirt'
    yq 'select(document_index==0).spec.kubevirt.enabled = true' -i "${lab_dir}/lcm/kanod.yaml"

    # yq has no min operator so we hack one
    yq '.vm.ram = ([.vm.ram // 16384, 16384] | sort_by(.))[1]' -i "${lab_dir}/lcm/config.yaml"
    NFS_SERVER="192.168.133.12"  NFS_PATH="/kanod" yq '
        select(document_index==0).spec.kubevirt.nfsProvisioner.nfsServer =  strenv(NFS_SERVER) |
        select(document_index==0).spec.kubevirt.nfsProvisioner.nfsPath =  strenv(NFS_PATH)
    ' -i "${lab_dir}/lcm/kanod.yaml"
fi

# Merge back the kanod CR in config.yaml
KANOD_VAULT="$kanod_vault" LAB_DIR="$lab_dir" yq '
    .kubernetes.manifests += [{"content": strenv(KANOD_VAULT) + load_str(strenv(LAB_DIR) + "/lcm/kanod.yaml")}]
' -i "${lab_dir}/lcm/config.yaml"

######################################
#  Broker machine
######################################

if [ "$redfish_broker" = 1 ]; then
    echo '- Configuration of Broker machine'
    base_template broker
    configure_vault broker
    cp "${ROOT_DIR}/templates/broker-kanod.yaml" "${lab_dir}/broker/kanod.yaml"
    # Configure os image
	OS_VERSION="${node_os}" NODE_REF="${node_ref}" K8S="${k8s_ref}" yq '
		.os = strenv(OS_VERSION) | .version = strenv(NODE_REF) | .kubernetes.version = strenv(K8S)
	' -i "${lab_dir}/broker/config.yaml"

    # Kanod operator version
    KANOD_OPERATOR="$kanod_operator" yq '.kubernetes.manifests[0].url = strenv(KANOD_OPERATOR)' -i "${lab_dir}/broker/config.yaml"

    if [ "$vault" = 1 ]; then
        yq '.nexus.certificate = "@vault:ca" | .admin.passwd = "@vault:kv1:admin:password"' -i "${lab_dir}/broker/config.yaml"
    else
        CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '.nexus.certificate = strenv(CA)' -i "${lab_dir}/broker/config.yaml"
        CA="$(< "${ROOT_DIR}/certs/ca.cert")" CERT="$(< "${ROOT_DIR}/certs/broker.cert")" KEY="$(< "${ROOT_DIR}/certs/broker.key")" yq '
            select(document_index==0).spec.argocd.gitcerts=[{"host": "git.service.kanod.home.arpa"}] |
            select(document_index==0).spec.argocd.gitcerts[0].value = strenv(CA) |
            select(document_index==0).spec.ingress.key.value = strenv(KEY) |
            select(document_index==0).spec.ingress.certificate.value = strenv(CERT) + "\n" + strenv(CA) |
            select(document_index==1).stringData.password = "secret1" |
            select(document_index==1).stringData.username = "infra_admin" |
            select(document_index==2).stringData.password = "secret" |
            select(document_index==2).stringData.username = "root" |
            select(document_index==3).stringData.password = "secret" |
            select(document_index==3).stringData.username = "root"' -i "${lab_dir}/broker/kanod.yaml"
    fi
    # inline the kanod custom resource in the config.yaml
    KANOD_VAULT="$kanod_vault" yq '.kubernetes.manifests[1].content |= strenv(KANOD_VAULT) + load_str("'"${lab_dir}"'/broker/kanod.yaml")' -i "${lab_dir}/broker/config.yaml"
fi

##################################
#   Switch
##################################

if [ "$switch" = 1 ]; then
    echo "- Switch configuration"
    mkdir -p "${lab_dir}/switch"
    cp "${ROOT_DIR}/templates/switch.yaml" "${lab_dir}/switch/config.yaml"
    adjust_resources 'switch' "${lab_dir}/switch/config.yaml"
    KEYS="$ssh_keys" yq ".admin.keys = env(KEYS)" -i "${lab_dir}/switch/config.yaml"
fi

##################################
#   OpenStack
##################################
if [ "$openstack" = 1 ]; then
    echo "- Openstack (Devstack) configuration"
    mkdir -p "${lab_dir}/openstack"
    cp "${ROOT_DIR}/templates/openstack.yaml" "${lab_dir}/openstack/config.yaml"
    adjust_resources 'openstack' "${lab_dir}/openstack/config.yaml"
    KEYS="$ssh_keys" yq ".admin.keys = env(KEYS)" -i "${lab_dir}/openstack/config.yaml"
    CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '.certificates."kanod_ca" = strenv(CA)' -i "${lab_dir}/openstack/config.yaml"
fi

##################################
#   Minions
##################################

echo "- Minions"
for ((i=1; i<=minions; i++ )); do
    name="vmok-$i"
    vmfile="${lab_dir}/${name}/config.yaml"
    mkdir -p "${lab_dir}/${name}"
    cp "${ROOT_DIR}/templates/vmok.yaml" "${vmfile}"
    adjust_resources 'vmok' "${vmfile}"
    NAME="${name}" MAC="$(printf "52:54:00:01:00:%02d" "${i}")" yq '
        .name=strenv(NAME) | .vm.networks[0].mac=strenv(MAC)
    ' -i "${vmfile}"
    if [ "$tpm" = 1 ]; then
        yq -i '.vm.tpm = true' -i "${vmfile}"
    fi
    if [ "$switch" = 1 ]; then
        # Replace interface on oknet with a wire at right offset connected to the switch
        WIRE=$(( i + 10 )) yq 'del(.vm.networks[0].network) | .vm.networks[0].wire = env(WIRE)' -i "${vmfile}"
    fi
    if [ "$uefi" = 1 ]; then
        yq e ".vm.uefi = true" -i "${vmfile}"
    fi
done

##################################
#   Server Inventory project
##################################

mkdir -p "${lab_dir}/bmh"
rm -f "${lab_dir}/bmh/"*".yaml"

declare_bmh_cred() {
    id=$1
    if [ "$vault" = 1 ]; then
        IDX="$id" yq '
            .secretGenerator += [ {
                "name": "bm-credentials-" + strenv(IDX),
                "literals" : ["username=<path:secret/baremetal/bmc/#username>", "password=<path:secret/baremetal/bmc/#password>"],
                "options": {
                    "annotations": {"avp.kubernetes.io/kv-version": "1"}
                }
            }]
        ' -i "${bmh_dir}/kustomization.yaml"
    else
        IDX="$id" yq '
            .secretGenerator += [ {
                "name": "bm-credentials-" + strenv(IDX),
                "literals" : ["username=root", "password=secret"]
            }]
        ' -i "${bmh_dir}/kustomization.yaml"
    fi
}

declare_user() {
    i=$1
    user="${lab_dir}/bmh/user-$i.yaml"
    cp "${ROOT_DIR}/gogs-data/servers/user.yaml" "${user}"
    NAME="pool-user-$i" CRED_NAME="user$i-secret" NB="$minions" yq '
        .metadata.name = strenv(NAME) |
        .spec.maxServers = env(NB) |
        .spec.credentialName = strenv(CRED_NAME)
    ' -i "${user}"
    IDX="$i" yq '
        .resources += [ "user-" + strenv(IDX) + ".yaml" ] |
        .secretGenerator += [{
            "name": "user" + strenv(IDX) + "-secret",
            "literals": ["password=secret"]
        }]
    ' -i "${lab_dir}/bmh/kustomization.yaml"
}

declare_network() {
    name=$1
    vlan=$2
    netdef="${lab_dir}/bmh/netdef-$name.yaml"
    cp "${ROOT_DIR}/gogs-data/servers/network-def.yaml" "${netdef}"
    NAME="$name" VLAN="$vlan" yq '
        .metadata.name = strenv(NAME) |
        .spec.vnid = strenv(VLAN) |
        .spec.subnetPrefix = "192.168." + strenv(VLAN) + ".0/24" |
        .spec.gatewayIp = "192.168." + strenv(VLAN) + ".1/24" |
        .spec.subnetStart = "192.168." + strenv(VLAN) + ".2" |
        .spec.subnetEnd = "192.168." + strenv(VLAN) + ".199" |
        .spec.credentialName = "cred-" + strenv(NAME)
    ' -i "$netdef"
    if [ "${3:0}" = 0 ]; then
        yq 'del(.metadata.annotations."kanod.io/untagged")' -i "$netdef"
    fi
    NAME="$name" yq '
        .resources += [ "netdef-" + strenv(NAME) + ".yaml" ] |
        .secretGenerator += [{
            "name": "cred-" + strenv(NAME),
            "literals": ["username=user-" + strenv(NAME), "password=secret"]
        }]
    ' -i "${lab_dir}/bmh/kustomization.yaml"
}

declare_bmh() {
    id=$1
    server="${bmh_dir}/server-${id}.yaml"
    port=$((5000+id))
    mac=$(printf "52:54:00:01:00:%02d" "${id}")
    if [ "${bmc_protocol}" = "ipmi" ]; then
        bmc_address="ipmi://192.168.133.1:$port"
    elif [ "${bmc_protocol}" = "redfish" ] || [ "${bmc_protocol}" = "redfish-virtualmedia" ]; then
        bmc_address="${bmc_protocol}://192.168.133.1:$port/redfish/v1/Systems/vmok-${id}"
    else
        echo "unknown protocol for bmc : ${bmc_protocol}"
        exit 1
    fi
    cp "${ROOT_DIR}/gogs-data/servers/server.yaml" "${server}"
    NAME="vmok-${id}" CRED_NAME="bm-credentials-${id}" MAC="$mac" BMC_ADDRESS="$bmc_address" yq '
        .metadata.name = strenv(NAME) |
        .spec.bootMACAddress = strenv(MAC) |
        .spec.bmc.address = strenv(BMC_ADDRESS) |
        .spec.bmc.credentialsName = strenv(CRED_NAME)
    ' -i "${server}"
    if [ "$ipam" = 1 ]; then
        yq '
            .metadata.labels."network.kanod.io/bmh-data" = "pxe-network" |
            .metadata.annotations."baremetalhost.metal3.io/paused" = "network.kanod.io/bmh-data"
        ' -i "${server}"
    else
        yq '
            .metadata.labels."kanod.io/dhcp" = "pxe" |
            .metadata.annotations."baremetalhost.metal3.io/paused" = "kanod.io/kanod-kea"
        ' -i "${server}"
    fi
    if [ "$uefi" = 1 ]; then
        yq '.spec.bootMode = "UEFI"' -i "${server}"
    fi
    if [ "$hybrid" = 1 ]; then
        yq '.metadata.labels."host.kanod.io/kind" = "baremetal"' -i "${server}"
    fi
    IDX="${id}" yq '
        .resources += [ "server-" + strenv(IDX) + ".yaml" ]
    ' -i "${bmh_dir}/kustomization.yaml"
}

declare_namespace() {
    name=$1
    touch "${bmh_dir}/namespace.yaml"
    yq '.apiVersion = "v1"' -i "${bmh_dir}/namespace.yaml"
    yq '.kind = "Namespace"' -i "${bmh_dir}/namespace.yaml"
    NAME=${name} yq '.metadata.name = strenv(NAME)' -i "${bmh_dir}/namespace.yaml"
    yq '.resources += [ "namespace.yaml" ]' -i "${bmh_dir}/kustomization.yaml"
}


if [ "$redfish_broker" = 0 ]; then
    echo '- server inventory'
    cp "${ROOT_DIR}/gogs-data/servers/"{kustomization.yaml,config.yaml} "${lab_dir}/bmh"
    bmh_dir="${lab_dir}/bmh"
    for ((i=1;i<=minions;i++)); do
        bmh_dir="${lab_dir}/bmh"
        declare_bmh_cred "$i" 
        declare_bmh "$i" 
    done

    if [ "$hybrid" = 0 ]; then 
        yq '.namespace = "capm3"' -i "${bmh_dir}/kustomization.yaml"
    else
        if [ "$hybrid_mono_namespace" = 1 ]; then 
            yq '.namespace = "capm3"' -i "${bmh_dir}/kustomization.yaml"
        else
            declare_namespace "infrahost"
            yq '.namespace = "infrahost"' -i "${bmh_dir}/kustomization.yaml"
        fi
    fi
else
    echo '- server inventory (broker mode)'
    bmh_dir="${lab_dir}/bmh"
    cp "${ROOT_DIR}/gogs-data/servers/"{kustomization.yaml,config.yaml} "${bmh_dir}"
    for ((i=1;i<=minions;i++)); do
        server="${bmh_dir}/server-$i.yaml"
        declare_bmh_cred "$i"
        port=$((5000+i))
        mac=$(printf "52:54:00:01:00:%02d" "${i}")
        bmc_address="https://192.168.133.1:$port/redfish/v1/Systems/vmok-$i"
        cp "${ROOT_DIR}/gogs-data/servers/server-def.yaml" "${server}"
        NAME="vmok-$i" CRED_NAME="bm-credentials-$i" MAC="$mac" BMC_ADDRESS="$bmc_address" yq '
            .metadata.name = strenv(NAME) |
            .spec.id = strenv(NAME) |
            .spec.macAddress = strenv(MAC) |
            .spec.url = strenv(BMC_ADDRESS) |
            .spec.baremetalcredential = strenv(CRED_NAME)
        ' -i "${server}"
        IDX="$i" yq '
            .resources += [ "server-" + strenv(IDX) + ".yaml" ]
        ' -i "${bmh_dir}/kustomization.yaml"
        if [ "$switch" = 1 ]; then
            IDX="$i" yq '.metadata.annotations."kanod.io/networks" = "['\''na-" + strenv(IDX) + "'\'']"' -i "$server"
            na="${bmh_dir}/na-$i.yaml"
            cp "${ROOT_DIR}/gogs-data/servers/na.yaml" "$na"
            IDX="$i" yq '
                .metadata.name = "na-" + strenv(IDX) |
                .spec.hostName = "vmok-" + strenv(IDX) |
                .spec.switchPort = "Ethernet" + strenv(IDX)
            ' -i "$na"
            IDX="$i" yq '
                .resources += [ "na-" + strenv(IDX) + ".yaml" ]
            ' -i "${bmh_dir}/kustomization.yaml"
        fi
    done
    yq '.namespace = "brokerdef-system"' -i "${bmh_dir}/kustomization.yaml"

    declare_user 1

    if [ "$multi_cluster" = 1 ]; then
        declare_user 2
    fi

    if [ "$switch" = 1 ]; then
        cp "${ROOT_DIR}/gogs-data/servers/switch.yaml" "${lab_dir}/bmh/switch.yaml"
        yq '.resources += ["switch.yaml"]' -i "${lab_dir}/bmh/kustomization.yaml"
        declare_network "k8s-1" "144" 0
        declare_network "pxe-1" "100" 1
        if [ "$multi_cluster" = 1 ]; then
            declare_network "k8s-2" "155" 0
            declare_network "pxe-2" "110" 1
        fi
    fi

fi
##################################
#   Meta Git project
##################################

echo '- meta git project'
mkdir -p "${lab_dir}/meta"
cp "${ROOT_DIR}/gogs-data/cluster-def"/{infra.yaml,kustomization.yaml,passwords} "${lab_dir}/meta/"

# inject proxy in infra.yaml on the default templates (first in lists)
if [ "$proxy_enabled" = 1 ]; then
    PROXY="$proxy_config" yq '
        .controlPlaneFlavours[0].serverConfig.proxy = env(PROXY) |
        .workerFlavours[0].serverConfig.proxy = env(PROXY) |
        .serverFlavours[0].serverConfig.proxy = env(PROXY)
    ' -i "${lab_dir}/meta/infra.yaml"
fi

# Certificate injection
if [ "$vault" == 1 ]; then
    # Get the CA from Vault for Nexus entry
    yq '.nexus.certificate = "@vault:ca"' -i "${lab_dir}/meta/infra.yaml"
else
    # inject the CA in default flavours and in nexus.
    CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '
        .controlPlaneFlavours[0].serverConfig.certificates."kanod_ca" = strenv(CA) |
        .workerFlavours[0].serverConfig.certificates."kanod_ca" =  strenv(CA) |
        .serverFlavours[0].serverConfig.certificates."kanod_ca" =  strenv(CA) |
        .nexus.certificate = strenv(CA)
    ' -i "${lab_dir}/meta/infra.yaml"
fi

# kubernetes supported versions if we want to do an upgrade
K8S_VERSION="${k8s_versions}" yq '.supportedVersions = env(K8S_VERSION)' -i "${lab_dir}/meta/infra.yaml"
# if switch defined modify networking to use vlans in infra
if [ "$switch" = 1 ]; then
    yq '
        with(.controlPlaneFlavours[].serverConfig | select(has("network"));
                .network.vlans.VLAN_NAME = (.network.ethernets["{{ds.meta_data.interface}}"] * {"id": "{{ds.meta_data.net_k8s_vnid}}", "link": "{{ds.meta_data.net_k8s_interface}}"}) |
                .network.vlans.VLAN_NAME.nameservers.addresses[0] = "192.168.133.2" |
                .network.ethernets["{{ds.meta_data.interface}}"] = {"dhcp4": false } |
                (.network.vlans.VLAN_NAME | key) = "vlan{{ds.meta_data.net_k8s_vnid}}" |
                (.network.ethernets["{{ds.meta_data.interface}}"] | key) = "{{ds.meta_data.net_k8s_interface}}"
            ) |
        with(.workerFlavours[].serverConfig | select(has("network"));
                 .network.vlans.VLAN_NAME = .network.ethernets["{{ds.meta_data.interface}}"] * {"id": "{{ds.meta_data.net_k8s_vnid}}", "link": "{{ds.meta_data.net_k8s_interface}}"} |
                 .network.vlans.VLAN_NAME.nameservers.addresses[0] = "192.168.133.2" |
                 .network.ethernets["{{ds.meta_data.interface}}"] = {"dhcp4": false } |
                 (.network.vlans.VLAN_NAME | key) = "vlan{{ds.meta_data.net_k8s_vnid}}" |
                 (.network.ethernets["{{ds.meta_data.interface}}"] | key) = "{{ds.meta_data.net_k8s_interface}}"
            ) |
        with(.controlPlaneFlavours[].serverConfig  | select(.kubernetes | has("keepalived"));
                 .kubernetes.keepalived.interface = "vlan{{ds.meta_data.net_k8s_vnid}}")
    ' -i "${lab_dir}/meta/infra.yaml"
    # Erase the default dhcp network.
else
    if [ "$ipam" = 1 ]; then
        cp "${ROOT_DIR}/gogs-data/servers/bmh-data.yaml" "${lab_dir}/meta/"
        yq '.resources += [ "bmh-data.yaml" ]' -i "${lab_dir}/meta/kustomization.yaml"
    else
        cp "${ROOT_DIR}/gogs-data/servers/dhcp.yaml" "${lab_dir}/meta/"
        yq '.resources += [ "dhcp.yaml" ]' -i "${lab_dir}/meta/kustomization.yaml"
        if [ "$hybrid" = 1 ]; then 
            if [ "$hybrid_mono_namespace" = 1 ]; then 
                yq '.metadata.namespace = "capm3"' -i "${lab_dir}/meta/dhcp.yaml"
            else
                yq '.metadata.namespace = "infrahost"' -i "${lab_dir}/meta/dhcp.yaml"
            fi
        fi
    fi
fi

declare_cluster() {
    i=$1
    vlan=$((133+i*11))
    cluster_root="${lab_dir}/meta/cluster$i"
    cp "${ROOT_DIR}/gogs-data/cluster-def/cluster$i.yaml" "${lab_dir}/meta/"
    cp "${ROOT_DIR}/gogs-data/cluster-def/git-cred$i.yaml" "${lab_dir}/meta/"
    cp -r "${ROOT_DIR}/gogs-data/cluster-def/cluster$i" "${lab_dir}/meta/"
    cp "${ROOT_DIR}/gogs-data/cluster-def/ci-data$i.yaml" "${lab_dir}/meta/"
    yq ".resources += [\"cluster$1\", \"cluster$1.yaml\", \"git-cred$1.yaml\", \"ci-data$1.yaml\"]" -i "${lab_dir}/meta/kustomization.yaml"
    if [ "${vault}" = 1 ]; then
        USER="<path:secret/cluster${1}_admin/git#username>" PASS="<path:secret/cluster${1}_admin/git#password>" yq '
          .stringData.username=strenv(USER) | .stringData.password=strenv(PASS) |
          .metadata.annotations["avp.kubernetes.io/kv-version"] = "1"
        ' -i "${lab_dir}/meta/git-cred$i.yaml"
    fi
    # DHCP mode without network support in multi tenant mode
    if [ "$ipam" = 0 ] && [ "$switch" = 0 ] && [ "$mode_multi_tenant" = 1 ] && [ "$hybrid" = 0 ]; then
        cp "${ROOT_DIR}/gogs-data/cluster-def/ippool.yaml" "${cluster_root}/"
        yq '.resources += ["ippool.yaml"]' -i "${cluster_root}/kustomization.yaml"
        START="192.168.133.$((32+(i-1)*64))" END="192.168.133.$((95+(i-1)*64))" yq '
            .spec.pools[0].start = strenv(START) |
            .spec.pools[0].end = strenv(END)
        ' -i "${cluster_root}/ippool.yaml"
    fi
    # Broker mode
    if [ "$redfish_broker" = 1 ]; then
        if [ "$vault" = 0 ]; then
            yq '.resources += ["broker-cm.yaml"]' -i "${cluster_root}/kustomization.yaml"
            CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '.data.broker_ca = strenv(CA)' -i "${cluster_root}/broker-cm.yaml"
        fi
        if [ "$switch" = 0 ]; then
            yq 'del(.spec.networkList)' -i "${cluster_root}.yaml"
        fi
    else
        yq 'del(.spec.poolUserList) | del(.spec.networkList)' -i "${cluster_root}.yaml"
    fi

    if [ "$hybrid_kubevirt" = 1 ]; then
        yq '.spec.multusNetworks = [{"bridge": "br0", "name": "k8s"}]' -i "${cluster_root}.yaml"
    fi
    # Pivot. Adds information about the cluster
    if [ "$pivot_enabled" == 1 ]; then
        # if we use the switch. The interface is a Vlan otherwise it is the direct interface
        if [ "$switch" == 1 ]; then
            itf="vlan${vlan}"
        else
            itf="ens3"
        fi
        ITF="$itf" yq '
            .spec.pivotInfo.pivot = false |
            .spec.pivotInfo.kanodName = "kanod" |
            .spec.pivotInfo.kanodNamespace = "kanod" |
            .spec.pivotInfo.ironicItf = env(ITF)
        ' -i "${cluster_root}.yaml"
        if [ "$redfish_broker" == 1 ]; then
            BMPOOL="baremetalpool-$i" yq '.spec.pivotInfo.bareMetalPoolList = [env(BMPOOL)]' -i "${cluster_root}.yaml"
            if [ "$switch" == 1 ]; then
                PXE="pxe-network-$i" K8S="default-network-$i"  yq '.spec.pivotInfo.networkList = [strenv(PXE), strenv(K8S)]' -i "${cluster_root}.yaml"
            fi
            if [ "$ipam" = 0 ] && [ "$switch" = 0 ]; then
                DHCPCONFIG="cluster${i}-subnet" yq '
                    .spec.pivotInfo.dhcpConfigList = [strenv(DHCPCONFIG)]
                ' -i "${cluster_root}.yaml"
            fi
            if [ "$switch" = 1 ]; then
                DHCPCONFIG_PXE="pxe-network-$i" DHCPCONFIG_DEFAULT="default-network-$i" yq '
                    .spec.pivotInfo.dhcpConfigList = [strenv(DHCPCONFIG_PXE), strenv(DHCPCONFIG_DEFAULT)]
                ' -i "${cluster_root}.yaml"
            fi
        else
            if [ "$switch" = 0 ] && [ "$hybrid" = 0 ]; then
                if [ "$ipam" = 0 ]; then
                    DHCPCONFIG="cluster${i}-subnet" yq '
                        .spec.pivotInfo.dhcpConfigList = [strenv(DHCPCONFIG)]
                        ' -i "${cluster_root}.yaml"
                else
                    IPPOOL="pxe-ippool" yq '
                        .spec.pivotInfo.ippoolList = [strenv(IPPOOL)]
                        ' -i "${cluster_root}.yaml"
                fi
            fi
        fi
        if [ "$byoh_in_pool" == 1 ]; then
            yq -i e '.spec.pivotInfo.byoHostPoolUsed = true' -i "${cluster_root}.yaml"
        fi

    fi

    if [ "$mode_multi_tenant" == 1 ]; then
        yq  '.spec.multiTenant = true' -i "${cluster_root}.yaml"
    else
        yq '.spec.multiTenant = false' -i "${cluster_root}.yaml"
    fi

    # In switch mode, correct the ci-data associated to clusterdef
    if [ "$switch" = 1 ]; then
        if [ "$kamaji_enabled" = 1 ]; then
            VLAN="$vlan" yq '
                .data.ippool_start="192.168." + strenv(VLAN) + ".2" |
                .data.ippool_end="192.168." + strenv(VLAN) + ".128" |
                .data.gateway="192.168." + strenv(VLAN) + ".1"
            ' -i "${lab_dir}/meta/ci-data$i.yaml"
        else
            VLAN="$vlan" yq '
                .data.cluster_endpoint="192.168." + strenv(VLAN) + ".200" |
                .data.ippool_start="192.168." + strenv(VLAN) + ".2" |
                .data.ippool_end="192.168." + strenv(VLAN) + ".128" |
                .data.gateway="192.168." + strenv(VLAN) + ".1"
            ' -i "${lab_dir}/meta/ci-data$i.yaml"
        fi
    fi
    # with kamaji, configure mettallb to access the controlplane
    if [ "$kamaji_enabled" = 1 ]; then
        metallb_kamaji="kamaji-metallb-cluster$i.yaml"
        cp "${ROOT_DIR}/gogs-data/cluster-def/kamaji-metallb-config.yaml" "${lab_dir}/meta/${metallb_kamaji}"
        cluster_endpoint=$(yq '.data.cluster_endpoint' "${lab_dir}/meta/ci-data$i.yaml")
        CP_IP="${cluster_endpoint}/32" yq 'select(document_index==0).spec.addresses += [env(CP_IP)]' -i "${lab_dir}/meta/${metallb_kamaji}"
        NAME="addresspool-for-kamaji-cluster$i" yq 'select(document_index==0).metadata.name = env(NAME)' -i "${lab_dir}/meta/${metallb_kamaji}"
        NAME="advert-for-kamaji-cluster$i" yq 'select(document_index==1).metadata.name = env(NAME)' -i "${lab_dir}/meta/${metallb_kamaji}"
        NAME="addresspool-for-kamaji-cluster$i" yq 'select(document_index==1).spec.ipAddressPools = [env(NAME)]' -i "${lab_dir}/meta/${metallb_kamaji}"
        NAME="$metallb_kamaji" yq '.resources += env(NAME)' -i "${lab_dir}/meta/kustomization.yaml"
    fi

    # define target namespace
    cdef_name=$(yq '.metadata.name' "${cluster_root}.yaml")
    cdef_namespace=$(yq '.metadata.namespace' "${cluster_root}.yaml")
    if [ "$mode_multi_tenant" == 1 ]; then
        target_namespace="${cdef_namespace}-${cdef_name}"
    else
        target_namespace="${cdef_namespace}"
    fi

    # In byoh in pool mode, create userdata for the baremetalhost (byohost creation)
    if [ "$byoh_enabled" = 1 ] && [ "$byoh_in_pool" = 1 ]; then
        pool_byoh_root="${lab_dir}/meta/pool-byoh-$i"
        cp -r "${ROOT_DIR}/gogs-data/cluster-def/pool-byoh-$i" "${lab_dir}/meta/."

        # userData for baremetalhost
        cp "${ROOT_DIR}/templates/byohost.yaml" "${pool_byoh_root}/byoh.yml"
        yq 'del(.vm)' -i "${pool_byoh_root}/byoh.yml"

        yaml_keys=$(echo "$ssh_keys"| yq -p=json)
        KEYS="${yaml_keys}" NODE_REF="${node_ref}" NAME_IMG="byoh-${node_os}-${k8s_ref}" \
            CLUSTER_NS="'{{ds.meta_data.cluster_ns}}'" NAME="'{{ds.meta_data.name}}'" BKCONFIG="'{{ds.meta_data.byoh}}'" \
            CA="$(< "${ROOT_DIR}/certs/ca.cert")" BMPOOL="baremetalpool-$i"  yq '
            .admin.keys = env(KEYS) |
            .version = env(NODE_REF) |
            .image = env(NAME_IMG) |
            .byoh.cluster_ns =  env(CLUSTER_NS) |
            .name = env(NAME) |
            .byoh.bootstrap_kubeconfig = env(BKCONFIG) |
            .nexus.certificate = strenv(CA) |
            .certificates."kanod_ca" = strenv(CA) |
            .byoh.host_labels += {"kanod.io/mode": "byoh", "kanod.io/baremetalpool": env(BMPOOL)}
        ' -i "${pool_byoh_root}/byoh.yml"

        if [ -n "$routes" ]; then
            ROUTES="$routes" yq '.network.ethernets["{{ds.meta_data.interface}}"].routes = env(ROUTES)' -i "${pool_byoh_root}/byoh.yml"
        fi

        if [ "$proxy_enabled" = 1 ]; then
            PROXY="$proxy_config" yq '.proxy = env(PROXY)' -i "${pool_byoh_root}/byoh.yml"
        fi

        if [ "$switch" == 1 ]; then
            VLAN="vlan${vlan}" ID="${vlan}" yq '
                with(. | select(has("network"));
                        .network.vlans.VLAN_NAME = .network.ethernets["{{ds.meta_data.interface}}"] * {"id": env(ID), "link": env(INTERFACE)} |
                        .network.vlans.VLAN_NAME.nameservers.addresses[0] = "192.168.133.2" |
                        .network.ethernets["{{ds.meta_data.interface}}"] = {"dhcp4": false } |
                        (.network.vlans.VLAN_NAME | key) = env(VLAN)
                    )' -i "${pool_byoh_root}/byoh.yml"
        else
            ADDRESS="['{{ds.meta_data.ip_pxe}}/{{ds.meta_data.prefix_pxe}}']" GATEWAY="'{{ds.meta_data.gateway_pxe}}'" yq '
                .network.ethernets["{{ds.meta_data.interface}}"].addresses = env(ADDRESS) |
                .network.ethernets["{{ds.meta_data.interface}}"].gateway4 = env(GATEWAY) |
                .network.ethernets["{{ds.meta_data.interface}}"].nameservers.addresses[0]= "192.168.133.1"
            ' -i "${pool_byoh_root}/byoh.yml"
        fi

        yq '.write_files.[0].content = "'"$(< "${pool_byoh_root}/byoh.yml")"'"' -i "${pool_byoh_root}/userData.yml"

        NAME="pool-byoh-$i" yq '.resources += env(NAME)' -i "${lab_dir}/meta/kustomization.yaml"

        # multi-tenancy
        # TODO: original version did not make sense. Understand why the infra admin
        # should push resources in the cluster namespace under cluster admin responsibility
        NS=${target_namespace} yq '.namespace = env(NS)' -i "${pool_byoh_root}/kustomization.yml"
    fi

    # define hostquota for target cluster
    if [ "$hybrid" = 1 ]; then 
        cp "${ROOT_DIR}/gogs-data/cluster-def/host-quota$i.yaml" "${lab_dir}/meta/"
        NAME="host-quota$i.yaml" yq '.resources += env(NAME)' -i "${lab_dir}/meta/kustomization.yaml"
        NAME="hostquota1-1" yq '.spec.hostQuotaList = [strenv(NAME)]' -i "${cluster_root}.yaml"
        NAME="hostquota1-2" yq '.spec.hostQuotaList += strenv(NAME)' -i "${cluster_root}.yaml"
    fi
}

declare_cluster 1
if [ "$multi_cluster" == 1 ]; then
    declare_cluster 2
fi


##################################
#   Cluster Git project(s)
##################################
echo '- Clusters'

mkdir -p "${lab_dir}/cluster1"

define_cluster() {
    i=$1
    vlan=$((133+i*11))
    cluster_file="${lab_dir}/cluster$1/config.yaml"
    additional_file="${lab_dir}/cluster$1/additional.yaml"
    mkdir -p "${lab_dir}/cluster$1/"
    rm "${additional_file}" &> /dev/null || true 

    if [ "${cluster_config}" != '' ]; then
        echo "  -> custom template ${cluster_config}"
        cp "${cluster_config}" "${cluster_file}"
        return
    fi
    echo '  -> standard templates'
    cp "${ROOT_DIR}/gogs-data/cluster$1/config.yaml" "${cluster_file}"
    # define os and k8s versions
    NODE_REF="${node_ref}" NODE_OS="${node_os}" K8S="${k8s_ref}" yq '
        .version = env(K8S) |
        .controlPlane.serverConfig.os_version = env(NODE_REF) |
        .controlPlane.serverConfig.os = env(NODE_OS) |
        .workers[].serverConfig.os_version = env(NODE_REF) |
        .workers[].serverConfig.os = env(NODE_OS) |
        .vmPools[].image.osVersion = env(NODE_REF)
    ' -i "${cluster_file}"

    # inject ssh keys
    KEYS="$ssh_keys" yq '
        .controlPlane.serverConfig.admin.keys = env(KEYS) |
        .workers[].serverConfig.admin.keys = env(KEYS)
    ' -i "${cluster_file}"
    # Hardwired password when vault is not used
    if [ "$vault" = 1 ]; then
        if [ "$tpm" = 1 ]; then
            yq 'del(.vault.role_id) | .vault.tpm=true' -i "${cluster_file}"
        fi
    else
        yq '
            del(.vault) |
            .controlPlane.serverConfig.admin.passwd = "secret" |
            .workers[].serverConfig.admin.passwd = "secret"
        ' -i "${cluster_file}"
    fi
    # Redefine flavours if ipam is used
    if [ "$ipam" = 1 ]; then
        yq -i '
            .controlPlane.flavour="mono-ipam" |
            .workers[].flavour="default-ipam"
        ' "${cluster_file}"
    fi
    if [ "$hybrid" = 1 ]; then
        yq -i '
            .hybrid=true |
            .controlPlane.hostSelector.matchLabels["host.kanod.io/kind"] = "baremetal" |
            .workers[0].hostSelector.matchLabels["host.kanod.io/kind"] = "baremetal"
        ' "${cluster_file}"

        if [ "$hybrid_kubevirt" = 1 ]; then
            yq -i '
                .workers[1] = .workers[0] | .workers[1].name="mdk" |
                .workers[1].hostSelector.matchLabels["host.kanod.io/kind"] = "kubevirt" |
                .workers[1].hostSelector.matchLabels["host.kanod.io/cores"] = "2" |
                .workers[1].hostSelector.matchLabels["kanod.io/interface"] = "enp1s0" |
                .workers[1].hostSelector.matchLabels["host.kanod.io/networks"] = "k8s" |
                .workers[1].flavour="default-dhcp"
            ' "${cluster_file}"
        fi

        if [ "$openstack" = 1 ]; then
            yq -i '
                .workers += .workers[0] |
                .workers[-1].name = "mdo" | 
                .workers[-1].flavour = "default-openstack" |
                with(.workers[-1].hostSelector.matchLabels;
                .["host.kanod.io/kind"] = "openstack" |
                .["host.kanod.io/openstack-cloud"] = "osp1" |
                .["host.kanod.io/openstack-networks"] = "network-1" |
                .["host.kanod.io/openstack-flavor"] = "kanod")
            ' "${cluster_file}"

            cat < "${ROOT_DIR}/gogs-data/cluster$1/openstack.yaml"  >> "${additional_file}"
        fi

        if [ "$pivot_enabled" = 1 ]; then
            yq -i '
                .controlPlane.hostSelector.matchLabels["host.kanod.io/real-kind"] = "baremetal" |
                .workers[0].hostSelector.matchLabels["host.kanod.io/real-kind"] = "baremetal" |
                .controlPlane.hostSelector.matchLabels["host.kanod.io/kind"] = "remote" |
                .workers[0].hostSelector.matchLabels["host.kanod.io/kind"] = "remote" |
                .controlPlane.hostSelector.matchLabels["host.kanod.io/remote-config"] = "remote-config" |
                .workers[0].hostSelector.matchLabels["host.kanod.io/remote-config"] = "remote-config"
            ' "${cluster_file}"

            if [ "$hybrid_kubevirt" = 1 ]; then
                yq -i '
                    .workers[1].hostSelector.matchLabels["host.kanod.io/kind"] = "remote" |
                    .workers[1].hostSelector.matchLabels["host.kanod.io/real-kind"] = "kubevirt" |
                    .workers[1].hostSelector.matchLabels["host.kanod.io/remote-config"] = "remote-config"
                ' "${cluster_file}"
            fi


            if [ "$mode_multi_tenant" == 1 ]; then
                target_namespace="capm3-cluster$1"
            else
                target_namespace="capm3"
            fi
            NS=${target_namespace} CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '
                select(document_index==0).spec.certificate = strenv(CA) |
                select(document_index==0).metadata.namespace = strenv(NS) |
                select(document_index==0).spec.secret.namespace = strenv(NS)
            ' "${ROOT_DIR}/gogs-data/cluster$1/remote.yaml"  >> "${additional_file}"
        fi
    fi

    if [ "$switch" = 1 ] && [ "$pivot_enabled" == 1 ]; then
        yq -i '
            .controlPlane.flavour="mono-ipam" |
            .workers[].flavour="default-ipam"
        ' "${cluster_file}"
    fi

    if [ "$autoscale" == 1 ]; then
        # Adpatation for autoscaler. Adds a new machine deployment with labels
        yq '
            del(.workers[].replicas) |
            .workers[1] = .workers[0] | .workers[1].name="md2" |
            .workers[].replicasBetween = {"min": 1, "max": 4} |
            .workers[0].kubeletExtraArgs.node-labels = "example.com/dep-label=md1" |
            .workers[1].kubeletExtraArgs.node-labels = "example.com/dep-label=md2"
        ' -i "${cluster_file}"
        # If the broker is used baremetalpool are automatic
        if [ "$redfish_broker" == 1 ]; then
            VAL="baremetalpool-$i" yq '
                .controlPlane.pool = strenv(VAL) | .workers[].pool = strenv(VAL) |
                del(.bareMetalPools[].replicas)
            ' -i "${cluster_file}"
        fi
    else
        # Need to give a size if no autoscaler
        if [ "$redfish_broker" == 1 ]; then
            yq '.bareMetalPools[].replicas = 2' -i "${cluster_file}"
        fi
    fi
    # Modification to go in RKE2 mode. We disable Calico
    if [ "$rke2" = 1 ]; then
        RKE_REF="${rke_machine_ref}" yq '
            .kind = "rke2" |
            .addons=[] |
            .controlPlane.serverConfig.os_version = strenv(RKE_REF) |
            .workers[].serverConfig.os_version = strenv(RKE_REF) |
            del(.controlPlane.serverConfig.os, .workers[].serverConfig.os) |
            .controlPlane.imageFormat = "raw" |
            .workers[].imageFormat = "raw"
        ' -i "${cluster_file}"
    fi
    # HA mode
    if [ "$ha_cluster" = 1 ]; then
        FLAVOUR=$flavour_kind yq '
            .controlPlane.flavour = "high-availability-" + env(FLAVOUR) |
            .controlPlane.replicas = 3 |
            .workers[].replicas = 1
        ' -i "${cluster_file}"
        # Adds enough replica to the pools when not in autoscale mode
        if [ "$autoscale" == 0 ] && [ "$redfish_broker" == 1 ]; then
            yq '.bareMetalPools[].replicas = 4' -i "${cluster_file}"
        fi
    fi
    # Broker mode
    if [ "$redfish_broker" = 1 ]; then
        # specify the bmc schema to use at pool level.
        BMC_SCHEMA="${bmc_protocol}" yq '.bareMetalPools.[].redfishSchema = env(BMC_SCHEMA)' -i "${cluster_file}"
        if [ "$switch" = 0 ]; then
            # Remove network pools definitions
            yq 'del(.networkPools)' -i "${cluster_file}"
            # blocking annotation on Kea only in (controlled) DHCP mode
            if [ "$ipam" = 0 ]; then
                yq '
                    .bareMetalPools[].blockingAnnotations = ["dhcp.kanod.io/configured"] |
                    .bareMetalPools[].labels."kanod.io/dhcp" = "pxe" |
                    .bareMetalPools[].annotations."kanod.io/ipam" = "pxe:pxe-ippool"
                ' -i "${cluster_file}"
            fi
        fi
        # TODO: if no autoscale put enough replicas in pool.
        if [ "$multi_cluster" == 1 ] && [ "$ha_cluster" == 0 ] && [ "$autoscale" == 0 ]; then
            yq -i '.bareMetalPools[].replicas = 2'  "${cluster_file}"
        fi
        # TODO: Must be moved to the BareMetalDef. Unfortunately we cannot configure
        # this parameter on the fly.
        if [ "$uefi" = 1 ]; then
            yq -i '.bareMetalPools[].bootMode = "UEFI"'  "${cluster_file}"
        fi
    else
        # Remove both network pools and bmpools
        yq 'del(.bareMetalPools) | del(.networkPools)' -i "${cluster_file}"
    fi

    if [ "$ipam" == 1 ]; then
        if [ "$switch" == 1 ]; then
            yq 'del(.ipPools[].start, .ipPools[].end, .ipPools[].prefix, .ipPools[].gateway, .ipPools[].dns)' -i "${cluster_file}"
        else
            yq 'del(.ipPools[].network)' -i "${cluster_file}"
            if [ "$redfish_broker" == 0 ]; then
                yq 'del(.ipPools[1])' -i "${cluster_file}"
            fi
        fi
    else
        if [ "$switch" == 1 ]; then
            yq 'del(.ipPools[].start, .ipPools[].end, .ipPools[].prefix, .ipPools[].gateway, .ipPools[].dns)' -i "${cluster_file}"
        else
            yq 'del(.ipPools, .ipPoolsBindings)' -i "${cluster_file}"
        fi
        if [ "$redfish_broker" == 1 ]; then
            yq 'del(.bareMetalPools[].bmhData)' -i "${cluster_file}"
        fi
    fi
    # kamaji mode: clean up the control-plane
    if [ "$kamaji_enabled" = 1 ]; then
        yq '
            .controlPlane.flavour = "kamaji" |
            .controlPlane.serverConfig = {} |
            del(.controlPlane.hostSelector)
        ' -i "${cluster_file}"
    fi
	# Byoh mode
	if [ "$byoh_enabled" = 1 ]; then
	    if [ "$byoh_provider_cp" = 1 ]; then
            yq 'with(.controlPlane; .flavour = "byoh" | .serverConfig ={} | .hostSelector.matchLabels = {"kanod.io/mode": "byoh"})' -i "${cluster_file}"
        fi
        if [ "$byoh_provider_worker" = 1 ]; then
			yq 'with(.workers[]; .flavour = "byoh" | .serverConfig ={} | .hostSelector.matchLabels = {"kanod.io/mode": "byoh"})' -i "${cluster_file}"
        fi
	fi

	# Byoh in pool mode
    if [ "$byoh_enabled" = 1 ] && [ "$byoh_in_pool" = 1 ]; then
        NODE_OS="byoh-${node_os}" NODE_REF="${node_ref}" BMPOOL="baremetalpool-$i" yq '
            .byohConfig.os = env(NODE_OS) |
            .byohConfig.osVersion = env(NODE_REF) |
            .byohConfig.imageFormat = "qcow2" |
            .byohConfig.apiServer = "https://192.168.133.10:6443" |
            .controlPlane.pool = strenv(BMPOOL) | 
            .workers[].pool = strenv(BMPOOL) |
            .bareMetalPools.[].bmhData.annotations += {"byoh": "kanod.io/byoh-bootstrapkubeconfig"} |
            .bareMetalPools.[].bmhData.annotations += {"cluster_ns": "kanod.io/byoh-cluster-namespace"} |
            .bareMetalPools.[].annotations += {"kanod.io/byoh-bootstrapkubeconfig": ""} |
            .bareMetalPools.[].annotations += {"kanod.io/byoh-cluster-namespace": ""} |
            .bareMetalPools.[].byoh.inPool = true
        ' -i "${cluster_file}"
    fi

    if [ "$byoh_provider_kubevirt" = 1 ]; then
        yq '.workers += {"name": "mdk", "flavour": "byoh", "serverConfig":{}, "replicas": 1, "hostSelector":{"matchLabels": {"kanod.io/pool": "vmpool-1", "kanod.io/mode": "byoh"}} }' -i "${cluster_file}"
    else
        yq 'del(.vmPools)' -i "${cluster_file}"
    fi
}

define_cluster 1
if [ "$multi_cluster" == 1 ]; then
    define_cluster 2
fi


##################################
#   TARGET NAMESPACE for cluster1
##################################

cluster_root="${lab_dir}/meta/cluster1"
cdef_name=$(yq '.metadata.name' "${cluster_root}.yaml")
cdef_namespace=$(yq '.metadata.namespace' "${cluster_root}.yaml")
if [ "$mode_multi_tenant" == 1 ]; then
    target_namespace="${cdef_namespace}-${cdef_name}"
else
    target_namespace="${cdef_namespace}"
fi


##################################
#   BYOH Minions
##################################

if [ "$byoh_enabled" = 1 ] && [ "$byoh_in_pool" = 0 ]; then
	echo "- BYOH Minions"
    # mode mono-cluster only with cluster1

	for (( i=1; i<=nb_byoh_hosts; i++ )); do
		byoh_config="${lab_dir}/byohost-${i}/config.yaml"
		address_host="192.168.133.$((120+i))"
		mkdir -p "${lab_dir}/byohost-${i}"
		cp "${ROOT_DIR}/templates/byohost.yaml" "${byoh_config}"
		ADDR="${address_host}/24" NAME="byohost-${i}" KEYS="$ssh_keys" \
		NODE_REF="${node_ref}" NAME_IMG="byoh-${node_os}-${k8s_ref}" \
		NS=${target_namespace} \
		CA="$(< "${ROOT_DIR}/certs/ca.cert")" yq '
			.network.ethernets.ens3.addresses[0] = strenv(ADDR) |
			.network.ethernets.ens3.gateway4 = "192.168.133.1" |
			.name = strenv(NAME) |
			.admin.keys = env(KEYS) |
			.nexus.certificate = strenv(CA) |
			.certificates."kanod_ca" = strenv(CA) |
			.byoh.cluster_ns =  strenv(NS) |
			.version = strenv(NODE_REF) |
			.image = strenv(NAME_IMG) |
			.byoh.host_labels += {"kanod.io/mode": "byoh"}
		' -i "${byoh_config}"


		if [ "$proxy_enabled" = 1 ]; then
            PROXY=${proxy_config} yq '.proxy = env(PROXY)' -i "${byoh_config}"
		fi
		if [ -n "$routes" ]; then
			ROUTES="$routes" yq '.network.ethernets.ens3.routes = env(ROUTES)' -i "${byoh_config}"
		fi
	done
fi

mkdir -p "${lab_dir}/kubeconfig"

echo '- Parameters for shell scripts'
cat > "${KIAB_CONF:-${HOME}/.kiab_conf}" <<EOF
ROOT_DIR=${ROOT_DIR}
LAB_DIR=${lab_dir}
GIT_URL=${git_url}
KANOD_BOOT_REF=${kanod_boot_ref}
KANOD_BUILDER_REF=${kanod_builder_ref}
KANOD_VAULT_TPM_REF=${vault_tpm_ref}
KANOD_SWITCH_REF=${switch_ref}
KANOD_TPM_REF=${tpm_ref}
KANOD_NODE_REF=${node_ref}
K8S_REF=${k8s_ref}
NB_VM=${minions}
KANOD_VAULT=${vault}
NODE_OS=${node_os}
INFRA_USER=infra_admin
INFRA_PASSWORD=secret1
CLUSTER_USER=cluster1_admin
CLUSTER2_USER=cluster2_admin
CLUSTER_PASSWORD=secret2
BMC_PROTOCOL=${bmc_protocol}
BMC_HOST=192.168.133.1
BMC_PASSWORD=secret
STORAGE_POOL=${storage_pool}
TPM=${tpm}
MULTI_CLUSTER=${multi_cluster}
MINION_SIZE=${minion_size}
SERVICE_SIZE=${service_size}
TARGET_NAMESPACE=${target_namespace}
AIRGAP=${airgap}
REDFISH_BROKER=${redfish_broker}
BROKER_AUTH=${broker_auth}
PIVOT_ENABLED=${pivot_enabled}
K8S_UPGRADE_ENABLED=${k8s_upgrade_enabled}
SCALING_ENABLED=${scaling_enabled}
GOGS_IP=git.service.kanod.home.arpa
KANOD_SWITCH=${switch}
OPENSTACK=${openstack}
IPAM=${ipam}
AUTOSCALE=${autoscale}
KAMAJI=${kamaji_enabled}
BYOH=${byoh_enabled}
NB_BYOHOST=${nb_byoh_hosts:-0}
HYBRID=${hybrid:-0}
BYOH_IN_POOL=${byoh_in_pool}
K8S_UPGRADED_VERSION=${k8s_upgraded_version:-}
KUBEVIRT=${kubevirt}
EOF
echo -e "$(yq '.variables | to_entries | map(.key + "=" + .value) | join("\n")' "$params" )" >> "${KIAB_CONF:-${HOME}/.kiab_conf}"
