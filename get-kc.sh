#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a
CLUSTER_NAMESPACE=${CLUSTER_NAMESPACE:=$TARGET_NAMESPACE}
echo "get kubeconfig for cluster $1 in namespace ${CLUSTER_NAMESPACE}"
kubectl get "secret/$1-kubeconfig" -n "${CLUSTER_NAMESPACE}" \
    --template="{{.data.value}}" | base64 -d > "kc-$1"
