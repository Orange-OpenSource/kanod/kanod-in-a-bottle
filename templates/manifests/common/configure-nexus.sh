#!/bin/bash

#  Copyright (C) 2020-2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

base=http://nexus3:8081/service/rest

echo 'Credential initialization'

NEXUS_ADMIN_PASSWORD=$(grep '^password:' /vault/secrets/admin | sed 's/^[^:]*: *//')

MAVEN_ADMIN_USER=$(grep '^username:' /vault/secrets/maven | sed 's/^[^:]*: *//')
MAVEN_ADMIN_PASSWORD=$(grep '^password:' /vault/secrets/maven | sed 's/^[^:]*: *//')

REGISTRY_USER=$(grep '^username:' /vault/secrets/registry | sed 's/^[^:]*: *//')
REGISTRY_PASSWORD=$(grep '^password:' /vault/secrets/registry | sed 's/^[^:]*: *//')

echo 'Nexus configuration'

if [ -f "/nexus-data/configured" ]; then
    echo "nexus already configured"
    # We still wait for the API.
    while ! curl -s -f -o /dev/null "$base/v1/read-only" ; do
        echo '.'
        sleep 1
    done
    echo "API ready !"
    exit 0
fi

echo 'Waiting for initialization.'
while [ ! -f "/nexus-data/admin.password" ]; do
    echo '.'
    sleep 1
done
echo
echo 'Waiting for Rest API ready.'

while ! curl -s -f -o /dev/null "$base/v1/read-only" ; do
    echo '.'
    sleep 1
done
echo

initial=$(cat /nexus-data/admin.password)
curl -X PUT "$base/v1/security/users/admin/change-password" -u "admin:${initial}" -H "accept: application/json" -H "Content-Type: text/plain" -d "${NEXUS_ADMIN_PASSWORD}"

function nexus_delete() {
    status=$(curl -L -X DELETE -w "%{http_code}" -o /dev/null -u "admin:${NEXUS_ADMIN_PASSWORD}" -H "accept: application/json" -H "Content-Type: application/json" "$1")
    if ! [[ "$status" =~ ^2[0-9][0-9]$ ]]; then
        echo "Something went wrong [request: $1 - status $status]"
    fi
}

function nexus_get() {
    curl -L -X GET -u "admin:${NEXUS_ADMIN_PASSWORD}" -H "accept: application/json" -H "Content-Type: application/json" "$1"
}

function nexus_post() {
    status=$(curl -X POST -u "admin:${NEXUS_ADMIN_PASSWORD}"  -w "%{http_code}" -o /dev/null -H "accept: application/json" -H "Content-Type: application/json" -d @- "$1")
    if ! [[ "$status" =~ ^2[0-9][0-9]$ ]]; then
        echo "Something went wrong [POST - request: $1 - status $status]"
    fi
}

function nexus_put() {
    status=$(curl -X PUT -u "admin:${NEXUS_ADMIN_PASSWORD}"  -w "%{http_code}" -o /dev/null -H "accept: application/json" -H "Content-Type: application/json" -d @- "$1")
    if ! [[ "$status" =~ ^2[0-9][0-9]$ ]]; then
        echo "Something went wrong [PUT - request: $1 - status $status]"
    fi
}

echo '- Remove initial repositories'
for i in $(nexus_get "$base/v1/repositories" |  grep '"name"' | sed  's/[^:]*: "//' | sed 's/",$//') ; do
    if [ "$i" == "kanod" ] || [ "$i" == "odoc" ]; then
        continue
    fi
    nexus_delete "$base/v1/repositories/$i"
done

echo '- Create a blobstore for maven'
nexus_post "$base/v1/blobstores/file" <<EOF
{

  "path": "mvn_store",
  "name": "mvn_store"
}
EOF

echo '- Create a blobstore for docker'
nexus_post "$base/v1/blobstores/file" <<EOF
{

  "path": "docker_store",
  "name": "docker_store"
}
EOF

echo '- Create a (hosted) maven repository'
nexus_post "$base/v1/repositories/maven/hosted" <<EOF
{
    "name": "kanod",
    "online": true,
    "storage": {
        "blobStoreName": "mvn_store",
        "strictContentTypeValidation": false,
        "writePolicy": "allow"
    },
    "cleanup": null,
    "maven": {
        "versionPolicy": "MIXED",
        "layoutPolicy": "PERMISSIVE"
    }
}
EOF

echo '- Create a (hosted) docker repository'
nexus_post "${base}/v1/repositories/docker/hosted" <<EOF
{
    "name": "odoc",
    "online": true,
    "storage": {
        "blobStoreName": "docker_store",
        "strictContentTypeValidation": true,
        "writePolicy": "allow"
    },
    "cleanup": null,
    "docker": {
        "v1Enabled": false,
        "forceBasicAuth": false,
        "httpPort": 8082,
        "httpsPort": null
    }
}
EOF

echo '- create docker admin role'
nexus_post "${base}/v1/security/roles" <<EOF
{
    "id": "docker-admin",
    "source": "default",
    "name": "docker-admin",
    "description": "role to push container on docker registry",
    "privileges": [
      "nx-repository-view-docker-odoc-*",
      "nx-repository-admin-docker-odoc-*"],
    "roles": []
}
EOF

echo '- create maven admin role'
nexus_post "${base}/v1/security/roles" <<EOF
{
    "id": "maven-admin",
    "source": "default",
    "name": "maven-admin",
    "description": "role to push container on docker registry",
    "privileges": [
      "nx-repository-admin-maven2-kanod-*",
      "nx-repository-view-maven2-kanod-*"],
    "roles": []
}
EOF

echo '- create maven admin user'
nexus_post "${base}/v1/security/users" <<EOF
{
    "userId": "${MAVEN_ADMIN_USER}",
    "firstName": "maven",
    "lastName": "manager",
    "emailAddress": "none@localhost.localdomain",
    "password": "${MAVEN_ADMIN_PASSWORD}",
    "status": "active",
    "roles": [ "maven-admin" ]
}
EOF

echo '- create docker admin user'
nexus_post "${base}/v1/security/users" <<EOF
{
    "userId": "${REGISTRY_USER}",
    "firstName": "docker",
    "lastName": "manager",
    "emailAddress": "none@localhost.localdomain",
    "password": "${REGISTRY_PASSWORD}",
    "status": "active",
    "roles": [ "docker-admin" ]
}
EOF

echo '- Fix the list of active realms to include docker'
nexus_put "${base}/v1/security/realms/active" <<EOF
[
  "NexusAuthenticatingRealm",
  "NexusAuthorizingRealm",
  "DockerToken"
]
EOF
touch "/nexus-data/configured"

if [ -f "/nexus-data/admin.password" ]; then
    rm "/nexus-data/admin.password"
fi

echo '- Enable anonymous user access ()'
nexus_put "${base}/v1/security/anonymous" <<EOF
{
  "enabled": true,
  "userId": "anonymous",
  "realmName": "NexusAuthorizingRealm"
}
EOF
