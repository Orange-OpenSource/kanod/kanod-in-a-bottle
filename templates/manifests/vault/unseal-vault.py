#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from pathlib import Path
import requests
import time

BASE_URL = 'http://vault.vault.svc.service.local:8200/v1'
KEY_PATH = '/data/vault_key'
noproxies = {"http": "", "https": ""}
YEAR = 3600 * 24 * 365


def wait_for_vault():
    print('wait for vault')
    while True:
        try:
            print(f'running : {BASE_URL}/sys/health')
            code = requests.get(
                f'{BASE_URL}/sys/health',  proxies=noproxies).status_code
            print(code)
            if code == 200 or code == 501 or code == 503:
                print('vault is available...')
                break
            print(code)
        except Exception as e:
            print(str(e))
            pass
        print('.', flush=True)
        print('sleeping...')
        time.sleep(5)


def unseal_vault():
    print('check if vault is sealed\n')
    key_path = Path(KEY_PATH)

    key_path = Path(KEY_PATH)
    if not key_path.is_file():
        print('* missing vault key (file {KEY_PATH})')
        return

    req = requests.get(
        f'{BASE_URL}/sys/health',  proxies=noproxies)
    code = req.status_code
    if not (code == 200 or code == 501 or code == 503):
        print('unexpected return code during request')
        print('request : {BASE_URL}/sys/health')
        print(f'return code : {code}')
        return

    print('request response : ' + str(req.json()))

    if req.json()['sealed'] is False:
        print('vault is not sealed')
    else:
        print('vault is sealed')
        print('* unsealing vault')
        with open(key_path, 'r') as fd:
            key = fd.readline()
        req = requests.put(
            f'{BASE_URL}/sys/unseal', proxies=noproxies,
            json={'key': key[:64]}
        )
        if req.status_code != 200 or req.json()['progress'] != 0:
            print('Cannot unseal the vault' + str(req.json()))


def main():
    wait_for_vault()
    unseal_vault()


main()
