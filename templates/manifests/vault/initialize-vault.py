#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from pathlib import Path
import requests
import shutil
import stat
import time
import yaml

BASE_URL = 'http://vault.vault.svc.service.local:8200/v1'
noproxies = {"http": "", "https": ""}
YEAR = 3600 * 24 * 365

def initialize_auths(token):
    print('Setting up auth methods')
    for kind in ['userpass', 'approle']:
        req = requests.post(
            f'{BASE_URL}/sys/auth/{kind}',
            headers={'X-Vault-Token': token},
            json={'type': kind}, proxies=noproxies)
        if req.status_code != 204:
            print(
                f'failed to initialize auth method {kind} '
                f'({req.status_code})')


def initialize_engines(token):
    print('Setting up engines')
    req = requests.post(
        f'{BASE_URL}/sys/mounts/secret',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'type': 'kv', 'options': {'version': '1'}})
    if req.status_code != 204:
        print(f'failed to initialize secret ({req.status_code})')
    req = requests.post(
        f'{BASE_URL}/sys/mounts/kv',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'type': 'kv', 'options': {'version': '2'}})
    if req.status_code != 204:
        print(f'failed to initialize kv ({req.status_code})')
    for endpoint in ['pki', 'pkiroot']:
        req = requests.post(
            f'{BASE_URL}/sys/mounts/{endpoint}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={'type': 'pki', 'max_lease_ttl': YEAR})
        if req.status_code != 204:
            print(f'failed to initialize pki ({req.status_code})')


def initialize_ca(conf, token):
    print('initializing CA')
    issuer = conf.get('ca', None)
    if issuer is None:
        print('No CA to configure')
        return

    req = requests.post(
        f'{BASE_URL}/pkiroot/config/ca',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'pem_bundle': issuer}
    )
    if req.status_code != 204:
        print(
            f'failed to set root ca ({req.status_code}):'
            f' {req.content.decode("utf-8")}')
    req = requests.post(
        f'{BASE_URL}/pki/intermediate/generate/internal',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={
            'common_name': 'kanod-intermediate',
            'ttl': YEAR, 'max_ttl': YEAR}
    )
    if req.status_code != 200:
        print(
            'failed to generate intermediate'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return
    csr = req.json().get('data', {}).get('csr', None)
    if csr is None:
        print('pki: did not find the intermediate csr')
        return
    req = requests.post(
        f'{BASE_URL}/pkiroot/root/sign-intermediate',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={
            'common_name': 'kanod-intermediate', 'csr': csr,
            'ttl':  YEAR, 'max_ttl': YEAR}
    )
    if req.status_code != 200:
        print(
            'failed to sign intermediate csr'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return
    data = req.json().get('data', {})
    cert = data.get('certificate', '')
    issuer = data.get('issuing_ca', '')
    full_chain = cert + '\n' + issuer
    req = requests.post(
        f'{BASE_URL}/pki/intermediate/set-signed',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'certificate': full_chain}
    )
    if req.status_code != 204:
        print(
            'Failed to set intermediate cert'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return


def initialize_roles(conf, token, use_tpm=False):
    print('Setting up application roles')
    roles = conf.get('approles', [])
    if use_tpm:
        roles.append({
            'name': 'tpm_auth',
            'policy': '''
              path "kv/data/machines/*" {
                  capabilities = ["read"]
              }
              path "auth/approle/role/+/secret-id" {
                  capabilities = ["create", "update"]
              }
            '''
        })
    roles.append({
        'name': 'vault',
        'policy': '''
           path "auth/approle/role/*" {
               capabilities = ["read","update","create"]
           }
        '''
    })
    for role in roles:
        name = role.get('name', None)
        id = role.get('id', None)
        policy = role.get('policy', None)
        extends = role.get('extends', False)
        if name is None:
            print('need name for each approle')
            continue
        default_policy = f'''
            path "secret/{name}/*" {{ capabilities = ["read"] }}
            path "kv/data/{name}/*" {{ capabilities = ["read"] }}
            path "pki/issue/{name}" {{
                capabilities = ["read","update","create"]
            }}
            path "pki/ca/pem" {{ capabilities = ["read"] }}
            '''
        if policy is None:
            policy = default_policy
        else:
            if extends:
                policy = default_policy + policy
        req = requests.put(
            f'{BASE_URL}/sys/policies/acl/{name}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={'policy': policy}
        )
        if req.status_code != 204:
            print(
                f'failed to create policy {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        req = requests.put(
            f'{BASE_URL}/auth/approle/role/{name}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={
                'token_policies': name,
                'token_ttl': '1h',
                'token_max_ttl': '1h'
            })
        if req.status_code != 204:
            print(
                f'failed to bind approle {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        req = requests.post(
            f'{BASE_URL}/pki/roles/{name}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={'allow_any_name': True, 'ou': 'kanod'}
        )
        if req.status_code != 204:
            print(
                f'failed to create pki role for {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        if id is not None:
            req = requests.post(
                f'{BASE_URL}/auth/approle/role/{name}/role-id',
                headers={'X-Vault-Token': token}, proxies=noproxies,
                json={'role_id': id}
            )
            if req.status_code != 204:
                print(
                    f'failed to bind role-id {id} to role {name} '
                    f'({req.status_code}): {req.content.decode("utf-8")}')


def initialize_k8s(conf, token):
    for k8s_spec in conf.get('kubernetes', []):
        name = k8s_spec.get('name', None)
        is_local= k8s_spec.get('local', False)
        if name is None:
            print('Kubernetes entry without name. Skipping it')
            continue
        print(f'Setting up kubernetes cluster {name}')
        req = requests.post(
            f'{BASE_URL}/sys/auth/kubernetes/{name}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={'type': 'kubernetes'})
        if req.status_code != 204:
            print(
                f'failed to initialize kubernetes method for cluster {name} '
                f'({req.status_code})')
            continue
        if is_local:
            data = {
                'kubernetes_host': 'https://kubernetes.default.svc.service.local'
            }
            req = requests.post(
                f'{BASE_URL}/auth/kubernetes/{name}/config',
                headers={'X-Vault-Token': token}, proxies=noproxies,
                json=data)
            if req.status_code != 204:
                print(
                    f'failed to configure local cluster'
                    f'({req.status_code})')
                continue
        roles = k8s_spec.get("roles", [])
        for role in roles:
            role_name = role.get('name', None)
            serviceaccount = role.get('account', 'default')
            namespace = role.get('namespace', 'default')
            policy = role.get('policy', None)
            extends = role.get('extends', False)
            default_policy = f'''
              path "secret/{name}/{role_name}/*" {{ capabilities = ["read"] }}
              path "kv/data/{name}/{role_name}/*" {{ capabilities = ["read"] }}
              path "pki/ca/pem" {{ capabilities = ["read"] }}
              '''
            if policy is None:
                policy = default_policy
            else:
                if extends:
                    policy = default_policy + policy
            req = requests.put(
                f'{BASE_URL}/sys/policies/acl/{name}-{role_name}',
                headers={'X-Vault-Token': token}, proxies=noproxies,
                json={'policy': policy}
            )
            if req.status_code != 204:
                print(
                    f'failed to create policy for k8s {name}-{role_name} '
                    f'({req.status_code}): {req.content.decode("utf-8")}')
            req = requests.post(
                f'{BASE_URL}/auth/kubernetes/{name}/role/{role_name}',
                headers={'X-Vault-Token': token}, proxies=noproxies,
                json={
                    'bound_service_account_names': [serviceaccount],
                    'bound_service_account_namespaces': [namespace],
                    'token_policies': [f'{name}-{role_name}']
                }
            )


def initialize_secrets(conf, token):
    print('iniitialize secrets')
    for spec in conf.get('kv1', []):
        path = spec.get('path', None)
        value = spec.get('value', None)
        if path is None or value is None:
            continue
        req = requests.post(
            f'{BASE_URL}/secret/{path}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json=value)
        if req.status_code != 204:
            print(
                f'failed to set secret at {path} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
    for spec in conf.get('kv2', []):
        path = spec.get('path', None)
        value = spec.get('value', None)
        if path is None or value is None:
            continue
        req = requests.post(
            f'{BASE_URL}/kv/data/{path}',
            headers={'X-Vault-Token': token}, proxies=noproxies,
            json={'data': value})
        if req.status_code != 200:
            print(
                f'failed to set secret at {path} '
                f'({req.status_code}): {req.content.decode("utf-8")}')


def initialize_tpm_auth(conf, token):
    print(f'Setting up TPM authenticator')
    req = requests.get(
        f'{BASE_URL}/auth/approle/role/tpm_auth/role-id',
        headers={'X-Vault-Token': token}, proxies=noproxies)
    if req.status_code != 200:
        print(f'cannot get role-id for tpm_auth')
        return
    role_id = req.json().get('data', {}).get('role_id')
    req = requests.post(
        f'{BASE_URL}/auth/approle/role/tpm_auth/secret-id',
        headers={'X-Vault-Token': token},  proxies=noproxies)
    secret_id = req.json().get('data', {}).get('secret_id')
    url = BASE_URL.replace('/v1', '')
    path = '/data/tpm-auth.config'
    with open(path, 'r', encoding='utf-8') as fd:
        fd.write(f'ROLE_ID={role_id}\n')
        fd.write(f'SECRET_ID={secret_id}\n')
        fd.write(f'VAULT_ADDR={url}\n')
    os.chmod(path, 0o400)
    shutil.chown(path, user="tpmauth", group="tpmauth")


def initialize_admin(conf, token):
    print('Setting up admin user')
    admin_conf = conf.get('admin', {})
    password = admin_conf.get('password', None)
    policy = admin_conf.get('policy', None)
    if password is None or policy is None:
        print('Need password and policy for admin')
        return
    req = requests.put(
        f'{BASE_URL}/sys/policies/acl/adminpol',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'policy': policy}
    )
    if req.status_code != 204:
        print(
            'failed to create policy for admin '
            f'({req.status_code}): {req.content.decode("utf-8")}')
    req = requests.post(
        f'{BASE_URL}/auth/userpass/users/admin',
        headers={'X-Vault-Token': token}, proxies=noproxies,
        json={'password': password, 'policies': ['adminpol']}
    )
    if req.status_code != 204:
        print(
            'failed to create admin '
            f'({req.status_code}: {req.content.decode("utf-8")})')


def initialize_vault():
    print('Initializing vault\n')
    datapath = Path('/data/vault-data')
    key_path = Path('/data/vault_key')
    token_path = Path('/data/vault_token')
    token = None
    if not key_path.is_file():
        print('* Bootstrap store')
        if not datapath.is_dir():
            datapath.mkdir(mode=stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        shutil.chown(datapath, user=100, group=1000)
        req = requests.put(
            f'{BASE_URL}/sys/init', proxies=noproxies,
            json={'secret_shares': 1, 'secret_threshold': 1})
        if req.status_code != 200:
            print('Cannot initialize Vault')
            return
        content = req.json()
        key = content['keys'][0]
        token = content['root_token']
        print('* Export keys')
        with open(key_path, 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(token_path, 'w') as fd:
            fd.write(token)
            fd.write('\n')
    if key_path.is_file():
        print('* Unsealing')
        with open(key_path, 'r') as fd:
            key = fd.readline()
        req = requests.put(
            f'{BASE_URL}/sys/unseal', proxies=noproxies,
            json={'key': key[:64]}
        )
        if req.status_code != 200 or req.json()['progress'] != 0:
            print('Cannot unseal the vault' + str(req.json()))
            return
        return token

def wait_for_vault():
    while True:
        try:
            code = requests.get(
                f'{BASE_URL}/sys/health',  proxies=noproxies).status_code
            print(code)
            if code == 200 or code == 501 or code == 503:
                break
            print(code)
        except Exception as e:
            print(str(e))
            pass
        print('.', flush=True)
        time.sleep(5)

def configure_vault_service(vault_conf):
    wait_for_vault()
    token = initialize_vault()
    use_tpm = os.path.exists('/usr/local/bin/tpm-auth')
    if token is not None:
        initialize_auths(token)
        initialize_engines(token)
        initialize_ca(vault_conf, token)
        initialize_roles(vault_conf, token, use_tpm)
        initialize_k8s(vault_conf, token)
        initialize_admin(vault_conf, token)
        initialize_secrets(vault_conf, token)
        if use_tpm:
            initialize_tpm_auth(vault_conf, token)


def main():
    with open('/init/vault-init.yaml') as file:
        config = yaml.load(file, Loader=yaml.SafeLoader)
    configure_vault_service(config)

main()