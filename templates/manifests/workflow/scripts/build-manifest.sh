#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

cd /app
# shellcheck disable=SC1091
. /app/env
if [ "${NO_UPLOAD:-0}" = '1' ]; then
    echo 'Already there'
    exit 0
fi

KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-4.5.4}
REGISTRY_PREFIX=registry.gitlab.com/orange-opensource/kanod
echo '- Loading kustomize'
curl -L -o kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/"download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
tar -zxvf kustomize.tgz kustomize
rm kustomize.tgz
cd build/config/manager
/app/kustomize edit set image "controller=${REGISTRY_PREFIX}/${PROJECT}:${VERSION}"
cd ../../..
/app/kustomize build /app/build/config/default -o /app/manifest.yaml
