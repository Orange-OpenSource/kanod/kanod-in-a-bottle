#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

cd /app
version="v$(python3 -c 'from importlib.metadata import version; print(version("kanod_registrar"))')"
artifact=registrar
if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${artifact}/${version}/${artifact}-${version}.qcow2" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi

cat > /app/env <<EOF
export FORMAT=qcow2
export ARTIFACT=${artifact}
export IMAGE_VERSION=${version}
EOF
kanod-image-builder  -s os_name="${artifact}" -s os_version="${version}" kanod_registrar
