#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

cd /app || exit 1
# shellcheck source=/dev/null
. /app/env

if [ "${NO_UPLOAD:-0}" -eq 1 ]; then
    echo "No upload"
    exit 0
fi

MAVEN_ADMIN_USER=$(grep '^username:' /vault/secrets/maven | sed 's/^[^:]*: *//')
MAVEN_ADMIN_PASSWORD=$(grep '^password:' /vault/secrets/maven | sed 's/^[^:]*: *//')

mkdir -p .m2
sed -e "s/MAVEN_ADMIN_USER/${MAVEN_ADMIN_USER}/" -e "s/MAVEN_ADMIN_PASSWORD/${MAVEN_ADMIN_PASSWORD}/"  /settings/settings.xml > .m2/settings.xml

# shellcheck disable=SC2086
mvn -s .m2/settings.xml --batch-mode -B deploy:deploy-file \
    -Dmaven.repo.local=.m2/repository \
    -DgroupId=kanod -DartifactId="${ARTIFACT}" \
    -Dversion="${VERSION}" -Dtype="${TYPE:-yaml}" -Dfile="${MANIFEST}" \
    -DrepositoryId=kanod -Durl="${REPO_URL}" ${ADDITIONAL:-}
