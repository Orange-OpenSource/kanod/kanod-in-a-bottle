#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

cd /app
version=$(python3 -c 'from importlib.metadata import version; print(version("kanod_node"))')

# shellcheck disable=SC2034
use_tpm=''
if [ "${ARTIFACT}" = '-' ]; then
    ARTIFACT=${OS_RELEASE}-${K8S_RELEASE}
fi
url="${REPO_URL}/kanod/${ARTIFACT}/${version}/${ARTIFACT}-${version}.qcow2"
echo "Checking ${url}"
if curl --output /dev/null --silent --head --fail "$url" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi
cat > /app/env <<EOF
export FORMAT=qcow2
export ARTIFACT=${ARTIFACT}
export IMAGE_VERSION=${version}
EOF


echo "DEBUG url=$url"
echo "DEBUG OS_RELEASE=$OS_RELEASE"
echo "DEBUG K8S_RELEASE=$K8S_RELEASE"
echo "DEBUG ARTIFACT=$ARTIFACT"
echo "DEBUG OPTIONS=$OPTIONS"
echo "DEBUG version=$version"

# shellcheck disable=SC2086
kanod-image-builder kanod_node -s k8s_release="${K8S_RELEASE}" -s target=ubuntu \
    -s container_runtime=containerd -s release="${OS_RELEASE}"  -s os_name="${ARTIFACT}" -s os_version="${version}" ${OPTIONS}
