#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

ARTIFACT=ironic-python-agent
ARTIFACT_VERSION=1.0.0
if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${ARTIFACT}/${ARTIFACT_VERSION}/${ARTIFACT}-${ARTIFACT_VERSION}.tar" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi

curl "https://tarballs.opendev.org/openstack/ironic-python-agent/dib/${IPA_NAME}.tar.gz" -o /app/${ARTIFACT}.tar
cd /app
tar -xvf ${ARTIFACT}.tar
rm -rf "${IPA_NAME}.d"
mv "${IPA_NAME}".kernel ${ARTIFACT}.kernel
mv "${IPA_NAME}".initramfs ${ARTIFACT}.initramfs
ls -l

cat > /app/env <<EOF
export ARTIFACT=${ARTIFACT}
export VERSION=${ARTIFACT_VERSION}
export MANIFEST=/app/${ARTIFACT}.tar
export TYPE=tar
export ADDITIONAL='-Dfiles=/app/ironic-python-agent.initramfs,/app/ironic-python-agent.kernel -Dtypes=initramfs,kernel -Dclassifiers=initramfs,kernel'
EOF
