#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${PROJECT}/${VERSION}/${PROJECT}-${VERSION}.yaml" ; then
    echo 'Manifest already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi
cat > /app/env <<EOF
export ARTIFACT=${PROJECT}
export VERSION=${VERSION}
export MANIFEST=/app/manifest.yaml
EOF
git clone "https://${KANOD_GROUP_URL}/${PROJECT}.git" /app/build && cd /app/build && git checkout "${VERSION}"
