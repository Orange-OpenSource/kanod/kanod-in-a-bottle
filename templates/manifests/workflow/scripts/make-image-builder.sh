#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

# shellcheck disable=SC2155
export REGISTRY_USER="$(grep '^username:' /vault/secrets/registry | sed 's/^[^:]*: *//')"
# shellcheck disable=SC2155
export REGISTRY_PASSWORD="$(grep '^password:' /vault/secrets/registry | sed 's/^[^:]*: *//')"

cp "/dockerfiles/Dockerfile-${OS_NAME}" /kaniko/Dockerfile
if [ -f "/dockerfiles/init-${OS_NAME}.sh" ]; then
cp "/dockerfiles/init-${OS_NAME}.sh" /kaniko
fi
if [ -n "${REGISTRY_MIRROR:-}" ]; then
mirror_config="--skip-tls-verify-registry ${REGISTRY_MIRROR} --registry-mirror ${REGISTRY_MIRROR}"
else
mirror_config=''
fi

mkdir -p /kaniko/.docker
echo "{\"auths\":{\"${REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${REGISTRY_USER}" "${REGISTRY_PASSWORD}" | base64 | tr -d "\n")\"}}}" > /kaniko/.docker/config.json
# shellcheck disable=SC2086
/kaniko/executor --insecure $mirror_config \
    --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy \
    --build-arg REGISTRY  --build-arg COMMON_TAG="${TAG}" $ARGS \
    --context dir:///kaniko \
    --destination "${REGISTRY}/orange-opensource/kanod/kanod-diskimage-launcher/$OS_NAME-builder:$TAG"
