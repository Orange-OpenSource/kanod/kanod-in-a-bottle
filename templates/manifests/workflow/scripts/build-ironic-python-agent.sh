#!/bin/bash
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

ARTIFACT=ironic-python-agent-glean
VERSION=1.0.0
APP_DIR=/app
BUILD_DIR=/tmp/dib

if [ -n "${http_proxy:-}" ]; then
    echo "http_proxy=$http_proxy" >> /etc/environment
fi
if [ -n "${https_proxy:-}" ]; then
    echo "https_proxy=$https_proxy" >> /etc/environment
fi
if [ -n "${no_proxy:-}" ]; then
    echo "no_proxy=$no_proxy" >> /etc/environment
fi

echo '********************** Installing necessary tools ***********************'
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
apt-get install -y sudo git curl kpartx cpio

if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${ARTIFACT}/${VERSION}/${ARTIFACT}-${VERSION}.tar" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi

if [ "${IPA_MIRROR:-}" != "" ]; then 
    echo "***************** downloading IPA from ${IPA_MIRROR} ********************"
    mkdir -p "${BUILD_DIR}"
    cd "${BUILD_DIR}"
    curl -O "${IPA_MIRROR}/ironic-python-agent.initramfs"
    curl -O "${IPA_MIRROR}/ironic-python-agent.kernel"
    tar cvf ironic-python-agent.tar ironic-python-agent.initramfs ironic-python-agent.kernel

else
    echo '***************** Cloning Metal3 Dev Tools (Nordix) *********************'

    cd "${APP_DIR}"
    git clone https://github.com/Nordix/metal3-dev-tools
    cd metal3-dev-tools/ci/scripts/image_scripts

    # We do not use Nordix upload and we do not perform tests
    export DISABLE_UPLOAD=true
    export ENABLE_BOOTSTRAP_TEST=false
    export DIB_FILE_INJECTOR_CONFIG_DRIVE_LABEL=conf-ipa
    export DIB_SIMPLE_INIT_CONFIG_DRIVE_LABEL=conf-ipa
    export IPA_BRANCH=9.13.0

    # fix ironic lib for IPA 9.13.0
    export DIB_REPOREF_ironic_lib="stable/2024.1"
    export IRONIC_LIB_FROM_SOURCE="true"

    touch /tmp/metadata.txt

    echo '******************** Building IPA with Nordix script ********************'

    sed -i 's/IRONIC_SIZE_LIMIT_MB=.*/IRONIC_SIZE_LIMIT_MB=600/g' build_ipa.sh

    ./build_ipa.sh
    echo '*********************** Building IPA Done Cleaning **********************'

    # Clean up (not necessary)
    cd "${APP_DIR}"
    rm -rf metal3-dev-tools
fi

echo '************************ Prepare Upload of IPA **************************'
# We prepare upload on Kanod nexus.
cat > "${APP_DIR}/env" <<EOF
export ARTIFACT=${ARTIFACT}
export VERSION=${VERSION}
export MANIFEST=/${BUILD_DIR}/ironic-python-agent.tar
export TYPE=tar
export ADDITIONAL='-Dfiles=${BUILD_DIR}/ironic-python-agent.initramfs,${BUILD_DIR}/ironic-python-agent.kernel -Dtypes=initramfs,kernel -Dclassifiers=initramfs,kernel'
EOF

echo '******************* End of build-ironic-python-agent ********************'
