#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

cd /app
version=$(python3 -c 'from importlib.metadata import version; print(version("kanod_node"))')
artifact=opensuse${OS_RELEASE}-${K8S_RELEASE}
if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${artifact}/${version}/${artifact}-${version}.qcow2" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi
cat > /app/env <<EOF
export FORMAT=qcow2
export ARTIFACT=${artifact}
export IMAGE_VERSION=${version}
EOF

# shellcheck disable=SC2154
if [ "${http_proxy:-}" != "" ]; then
    cat >> /etc/environment <<EOF
http_proxy=${http_proxy}
https_proxy=${https_proxy}
no_proxy=${no_proxy}
EOF
fi

# shellcheck disable=SC2086
kanod-image-builder kanod_node -s k8s_release="${K8S_RELEASE}" -s target=opensuse \
    -s release="${OS_RELEASE}"  -s os_name="${artifact}" -s os_version="${version}" ${OPTIONS}
