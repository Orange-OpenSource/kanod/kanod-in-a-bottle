#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu
KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-4.5.4}

SUBST_URL=$(echo "$URL" | sed "s/\\\${ARTIFACT_VERSION}/${ARTIFACT_VERSION}/g")
echo "Fetching manifest $SUBST_URL"
if curl --output /dev/null --silent --head --fail "${REPO_URL}/kanod/${ARTIFACT}/${ARTIFACT_VERSION}/${ARTIFACT}-${ARTIFACT_VERSION}.yaml" ; then
    echo 'Image already present'
    echo 'export NO_UPLOAD=1'  > /app/env
    exit 0
fi
if ! curl -L "${SUBST_URL}" -o /app/manifest.yaml; then
    echo "Cannot fetch manifest ${SUBST_URL}"
    exit 1
fi
echo 'Looking for images'
for source in $(grep -o 'image: .*' /app/manifest.yaml | tr -d "\"\'" | sed -e 's/^image: //' -e "s/['\"]//g" | sort | uniq); do
    case "${source}" in
        'kanod-registry.io/'*)
            echo "- kanod image"; continue;;
        *) ;;
    esac
    target="${EXTERNAL_REGISTRY}"/$(echo "$source" | sed -e 's![^/]*[:.][^/]*/!!');
    path=$(echo "$target" | sed -e 's!:!/manifests/!');
    if [ "$(curl -sL --insecure -o /dev/null "$path" -w '%{http_code}')" = 200 ]; then
        echo "- already synchronized";
        continue
    fi
    echo "$source => $target";
    echo "$source;$target" >> /app/todo.csv;
done
echo "$NAMESPACE"
if [ "$NAMESPACE" != '-' ]; then
    set -x
    mv /app/manifest.yaml /app/resource.yaml
    cat > /app/namespace.yaml <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: $NAMESPACE
EOF
    cat > /app/kustomization.yaml <<EOF
namespace: $NAMESPACE
resources:
- namespace.yaml
- resource.yaml
EOF
    echo '- Loading kustomize'
    curl -L -o /app/kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/"download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
    tar  -C /app -zxvf /app/kustomize.tgz kustomize
    /app/kustomize build /app/ > /app/manifest.yaml
    rm /app/kustomize.tgz /app/kustomize /app/resource.yaml /app/namespace.yaml /app/kustomization.yaml
fi

cat > /app/env <<EOF
export ARTIFACT=${ARTIFACT}
export VERSION=${ARTIFACT_VERSION}
export MANIFEST=/app/manifest.yaml
EOF
