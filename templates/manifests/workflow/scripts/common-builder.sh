#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

# shellcheck disable=SC2155
export REGISTRY_USER="$(grep '^username:' /vault/secrets/registry | sed 's/^[^:]*: *//')"
# shellcheck disable=SC2155
export REGISTRY_PASSWORD="$(grep '^password:' /vault/secrets/registry | sed 's/^[^:]*: *//')"

cat > /kaniko/Dockerfile << EOF
FROM python:3.9
RUN python -m pip install -U setuptools pip
RUN mkdir -p /app
RUN git clone "https://${KANOD_GROUP_URL}/image-builder.git" /app/image-builder
RUN cd /app/image-builder && git checkout ${IMAGE_BUILDER_VERSION} && python setup.py sdist && mv dist/*.tar.gz /app/image-builder.tar.gz
RUN git clone "https://${KANOD_GROUP_URL}/kanod-node.git" /app/kanod-node
RUN cd /app/kanod-node && git checkout ${KANOD_NODE_VERSION} && python setup.py sdist && mv dist/*.tar.gz /app/kanod-node.tar.gz
RUN git clone "https://${KANOD_GROUP_URL}/tpm-registrar.git" /app/tpm-registrar
RUN cd /app/tpm-registrar && git checkout ${TPM_REGISTRAR_VERSION} && python setup.py sdist && mv dist/*.tar.gz /app/registrar.tar.gz
RUN git clone "https://${KANOD_GROUP_URL}/rke-machine.git" /app/rke-machine
RUN cd /app/rke-machine && git checkout ${RKE_MACHINE_VERSION} && python setup.py sdist && mv dist/*.tar.gz /app/rke-machine.tar.gz
EOF

mkdir -p /kaniko/.docker
if [ -n "${REGISTRY_MIRROR:-}" ]; then
    mirror_config="--skip-tls-verify-registry ${REGISTRY_MIRROR} --registry-mirror ${REGISTRY_MIRROR}"
else
    mirror_config=''
fi
echo "{\"auths\":{\"${REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${REGISTRY_USER}" "${REGISTRY_PASSWORD}" | base64 | tr -d "\n")\"}}}" > /kaniko/.docker/config.json
# shellcheck disable=SC2086
/kaniko/executor --insecure ${mirror_config} \
    --build-arg IMAGE_BUILDER_VERSION --build-arg KANOD_NODE_VERSION \
    --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy \
    --context dir:///kaniko \
    --destination "${REGISTRY}/orange-opensource/kanod/kanod-diskimage-launcher/common:${TAG}"
