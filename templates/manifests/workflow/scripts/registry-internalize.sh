#!/bin/sh

#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

if [ ! -f /app/todo.csv ]; then
    echo 'no work unit'
    exit 0
fi

REGISTRY_USER=$(grep '^username:' /vault/secrets/registry | sed 's/^[^:]*: *//')
REGISTRY_PASSWORD=$(grep '^password:' /vault/secrets/registry | sed 's/^[^:]*: *//')

echo "${REGISTRY_PASSWORD}" | skopeo login --tls-verify=false --username "${REGISTRY_USER}" \
    --password-stdin "${EXTERNAL_REGISTRY}"
while IFS=';' read -r img target; do
    if [ -n "${REGISTRY_MIRROR:-}" ]; then
        # If we use a registry mirror, we first try to find the image there
        base="$(echo "${img}" | sed 's![^.:/]*[.:][^/]*/!!')"
        case "$base" in
        redis:* | ubuntu:*)
            base=library/$base
            ;;
        esac
        altimg="$REGISTRY_MIRROR/$base"
        if skopeo copy --src-tls-verify=false --dest-tls-verify=false  "docker://${altimg}" "docker://${target}"; then
            continue
        fi
    fi
    skopeo copy --dest-tls-verify=false  "docker://${img}" "docker://${target}"
done < /app/todo.csv
