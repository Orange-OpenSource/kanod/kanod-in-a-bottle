#!/bin/sh
#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

curl -L "https://dl.k8s.io/release/${K8S_RELEASE}/bin/linux/amd64/kubeadm" -o "/app/kubeadm"
chmod +x /app/kubeadm
touch /app/todo.csv
echo "kubeadm config images list --kubernetes-version ${K8S_RELEASE} :"
/app/kubeadm config images list --kubernetes-version "${K8S_RELEASE}"


for img in $(/app/kubeadm config images list --kubernetes-version "${K8S_RELEASE}"); do
    target=$(echo "$img" | sed -e 's![^/]*[:.][^/]*/!!')
    target="${EXTERNAL_REGISTRY}/${target}"
    path=$(echo "$target" | sed -e 's!:!/manifests/!')
    if curl --output /dev/null --silent --head --fail "${REGISTRY}/v2/$path"; then
    echo "  ${img} already uploaded"
    else
    echo "${img};${target}" >> /app/todo.csv
    case "$target" in
        */coredns/coredns*)
        target=$(echo "$target" | sed -e 's!coredns/coredns!coredns!')
        echo "${img};${target}" >> /app/todo.csv
        ;;
    esac
    fi
done
cat /app/todo.csv
