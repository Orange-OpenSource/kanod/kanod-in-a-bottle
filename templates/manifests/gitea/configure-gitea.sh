#!/bin/sh

#  Copyright (C) 2023 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

if [ -f /data/gitea/initialized ]; then
    echo "- Already configured"
    exit 0
fi

GITEA_ADMIN_USER=$(grep '^username:' /vault/secrets/admin | sed 's/^[^:]*: *//')
GITEA_ADMIN_PASSWORD=$(grep '^password:' /vault/secrets/admin | sed 's/^[^:]*: *//')

mkdir -p /data/gitea/conf/
echo "- Set configuration"
cat > /data/gitea/conf/app.ini <<EOF
APP_NAME = Gitea: Git with a cup of tea
RUN_MODE = prod
RUN_USER = git

[repository]
ROOT = /data/git/repositories

[repository.local]
LOCAL_COPY_PATH = /data/gitea/tmp/local-repo

[repository.upload]
TEMP_PATH = /data/gitea/uploads

[server]
APP_DATA_PATH    = /data/gitea
DOMAIN           = localhost
SSH_DOMAIN       = localhost
HTTP_PORT        = 3000
ROOT_URL         = ${GITEA_URL}
DISABLE_SSH      = false
SSH_PORT         = 22
SSH_LISTEN_PORT  = 22
LFS_START_SERVER = true
LFS_JWT_SECRET   = hAm4q3QLK4fLmttNULPAGb1j5_gRw2VTF3d7Onch9Z4
OFFLINE_MODE     = false

[database]
PATH     = /data/gitea/gitea.db
DB_TYPE  = sqlite3
qHOST     = localhost:3306
NAME     = gitea
USER     = root
PASSWD   =
LOG_SQL  = false
SCHEMA   =
SSL_MODE = disable
CHARSET  = utf8

[indexer]
ISSUE_INDEXER_PATH = /data/gitea/indexers/issues.bleve

[session]
PROVIDER_CONFIG = /data/gitea/sessions
PROVIDER        = file

[picture]
AVATAR_UPLOAD_PATH            = /data/gitea/avatars
REPOSITORY_AVATAR_UPLOAD_PATH = /data/gitea/repo-avatars
ENABLE_FEDERATED_AVATAR       = false

[attachment]
PATH = /data/gitea/attachments

[log]
MODE      = console
LEVEL     = info
ROUTER    = console
ROOT_PATH = /data/gitea/log

[security]
INSTALL_LOCK                  = true
SECRET_KEY                    = adqfdfqsdsfqsdf

[service]
DISABLE_REGISTRATION              = false
REQUIRE_SIGNIN_VIEW               = false
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[lfs]
PATH = /data/git/lfs

[mailer]
ENABLED = false

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[repository.pull-request]
DEFAULT_MERGE_STYLE = merge

[repository.signing]
DEFAULT_TRUST_MODEL = committer

[webhook]
ALLOWED_HOST_LIST = *.kanod.home.arpa,192.168.133.0/24
SKIP_TLS_VERIFY = true
EOF
echo "- Change rights"
chown -R git:git /data/gitea
echo "- Create user"
su git -c "cd /tmp && /app/gitea/gitea migrate"
su git -c "cd /tmp && /app/gitea/gitea admin user list"
su git -c "cd /tmp && /app/gitea/gitea admin user create --admin --username $GITEA_ADMIN_USER --password $GITEA_ADMIN_PASSWORD --email gitea@localhost --must-change-password=false"
echo "- Set initialized"
touch /data/gitea/initialized
