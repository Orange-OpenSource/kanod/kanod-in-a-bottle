#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
import requests
import time
import urllib
import yaml

TOKEN_PATH = '/var/run/secrets/kubernetes.io/serviceaccount/token'
GITEA_ROLE = 'gitea'

gitea_url = os.environ['GITEA_INTERNAL_URL']
vault = os.environ['VAULT_INTERNAL_URL']
noproxies = {"http": "", "https": ""}

def wait_for_vault():
    while True:
        print('.', flush=True)
        try:
            req = requests.get(f'{vault}/v1/sys/health', proxies=noproxies)
            if req.status_code == 200:
                break
        except Exception:
            print("failed")
            pass
        time.sleep(2)
    print()

def get_token():

    with open(TOKEN_PATH, 'r') as fd:
        jwt = fd.read()
    req = requests.post(
        f'{vault}/v1/auth/kubernetes/service/login',
        json={'jwt': jwt, 'role': GITEA_ROLE},
        proxies=noproxies,
    )
    if req.status_code == 200:
        vault_token = req.json().get('auth', {}).get('client_token', None)
        if vault_token is None:
            print('No token with vault')
            return None
        return vault_token
    else:
        print(f'Bad status from vault: {req.status_code}')
        return None

def get_secret(token, path):
    url = f'{vault}/v1/secret/{path}'
    req = requests.get(
        url, headers={'X-Vault-Token': token}, proxies=noproxies)
    if req.status_code == 200:
        res = req.json().get('data', {})
        return res
    else:
        print(f"failed with path {url}")
        return {}

def declare_users(token, gitea_config, auth):
    users = gitea_config.get('users', [])
    usernames = []
    for user in users:
        vault = user.get('vault', None)
        if vault is not None:
            config = get_secret(token, f'{vault}/git')
        else:
            config = user
        username = config.get('username', None)
        password = config.get('password', None)
        email = config.get('email', None)
        if username is None or password is None:
            print('Incomplete entry while processing gitea user')
            continue
        email = user.get('email', f'{username}@localhost.localdomain')
        usernames.append(username)
        request = {
            'username': username, 'password': password,
            "must_change_password": False, 'email': email}
        resp = requests.post(
            f'{gitea_url}/api/v1/admin/users',
            json=request, auth=auth)
        if resp.status_code not in [200, 201]:
            print(resp.content.decode('utf-8'))
    return usernames


def declare_team(usernames, org, orgname, role, perm, auth):
    members = org.get(f'{role}s', [])
    if len(members) > 0:
        request = {'name': f'{orgname}-{role}s', 'permission': perm}
        resp = requests.post(
            f'{gitea_url}/api/v1/admin/orgs/{orgname}/teams',
            json=request, auth=auth, proxies=noproxies)
        if resp.status_code not in [200, 201]:
            print(f'Cannot create team of {role}s for {orgname}:'
                  f'{resp.status_code}')
            print(resp.content.decode('utf-8'))
            return
        team_id = resp.json().get('id', None)
        if team_id is None:
            print('Cannot find team id')
            return
        for member in members:
            if member not in usernames:
                print(f'Unknown {role} member of {orgname}: {member}')
                return
            resp = requests.put(
                f'{gitea_url}/api/v1/admin/teams/{team_id}/members/{member}',
                auth=auth, proxies=noproxies
            )
            if resp.status_code != 204:
                print(f'Cannot add {member} as {role} of {orgname}')


def declare_organizations(gitea_config, usernames, auth):
    orgnames = []
    orgs = gitea_config.get('organizations', [])
    for org in orgs:
        orgname = org.get('name', None)
        orgnames.append(orgname)
        owner = org.get('owner', None)
        if orgname is None or owner is None:
            print('Incomplete entry while processing gitea organization')
            continue
        if owner not in usernames:
            print(f'unknown owner {owner} for organization {orgname}')
            continue
        description = org.get('description', f'organization {orgname}')
        request = {'username': orgname, 'description': description}
        resp = requests.post(
            f'{gitea_url}/api/v1/admin/users/{owner}/orgs',
            json=request, auth=auth, proxies=noproxies)
        if resp.status_code not in [200, 201]:
            print(f'Cannot create organization {orgname}')
            print(resp.content.decode('utf-8'))
            continue
        for (role, perm) in [
            ('admin', 'admin'),
            ('writer', 'write'),
            ('reader', 'read')
        ]:
            declare_team(
                usernames=usernames, org=org, orgname=orgname, role=role,
                perm=perm, auth=auth)
    return orgnames


def create_token(auth):
    (user, _) = auth
    request = {'name': 'config'}
    resp = requests.post(
        f'{gitea_url}/api/v1/users/{user}/tokens',
        json=request,
        auth=auth, proxies=noproxies)
    if resp.status_code == 201:
        return resp.json()['sha1']
    return None


def declare_project_webhook(owner, project_name, webhook, token):
    webhook_url = webhook.get('url', None)
    webhook_secret = webhook.get('secret', None)
    if webhook_url is None:
        return
    request = {
        'type': 'gitea',
        'config': {
            'url': webhook_url,
            'content_type': 'json'
        },
        'active': True
    }
    if webhook_secret is not None:
        request['config']['secret'] = webhook_secret
    resp = requests.post(
        f'{gitea_url}/api/v1/repos/{owner}/{project_name}/hooks',
        headers={'Authorization': f'token {token}'},
        json=request, proxies=noproxies)
    if resp.status_code not in [200, 201]:
        print(resp.content.decode('utf-8'))


def declare_project(gitea_config, usernames, orgnames, auth):
    projects = gitea_config.get('projects', [])
    token = create_token(auth)
    for project in projects:
        name = project.get('name', None)
        owner = project.get('owner', None)
        if name is None or owner is None:
            print('Incomplete entry while processing gitea project')
            continue
        if owner not in usernames and owner not in orgnames:
            print(f'Unknown owner {owner} for project {name}')
            continue
        description = project.get('description', f'project {name}')
        private = project.get('private', True)
        request = {
            'name': name, 'description': description, 'private': private
        }
        resp = requests.post(
            f'{gitea_url}/api/v1/admin/users/{owner}/repos',
            json=request,
            auth=auth, proxies=noproxies)
        if resp.status_code not in [200, 201]:
            print(resp.content.decode('utf-8'))
        for webhook in project.get('webhooks', []):
            declare_project_webhook(owner, name, webhook, token)


def configure_gitea(gitea_config):
    wait_for_vault()
    token = get_token()
    admin_config = get_secret(token, 'gitea/gitea')
    gitea_admin_user = admin_config.get('username', None)
    gitea_admin_password = admin_config.get('password', None)
    if gitea_admin_user is None or gitea_admin_password is None:
        print("Admin username/password is missing for gitea")
        raise Exception('Admin username/password must be defined for gitea')

    while True:
        try:
            if requests.get(gitea_url, proxies=noproxies).status_code == 200:
                break
        except Exception:
            pass
        print('.', flush=True)
        time.sleep(5)
    auth = (gitea_admin_user, gitea_admin_password)
    usernames = declare_users(token, gitea_config, auth)
    orgnames = declare_organizations(gitea_config, usernames, auth)
    declare_project(gitea_config, usernames, orgnames, auth)

def main():
    print('Initializing')
    with open('/scripts/gitea-config.yaml') as file:
        config = yaml.load(file, Loader=yaml.SafeLoader)
    configure_gitea(config)

main()