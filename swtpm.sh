#!/bin/bash
LIBTPM_REF=0.9.5
SWTPM_REF=0.8.0

tmp=$(mktemp -d)
mkdir -p "${tmp}"
cd "${tmp}" || (rmdir "${tmp}" && exit 1)

cleanup() {
    err=$?
    rm -rf "${tmp}"
    exit "${err}"
}

trap cleanup EXIT

if ! [ -d "libtpms-${LIBTPM_REF}" ]; then
    curl -L -o libtpms.tgz "https://github.com/stefanberger/libtpms/archive/refs/tags/v${LIBTPM_REF}.tar.gz"
    tar -zxvf libtpms.tgz
fi
if ! [ -d "swtpm-${SWTPM_REF}" ]; then
    curl -L -o swtpm.tgz "https://github.com/stefanberger/swtpm/archive/refs/tags/v${SWTPM_REF}.tar.gz"
    tar -zxvf swtpm.tgz
fi

echo '*** Building libtpms ***'
cd "./libtpms-${LIBTPM_REF}" || exit 1
sudo apt-get install -y automake autoconf libtool make gcc libc-dev libssl-dev
./autogen.sh --with-openssl --prefix=/usr --with-tpm2
make -j
sudo make install

echo '*** Building swtpm ***'
cd "../swtpm-${SWTPM_REF}" || exit 1
sudo mk-build-deps --install ./debian/control
sudo apt-get install -y debhelper dh-apparmor expect gawk gnutls-bin gnutls-dev \
    libfuse-dev libglib2.0-dev libjson-glib-dev libseccomp-dev libssl-dev \
    libtasn1-6-dev libtool net-tools socat softhsm2
./autogen.sh --with-openssl --prefix=/usr
make -j
sudo make install

echo '*** Configuration of swtpm ***'
getent group tss || sudo groupadd tss
getent passwd tss || sudo useradd -g tss tss

sudo mkdir /var/lib/swtpm-localca
sudo chown tss:root /var/lib/swtpm-localca
sudo swtpm_setup --tpmstate /var/lib/swtpm-localca --create-ek-cert \
    --create-platform-cert --allow-signing --tpm2
sudo chown -R tss.root /var/lib/swtpm-localca
sudo chmod -R 600 /var/lib/swtpm-localca
sudo chmod 750 /var/lib/swtpm-localca
sudo chmod 644 /var/lib/swtpm-localca/certserial \
    /var/lib/swtpm-localca/issuercert.pem \
    /var/lib/swtpm-localca/swtpm-localca-rootca-cert.pem

rm -rf "${tmp}"
