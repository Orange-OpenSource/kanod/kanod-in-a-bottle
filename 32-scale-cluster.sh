#!/bin/bash

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

CLUSTER_NAME="${CLUSTER_NAME:-cluster1}"

if [ "$KANOD_SWITCH" = 1 ]; then
    export http_proxy=socks5://localhost:22300
    export https_proxy=socks5://localhost:22300
    export HTTP_PROXY=$http_proxy
    export HTTPS_PROXY=$https_proxy
fi

kc2=${LAB_DIR}/kubeconfig/${CLUSTER_NAME}.kubeconfig


echo "Start at : $(date)"

nbNodes=0
nbCp=$(yq e '.controlPlane.replicas' "${LAB_DIR}/cluster1/config.yaml")

if [ "${AUTOSCALE:-0}" = 0 ]; then
  # Assumes a single md
  nbWorkers=$(yq e '.workers.[].replicas' "${LAB_DIR}/cluster1/config.yaml")
  # scaling workers : +1
  (( nbWorkers=nbWorkers+1 ))
  (( nbNodes=nbCp+nbWorkers ))
  pushd "${LAB_DIR}/cluster1" || { echo "error during cd ${LAB_DIR}/cluster1" ; exit; }
  if [ "${REDFISH_BROKER}" == 1 ]; then
    BMPOOL_SIZE=${nbNodes} yq -i e '.bareMetalPools.[].replicas = env(BMPOOL_SIZE)' config.yaml
  fi
  NB=${nbWorkers} yq -i e '.workers.[].replicas = env(NB)' config.yaml
  git commit -m "update" config.yaml
  git push origin main
  popd || exit
else
  # When in autoscale mode, configuration is not changed.
  # We modify the size by forcing a deployment that cannot fit on the single node
  # of md1 deployment
  kubectl --kubeconfig "${kc2}" apply -f - <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test
  namespace: default
spec:
  selector:
    matchLabels:
      app: test-app
  template:
    metadata:
      labels:
        app: test-app
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - test-app
            topologyKey: "kubernetes.io/hostname"
      nodeSelector:
        example.com/dep-label: md1
      containers:
      - name: main
        image: registry.k8s.io/hpa-example
        ports:
        - containerPort: 80
EOF
  kubectl --kubeconfig "${kc2}" rollout status deployment/test
  nbWorkers=3
  (( nbNodes=nbCp+nbWorkers ))
  kubectl --kubeconfig "${kc2}" scale --replicas=2 deployment/test
fi

echo "Waiting for $nbNodes nodes ready after scaling"
mkdir -p "${LAB_DIR}/logs"
count=0
while [ "$(kubectl --kubeconfig "${kc2}" --no-headers=true get nodes 2> /dev/null | wc -l)" != "${nbNodes}" ] || \
    [ "$(kubectl --kubeconfig "${kc2}" --no-headers=true get nodes 2> /dev/null | grep -v NotReady | grep -c Ready)" != "${nbNodes}" ]; do
    count=$((count+1))
    if [ "$count" -gt 300 ]; then
        ./view-deployment.sh -l -t -n
        echo; echo 'Timout waiting for nodes'
        exit 1
    fi
    printf "\n\n------- %s --------\n" "$(date)" &>> "${LAB_DIR}/logs/deployment_scaling.log"
    ./view-deployment.sh -l -t -n &>> "${LAB_DIR}/logs/deployment_scaling.log"
    sleep 10
    echo -n '.'
done
if [ "${AUTOSCALE:-0}" = 1 ]; then
    kubectl --kubeconfig "${kc2}" rollout status deployment/test
fi
echo
echo "End at : $(date)"
