import bcrypt
import flask
from threading import Timer
import importlib
import os
import sys
import traceback

from sushy_tools.emulator import api_utils
from sushy_tools import error

root_dir = os.environ['ROOT_DIR']
lab_dir = os.environ['LAB_DIR']

sushy_users = flask.Blueprint(
    'SushyUsers', __name__,
    url_prefix='/redfish/v1/AccountService',
    template_folder=os.path.join(root_dir, 'redfish/templates'))

accounts = [
    {'accountid':'1', 'username': 'root', 'enabled': True, 'roleid': 'Administrator'},
    {'accountid':'2', 'username': 'none', 'enabled': False, 'roleid': 'None'},
    {'accountid':'3', 'username': 'none', 'enabled': False, 'roleid': 'None'},
    {'accountid':'4', 'username': 'none', 'enabled': False, 'roleid': 'None'},
]

roles = [
    {
        'roleid':'Administrator',
        'privileges': ['Login', 'ConfigureManager', 'ConfigureUsers', 'ConfigureComponents'],
        'oemprivileges': ['ClearLogs', 'AccessVirtualConsole', 'AccessVirtualMedia', 'TestAlerts', 'ExecuteDebugCommands']
    },
    {
        'roleid':'Operator',
        'privileges': ['Login', 'ConfigureComponents'],
    },
    {
        'roleid':'ReadOnly',
        'privileges': ['Login'],
    },
    {
        'roleid':'None',
        'privileges': [],
    },
]


def crypt_password(passwd):
    salt = bcrypt.gensalt(rounds=5)
    crypted = bcrypt.hashpw(passwd.encode('utf-8'), salt)
    return crypted.decode('utf-8')

def set_password(username, password):
    try:
        auth_file = flask.current_app.config['SUSHY_EMULATOR_AUTH_FILE']
        with open(auth_file,'a') as fd:
            fd.write('%s:%s\n' % (username, crypt_password(password)))
    except Exception as e:
        flask.current_app.logger.error('cannot set redfish password for user %s', username)

def update_password(username, password=None):
    auth_file = flask.current_app.config['SUSHY_EMULATOR_AUTH_FILE']
    with open(auth_file,'r') as fd:
        lines = fd.readlines()
    with open(auth_file,'w') as fd:
        for line in lines:
            if line.startswith(username + ':'):
                if password is not None:
                    fd.write('%s:%s\n' % (username, crypt_password(password)))
            else:
                fd.write(line)



@sushy_users.route('', methods=['GET'])
@api_utils.returns_json
def get_account_service():
    return flask.render_template('account_service.json')

@sushy_users.route('Accounts', methods=['GET'])
@api_utils.returns_json
def get_accounts():
    return flask.render_template('accounts.json', accounts=accounts)

@sushy_users.route('Accounts', methods=['POST'])
@api_utils.returns_json
def post_accounts():
    username = flask.request.json.get('UserName', None)
    password = flask.request.json.get('Password', None)
    roleid = flask.request.json.get('RoleId', None)
    if username is None:
        raise error.BadRequest('UserName must be defined')
    if password is None:
        raise error.BadRequest('Password must be defined')
    if roleid is None:
        raise error.BadRequest('RoleId must be defined')
    for account in accounts:
        if account['username'] == username:
            raise error.BadRequest('Account UserName already in use')
    okrole = False
    for role in roles:
        if role['roleid'] == roleid:
            okrole = True
    if not okrole:
        raise error.BadRequest('RoleId does not exists')
    account = next(
        (account for account in accounts if account['roleid'] == 'None'),
        None)
    if account is None:
        raise error.BadRequest('No more account available')
    set_password(username, password)
    account['username'] = username
    account['enabled'] = True
    account['roleid'] = roleid
    return '', 204


@sushy_users.route('Accounts/<account_id>', methods=['GET'])
@api_utils.returns_json
def get_account(account_id):
    account = next(
        account for account in accounts
        if account['accountid'] == account_id)
    return flask.render_template('account.json', **account)

@sushy_users.route('Accounts/<account_id>', methods=['DELETE'])
@api_utils.returns_json
def delete_account(account_id):
    account = next((
        account for account in accounts
        if account['accountid'] == account_id), None)
    if account is None:
        return '', 204
    update_password(account_id)
    account['username']='none'
    account['enabled']=False
    account['roleid']='None'
    return '', 204

@sushy_users.route('Accounts/<account_id>', methods=['PATCH'])
@api_utils.returns_json
def patch_account(account_id):
    account = next(
        account for account in accounts
        if account['accountid'] == account_id)
    username = flask.request.json.get('UserName', None)
    password = flask.request.json.get('Password', None)
    roleid = flask.request.json.get('RoleId', None)
    if username is not None:
        account['username'] = username
    if roleid is not None:
        account['roleid'] = roleid
    if password is not None:
        update_password(account['username'], password=password)
    return flask.render_template('account.json', **account)

@sushy_users.route('Roles', methods=['GET'])
@api_utils.returns_json
def get_roles():
    return flask.render_template('roles.json', roles=roles)

@sushy_users.route('Roles/<role_id>', methods=['GET'])
@api_utils.returns_json
def get_role(role_id):
    role = next(
        role for role in roles
        if role['roleid'] == role_id)
    return flask.render_template('role.json', **role)


def late():
    try:
        main = importlib.import_module('sushy_tools.emulator.main')
        main.app.register_blueprint(sushy_users)
        # flask.current_app.register_blueprint(sushy_users)
    except Exception as e:
        flask.current_app.logger.error('cannot register blueprint for account management')
        with open('/tmp/err','w') as fd:
            traceback.print_exception(*sys.exc_info(), file=fd)
            fd.write(str(e))

t = Timer(0.0, late)
t.start()
