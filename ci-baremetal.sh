#!/bin/bash

# shellcheck disable=SC1083

set -eu
 
function configure_lcm() {
    echo "Update LCM configuration"
    
    # avoid interference with 192.168.200.0/24 network for baremetal on hydra
    yq ea '.kubernetes.standalone.pod_subnet = "192.168.212.0/22"' -i  "${LAB_DIR}/lcm/config.yaml"

    yq ea '.vm.networks += {"interface": "enp59s0f0"}' -i  "${LAB_DIR}/lcm/config.yaml"
    yq ea '.network.ethernets.ens4.dhcp4 = false' -i  "${LAB_DIR}/lcm/config.yaml"
    yq ea '.network.ethernets.ens4.addresses[0] = "192.168.200.135/24"' -i  "${LAB_DIR}/lcm/config.yaml"

    if [ "$SKETCH" == "regular" ]; then 
      yq ea 'select(document_index==0).spec.ironic.interface = "ens4"' -i "${LAB_DIR}/lcm/kanod.yaml"
      yq ea 'select(document_index==0).spec.ironic.ip = "192.168.200.135"' -i "${LAB_DIR}/lcm/kanod.yaml"
    fi

    if [ "$SKETCH" == "regular-ipam" ]; then 
      yq ea 'select(document_index==0).spec.ironic.interface = "ens5"' -i "${LAB_DIR}/lcm/kanod.yaml"
      VAL=${LCM_IP_EXT} yq ea 'select(document_index==0).spec.ironic.ip = env(VAL)' -i "${LAB_DIR}/lcm/kanod.yaml"

      yq ea '.vm.networks += {"interface": "external"}' -i  "${LAB_DIR}/lcm/config.yaml"
      yq ea '.network.ethernets.ens5.dhcp4 = false' -i  "${LAB_DIR}/lcm/config.yaml"
      VAL=${LCM_IP_EXT}"/27" yq ea '.network.ethernets.ens5.addresses[0] = env(VAL)' -i  "${LAB_DIR}/lcm/config.yaml"
      VAL=${GATEWAY_EXT} yq ea '.network.ethernets.ens5.gateway4 = env(VAL)' -i  "${LAB_DIR}/lcm/config.yaml"
    fi


    if [ "$KANOD_VAULT" == 1 ]; then
        yq e '.kubernetes.manifests[1].content |= "@vault:yaml:" + load_str("'"${LAB_DIR}"'/lcm/kanod.yaml")' -i "${LAB_DIR}/lcm/config.yaml"
    else
        yq e '.kubernetes.manifests[1].content |= load_str("'"${LAB_DIR}"'/lcm/kanod.yaml")' -i "${LAB_DIR}/lcm/config.yaml"
    fi
}


function configure_service() {
    echo "Update SERVICE configuration"

    # configuration for ntp server in order to answer to baremetal on hydra network (192.168.200.0/24)
    yq ea '.ntp.clients = ["192.168.0.0/16"]' -i  "${LAB_DIR}/service/config.yaml"

    # use lcm as gateway for 192.168.200.0/24 network
    yq ea '.network.ethernets.ens3.routes += [{"via": "192.168.133.10", "to": "192.168.200.0/24"}]' -i  "${LAB_DIR}/service/config.yaml"

    if [ "$SKETCH" == "regular-ipam" ]; then 
      # use lcm as gateway for external hydra network
      VAL=${BM1_IP_EXT}"/32" yq ea '.network.ethernets.ens3.routes += [{"via": "192.168.133.10", "to": env(VAL)}]' -i  "${LAB_DIR}/service/config.yaml"
      VAL=${BM2_IP_EXT}"/32" yq ea '.network.ethernets.ens3.routes += [{"via": "192.168.133.10", "to": env(VAL)}]' -i  "${LAB_DIR}/service/config.yaml"
    fi

    # build only one os/k8s version in the nexus
    # yq 'del(.[] |  select(key > 0))' -oj -i "${LAB_DIR}/service/manifests/workflow/k8s-release.json"
    # version=$(yq '.[0].release' -oj "${LAB_DIR}/service/manifests/workflow/k8s-release.json")
    # VER=$version yq 'del(.[] | select(.k8s_release!=env(VER)))' -oj -i "${LAB_DIR}/service/manifests/workflow/ubuntu.json"
}


function configure_infra() {
    echo "Update INFRA configuration"
    yq ' 
      .data.cluster_endpoint = "192.168.200.200" |
      .data.ippool_start = "192.168.200.128" |
      .data.ippool_end = "192.168.200.199" |
      .data.prefix = "24" |
      .data.gateway = "192.168.133.1"
    ' -i -i "${LAB_DIR}/meta/ci-data1.yaml"

    yq '(.controlPlaneFlavours[] | select (.flavour == "base")).kubernetes.network.pods = ["192.168.212.0/22"]'  -i "${LAB_DIR}/meta/infra.yaml"

    yq ea '.controlPlane.hostSelector.matchLabels."kanod.io/mode" = "bm"' -i  "${LAB_DIR}/cluster1/config.yaml"
    yq ea '.workers[].hostSelector.matchLabels."kanod.io/mode" = "bm"' -i  "${LAB_DIR}/cluster1/config.yaml"

    yq ea '.baremetal.annotations += "itf_1"' -i  "${LAB_DIR}/meta/infra.yaml"
    yq ea '.baremetal.annotations += "ip_mgt"' -i  "${LAB_DIR}/meta/infra.yaml"

    if [ "$SKETCH" == "regular" ]; then 
      yq '
        .spec.subnet = "192.168.200.0" |
        .spec.prefix = 24 |
        .spec.gateway = "192.168.200.135" |
        .spec.pxe = true |
        .spec.dnsServers = ["192.168.133.1"] |
        .spec.pools = [{"start": "192.168.200.211", "end": "192.168.200.220"}] |
        .spec.ipPool = "pxe-ippool"
      ' -i "${LAB_DIR}/meta/dhcp.yaml"

      yq e  'del(.controlPlaneFlavours[] | select(.flavour=="mono-dhcp").serverConfig.network.ethernets.ens3)' -i "${LAB_DIR}/meta/infra.yaml"
      yq e  'del(.workerFlavours[] | select(.flavour=="default-dhcp").serverConfig.network.ethernets.ens3)' -i "${LAB_DIR}/meta/infra.yaml"

      yq '
        with(.controlPlaneFlavours[] | select(.flavour=="mono-dhcp");
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".dhcp4 = false |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".addresses = ["{{ds.meta_data.endpoint_host}}/{{ds.meta_data.prefix}}"] |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".gateway4 = "192.168.200.135" |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".nameservers = {"addresses": ["192.168.133.1"]}) |
        with(.workerFlavours[] | select(.flavour=="default-dhcp");
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".dhcp4 = false |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".addresses = ["{{ds.meta_data.ip_mgt}}/24"] |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".gateway4 = "192.168.200.135" |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".nameservers = {"addresses": ["192.168.133.1"]})
      ' -i "${LAB_DIR}/meta/infra.yaml"
    fi 

    if [ "$SKETCH" == "regular-ipam" ]; then 
      create_bmh_data

      yq e  'del(.controlPlaneFlavours[] | select(.flavour=="mono-ipam").serverConfig.network.ethernets.ens3)' -i "${LAB_DIR}/meta/infra.yaml"
      yq e  'del(.workerFlavours[] | select(.flavour=="default-ipam").serverConfig.network.ethernets.ens3)' -i "${LAB_DIR}/meta/infra.yaml"

      yq '
        with(.controlPlaneFlavours[] | select(.flavour=="mono-ipam");
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".dhcp4 = false |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".addresses = ["{{ds.meta_data.endpoint_host}}/{{ds.meta_data.prefix}}"] |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".gateway4 = "192.168.200.135" |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".nameservers = {"addresses": ["192.168.133.1"]}) |
        with(.workerFlavours[] | select(.flavour=="default-ipam");
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".dhcp4 = false |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".addresses = ["{{ds.meta_data.ip_mgt}}/24"] |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".gateway4 = "192.168.200.135" |
                .serverConfig.network.ethernets."'{{ds.meta_data.itf_1}}'".nameservers = {"addresses": ["192.168.133.1"]})
      ' -i "${LAB_DIR}/meta/infra.yaml"
    fi 
}


function create_bmh_manifest() {
  echo "Create BMH manifests"

    cat > "${LAB_DIR}/bmh/bm-1.yaml" <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: bm-1-bmc-secret
  namespace: capm3
type: Opaque
data:
  username: ${BM1_BMC_USERNAME}
  password: ${BM1_BMC_PASSWORD}
---
apiVersion: metal3.io/v1alpha1
kind: BareMetalHost
metadata:
  name: bm-1
  namespace: capm3
  labels:
    kanod.io/mode: bm
    kanod.io/interface: ${BM1_INTERFACE}
  annotations:
    kanod.io/ipam: pxe:pxe-ippool
    kanod.io/itf_1: ${BM1_INTERFACE}
    kanod.io/ip_ext: "${BM1_IP_EXT}"
    kanod.io/ip_k8s: "${BM1_IP_K8S}"
    kanod.io/ip_mgt: "${BM1_IP_MGT}"
spec:
  bootMode: legacy
  online: false
  bmc:
    address: ${BM1_BMC_ADDR}
    credentialsName: bm-1-bmc-secret
    disableCertificateVerification: true
  bootMACAddress: ${BM1_BMC_MAC_ADDR}
EOF

    cat > "${LAB_DIR}/bmh/bm-2.yaml" <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: bm-2-bmc-secret
  namespace: capm3
type: Opaque
data:
  username: ${BM2_BMC_USERNAME}
  password: ${BM2_BMC_PASSWORD}
---
apiVersion: metal3.io/v1alpha1
kind: BareMetalHost
metadata:
  name: bm-2
  namespace: capm3
  labels:
    kanod.io/mode: bm
    kanod.io/interface: ${BM2_INTERFACE}
  annotations:
    kanod.io/ipam: pxe:pxe-ippool
    kanod.io/itf_1: ${BM2_INTERFACE}
    kanod.io/ip_ext: "${BM2_IP_EXT}"
    kanod.io/ip_k8s: "${BM2_IP_K8S}"
    kanod.io/ip_mgt: "${BM2_IP_MGT}"
spec:
  bootMode: legacy
  online: false
  bmc:
    address: ${BM2_BMC_ADDR}
    credentialsName: bm-2-bmc-secret
    disableCertificateVerification: true
  bootMACAddress: ${BM2_BMC_MAC_ADDR}
EOF
}


function create_bmh() {
    echo "Create BAREMETALHOST for baremetals"
    REPO=hosts
    GITLAB_BUILD_DIR=$(pwd)
    mkdir -p "${LAB_DIR}/bmh"
    create_bmh_manifest

    cat > "${LAB_DIR}/bmh/kustomization.yaml" <<EOF
---
namespace: ${TARGET_NAMESPACE}
resources:
- bm-1.yaml
- bm-2.yaml
EOF
    if [ "$SKETCH" == "regular" ]; then 
        for bmh_file in "${LAB_DIR}/bmh/bm-1.yaml" "${LAB_DIR}/bmh/bm-2.yaml"; do
            yq e 'select(.kind=="BareMetalHost").metadata.labels += {"kanod.io/dhcp": "pxe"}' -i "${bmh_file}"
            yq e 'select(.kind=="BareMetalHost").metadata.annotations += {"baremetalhost.metal3.io/paused": "kanod.io/kanod-kea"}' -i "${bmh_file}"
        done
    fi

    if [ "$SKETCH" == "regular-ipam" ]; then 
        for bmh_file in "${LAB_DIR}/bmh/bm-1.yaml" "${LAB_DIR}/bmh/bm-2.yaml"; do
            yq e 'select(.kind=="BareMetalHost").metadata.labels += {"network.kanod.io/bmh-data": "pxe-network"}' -i "${bmh_file}"
            yq e 'select(.kind=="BareMetalHost").metadata.annotations += {"baremetalhost.metal3.io/paused": "network.kanod.io/bmh-data"}' -i "${bmh_file}"
            sed -i 's!address: redfish://!address: idrac-virtualmedia://!g' "${bmh_file}"
        done
    fi

}

function push_bmh_manifests() {
    echo "Push BMH config to git"

    cd "${LAB_DIR}/bmh" || { echo "error during cd bmh" ; exit; }
    git init -b main
    git config --local user.name "${INFRA_USER}"
    git config --local user.email "${INFRA_USER}@localhost.localdomain"
    git config --local http.sslVerify false
    git remote add origin "https://${INFRA_USER}:${INFRA_PASSWORD}@${GOGS_IP}/${INFRA_USER}/${REPO}.git" || true

    git add .
    git commit -m 'Initial setup' || true
    git push -f origin main
    cd "${GITLAB_BUILD_DIR}"
}


function clean_all() {
    echo "Delete kanod vm(s) and libvirt volume"
    virsh destroy lcm
    virsh undefine lcm
    virsh destroy service
    virsh undefine service
    if test -d "${LAB_DIR}" ; then 
        rm -fr "${LAB_DIR}/lcm"
        rm -fr "${LAB_DIR}/service"
        rm -fr "${LAB_DIR}/${STORAGE_POOL:-okstore}"
    fi
}


function delete_cluster() {
    echo "Delete kanod cluster"
    (cd  "${LAB_DIR}/cluster1/" ; git rm config.yaml ; git commit -m delete config.yaml ; git push origin main)
    kubectl --kubeconfig  "${LAB_DIR}/kubeconfig/lcm.kubeconfig" -n "${TARGET_NAMESPACE}" delete cluster cluster1

    count=0
    while [ "$(kubectl --kubeconfig "${LAB_DIR}/kubeconfig/lcm.kubeconfig" -n "${TARGET_NAMESPACE}" get bmh 2> /dev/null | grep -c available)" != 2 ]; do 
        count=$((count+1))
        if [ "$count" -gt 360 ]; then
            echo 'Timout waiting for baremetalhost deletion'
            exit 1
        fi
        sleep 10
        echo -n '.'
    done
}


function create_bmh_data() {
  echo "Create BMH-DATA manifest"

  cat > "${LAB_DIR}/meta/bmh-data.yaml" <<EOF
---
apiVersion: ipam.metal3.io/v1alpha1
kind: IPPool
metadata:
  name: pxe-ippool
  namespace: capm3
spec:
  gateway: 192.168.200.135
  namePrefix: pxe
  pools:
  - end: 192.168.200.220
    start: 192.168.200.211
  prefix: 24
---
apiVersion: network.kanod.io/v1beta1
kind: BmhData
metadata:
  name: pxe-network
  namespace: ${TARGET_NAMESPACE}
spec:
  preprovisioningNetwork: pxe-network-data
  generateMetadata: false
  fromAnnotations:
  - key: preprovisioning_ip
    annotation: kanod.io/ip_ext
EOF


  cat >> "${LAB_DIR}/meta/bmh-data.yaml" <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: pxe-network-data
  namespace: ${TARGET_NAMESPACE}
stringData:
  template: |
    {
      "links": [
        {"id": "enp2s0f0", "type": "phy", "ethernet_mac_address": "{{: .boot_mac_address :}}"},
        { "id": "vlan_ext", "type": "vlan", "vlan_link": "enp2s0f0", "vlan_id": 20, "vlan_mac_address": "{{: .boot_mac_address :}}"}
      ],
      "networks": [{
        "id": "network0",
        "type": "ipv4",
        "link": "vlan_ext",
        "ip_address": "{{: .preprovisioning_ip :}}",
        "netmask": "255.255.255.0",
        "network_id": "network0",

        "routes": [{
          "network": "0.0.0.0",
          "netmask": "0.0.0.0",
          "gateway": "${LCM_IP_EXT}"
        },
        {
          "network": "192.168.133.0",
          "netmask": "255.255.255.0",
          "gateway": "${LCM_IP_EXT}"
        }]
      }],
      "services":[
        {"type": "dns", "address": "192.168.133.1"}
      ]
    }
EOF

}


#############
# main script

export no_proxy="$no_proxy,maven.service.kanod.home.arpa,git.service.kanod.home.arpa,192.168.133.10,192.168.133.12,192.168.200.0/24,192.168.200.200"
export NO_PROXY="$no_proxy"

GITLAB_BUILD_DIR=$(pwd)
export SKETCH=${SKETCH:-regular}

if [ "$SKETCH" != "regular" ] && [ "$SKETCH" != "regular-ipam" ]; then 
    echo "SKETCH $SKETCH not authorized"
    exit 1
fi

export TEMPLATE=${TEMPLATE:-param.ci}

cp "${TEMPLATE}" param.ci.bm
PROXY=$(yq '.proxy.no_proxy'  param.ci.bm)
PROXY="$PROXY,172.20.18.55,192.168.200.200,192.168.200.0/24" yq '.proxy.no_proxy = env(PROXY)' -i  param.ci.bm

TEMPLATE=param.ci.bm TEMPLATE_ONLY=1 ./ci-script.sh 

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
# shellcheck disable=SC1090,SC1091
source "${CI_BAREMETAL_CONFIG:-ci-baremetal-config}"
set +a

configure_lcm
configure_service
configure_infra
create_bmh

./01-basic-projects.sh
./02-mk-network.sh
./03-mk-store.sh
./06-launch-service.sh -s || true
./07-launch-lcm.sh -s
push_bmh_manifests
./11-meta.sh
./12-cluster.sh
./wait.sh
./view-deployment.sh -l -n

echo
delete_cluster

./view-deployment.sh -l

echo
clean_all
