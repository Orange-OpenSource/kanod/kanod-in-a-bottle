#!/bin/bash

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

export LIGHT_VIEW=${LIGHT_VIEW:-0}
export CLUSTER_NAME="${CLUSTER_NAME:-cluster1}"

if [ "$CLUSTER_NAME" == "cluster1" ]; then
    if [ "${SWITCH:-0}" = 1 ]; then
        CLUSTER_IP="192.168.144.200"
    else
        CLUSTER_IP="192.168.133.200"
    fi
elif [ "$CLUSTER_NAME" == "cluster2" ]; then
    if [ "${SWITCH:-0}" = 1 ]; then
        CLUSTER_IP="192.168.155.200"
    else
        CLUSTER_IP="192.168.133.210"
    fi
else
    echo "unknown cluster name ${CLUSTER_NAME}"
    exit
fi

function get_lcm_logs() {
    mkdir -p "${LAB_DIR}/lcm_logs"
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.10 sudo tar -czvf - -C /var/log/pods . | tar -C "${LAB_DIR}/lcm_logs" -xzvf - .
}

function get_target_cluster_logs() {
    mkdir -p "${LAB_DIR}/target_cluster_logs"
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@"${CLUSTER_IP}" sudo tar -czvf - -C /var/log/pods . | tar -C "${LAB_DIR}/target_cluster_logs" -xzvf - .
}

temp=$(mktemp -d)

kc1=${LAB_DIR}/kubeconfig/lcm.kubeconfig

echo "Start at : $(date)"

if [ "${REDFISH_BROKER:-}" == 0 ]; then
    echo "delete argocd application managing the baremetalhosts deployment on lcm cluster"
    kubectl --kubeconfig "${kc1}" -n argocd delete application baremetal-hosts
fi

pushd "${LAB_DIR}/meta" || { echo "error during cd ${LAB_DIR}/meta" ; exit; }
yq -i  e '.spec.pivotInfo.pivot = true' "${CLUSTER_NAME}.yaml"
git commit -m "update" "${CLUSTER_NAME}.yaml"
git push origin main
popd || exit

echo "Waiting for end of pivoting process for cluster ${CLUSTER_NAME}"
mkdir -p "${LAB_DIR}/logs"
count=0
while  [ "$(kubectl --kubeconfig "${kc1}" get -n capm3 clusterdef "${CLUSTER_NAME}" -o yaml | yq e '.status.phase')" != "Running" ] ||  \
    [ "$(kubectl --kubeconfig "${kc1}" get -n capm3 clusterdef "${CLUSTER_NAME}" -o yaml | yq e '.status.pivotstatus')" != "ClusterPivoted" ]; do
    count=$((count+1))
    if [ "$count" -gt 1500 ]; then
        ./view-deployment.sh -l -t -n  || true
        get_lcm_logs || true
        get_target_cluster_logs || true
        echo 'Timout waiting for end of pivoting process'
        exit 1
    fi
    sleep 2
    if [ "${VERBOSE:-0}" = 1 ]; then
        clear
        ./view-deployment.sh -l -t -c
    else
        echo -n '.'
    fi
done

if [ "${VERBOSE:-0}" = 1 ]; then
    clear
    ./view-deployment.sh -l -t -c
fi

echo
echo "End at : $(date)"
rm -r "${temp}"
