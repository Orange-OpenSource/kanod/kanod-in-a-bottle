#!/bin/bash

set -eu

function check() {
    a=$(yq .components."$1" templates/versions.yaml)
    b=$(grep "^$2=" "${tmp_git_stack}"/manifests/Makefile | sed 's/.*RELEASE=//g')
    if [ "$a" != "$b" ] ; then 
        echo "$1 : $a $b -> NOK !"
        ERROR=1
    else
        echo "component $1 : $a $b"
    fi
}

ERROR=0

tmp_git_stack=$(mktemp -d)
stack_version=$(yq .components.stack templates/versions.yaml)
git clone --quiet https://gitlab.com/Orange-OpenSource/kanod/kanod-stack.git "${tmp_git_stack}"

pushd "${tmp_git_stack}" || (rmdir "${tmp_git_stack}" && exit 1)
git checkout --quiet "${stack_version}"
popd || exit 1

echo "stack_version : ${stack_version}"
echo; echo; echo "Results:"
check baremetalpool BAREMETALPOOL_RELEASE
check bmh-data BMH_DATA_RELEASE
check brokerdef BROKERDEF_RELEASE
check brokernet BROKERNET_RELEASE
check byohostpool BYOHOSTPOOL_RELEASE
check byohostpool-scaler BYOHOSTPOOL_SCALER_RELEASE
check cluster-api-rke2 CAPI_RKE2_RELEASE
check cluster-api-provider-metal3-host METAL3_HOST_RELEASE
check cluster-def CLUSTERDEF_RELEASE
check cluster-network CLUSTER_NETWORK_RELEASE
check dashboard DASHBOARD_RELEASE
check host-baremetal HOST_BAREMETAL_RELEASE
check hostclaim HOSTCLAIM_RELEASE
check host-kubevirt HOST_KUBEVIRT_RELEASE
check host-openstack-operator HOST_OPENSTACK_RELEASE
check ipa-downloader IPA_DOWNLOADER_RELEASE
check ironic-credentials IRONIC_CREDENTIAL_RELEASE
check kanod-cert-patcher CERT_PATCHER_RELEASE
check kanod-kea KEA_RELEASE
check kanod-poolscaler POOLSCALER_RELEASE
check kanod-updater UPDATER_RELEASE
check network-operator NETWORK_OPERATOR_RELEASE
check openstack-resource-controller OPENSTACK_RESOURCE_CONTROLLER_RELEASE
check tpm-registrar REGISTRAR_RELEASE

if [ $ERROR == 1 ]; then
    echo; echo "versions incoherence(s) found"
    exit 1
else
    echo; echo "versions ok"
fi
rm -fr "${tmp_git_stack}"
