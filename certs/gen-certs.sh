#!/bin/bash

set -eu

if [ "${GENCA:-}" = 1 ]; then
  openssl genrsa -out "ca.key" 2048
  openssl req -x509 -new -nodes -key "ca.key" -sha256 -days 1825 -out "ca.cert" -subj /CN="Kanod CA"
fi

targets=(lcm broker)
ips=(192.168.133.11 192.168.133.14)

for i in "${!targets[@]}"; do
  target=${targets[$i]}
  ip=${ips[$i]}
  openssl genrsa -out "${target}.key" 2048
  openssl req -new -key "${target}.key" -out "${target}.csr" -subj "/CN=${target}/"
  openssl x509 -req -in "${target}.csr" -CA "ca.cert" -CAkey "ca.key" \
     -CAcreateserial -out "${target}.cert" -days 825 \
     -sha256 -extfile <(echo "subjectAltName=DNS:${target}.kanod.home.arpa, DNS:*.${target}.kanod.home.arpa, IP:${ip}")
  cat "${target}.cert" ca.cert > "${target}.chain"
  rm "${target}.csr"
done

