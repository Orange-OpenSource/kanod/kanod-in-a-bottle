vi ~/kanod_lab/cluster/config.yaml

#$ wait 1000
#$ sendcharacter /
#$ sendcharacter m
#$ sendcharacter d
#$ sendcharacter 1
#$ sendcontrol m
#$ wait 500

#$ sendcharacter /
#$ sendcharacter r
#$ sendcharacter e
#$ sendcharacter p
#$ sendcharacter l
#$ sendcharacter i
#$ sendcharacter c
#$ sendcharacter a
#$ sendcharacter s
#$ sendcontrol m
#$ wait 500
#$ sendcharacter $
#$ sendcharacter r
#$ sendcharacter 2
#$ wait 500

#$ sendcharacter Z
#$ sendcharacter Z

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster add config.yaml

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster commit -m 'scale cluster'

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster push origin master

#$ expect kanod:kanod-in-a-bottle> 

VERBOSE=1 ./wait.sh

#$ unbound-expect kanod:kanod-in-a-bottle> 

KUBECONFIG=~/kanod_lab/kubeconfig/cluster1.kubeconfig kubectl get node -o wide

#$ expect kanod:kanod-in-a-bottle> 
