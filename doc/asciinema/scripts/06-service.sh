./06-launch-service.sh -s

#$ unbound-expect kanod:kanod-in-a-bottle> 

export KUBECONFIG=~/kanod_lab/kubeconfig/lcm.kubeconfig

#$ expect kanod:kanod-in-a-bottle> 

kubectl get pod -A

#$ expect kanod:kanod-in-a-bottle> 
