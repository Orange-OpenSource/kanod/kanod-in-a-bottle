yq -i '.workers.[] |= select(.name == "md1").replicas = 2'  ~/kanod_lab/cluster/config.yaml

#$ expect kanod:kanod-in-a-bottle>

git -C ~/kanod_lab/cluster add config.yaml

#$ expect kanod:kanod-in-a-bottle>

git -C ~/kanod_lab/cluster commit -m 'scale cluster'

#$ expect kanod:kanod-in-a-bottle>

git -C ~/kanod_lab/cluster push origin master

#$ expect kanod:kanod-in-a-bottle>

while [ "$(kubectl --kubeconfig ~/kanod_lab/kubeconfig/cluster1.kubeconfig get nodes 2> /dev/null | grep -v NotReady | grep -c Ready)" != "3" ]; do CLUSTER_NAME=cluster1 ./view-deployment.sh -t ; sleep 10 ; done

#$ unbound-expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 CLUSTER_NAME=cluster1 ./view-deployment.sh -t

#$ expect kanod:kanod-in-a-bottle>

KUBECONFIG=~/kanod_lab/kubeconfig/cluster1.kubeconfig kubectl get node -o wide

#$ expect kanod:kanod-in-a-bottle>

