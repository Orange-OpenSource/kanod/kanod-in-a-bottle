LIGHT_VIEW=1 VERBOSE=1 CLUSTER_NAME=cluster1 ./30-pivot-cluster.sh

#$ unbound-expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 ./view-deployment.sh

#$ expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 VERBOSE=1 CLUSTER_NAME=cluster2 ./30-pivot-cluster.sh

#$ unbound-expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 ./view-deployment.sh

#$ expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 CLUSTER_NAME=cluster1 ./view-deployment.sh -t

#$ expect kanod:kanod-in-a-bottle>

LIGHT_VIEW=1 CLUSTER_NAME=cluster2 ./view-deployment.sh -t

#$ expect kanod:kanod-in-a-bottle>
