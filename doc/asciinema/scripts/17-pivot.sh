yq -i e '.spec.pivotInfo.pivot = true' ${LAB_DIR}/meta/cluster1.yaml
git -C ${LAB_DIR}/meta commit -m "Pivot cluster" cluster1.yaml
git -C ${LAB_DIR}/meta push origin master
while  [ "$(kubectl get -n capm3 clusterdef cluster1 -o 'jsonpath={.status.pivotstatus}')"  != "ClusterPivoted" ]; do LIGHT_VIEW=1 VERBOSE=1 ./view-deployment.sh -l -t; sleep 10; done
#$ unbound-expect kanod:kanod-in-a-bottle> 
kubectl get application -A
#$ expect kanod:kanod-in-a-bottle> 
