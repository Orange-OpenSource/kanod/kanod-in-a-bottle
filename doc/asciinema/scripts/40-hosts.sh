cp param.ci params.yml
#$ unbound-expect kanod:kanod-in-a-bottle>

yq -i '.bmc_protocol="redfish"' params.yml
#$ unbound-expect kanod:kanod-in-a-bottle>

yq -i '.mode_multi_tenant=true | .mode_multi_cluster=true' params.yml
#$ unbound-expect kanod:kanod-in-a-bottle>

yq -i '.hybrid.enabled=true' params.yml
#$ unbound-expect kanod:kanod-in-a-bottle>

./00-mk-templates.sh
#$ unbound-expect kanod:kanod-in-a-bottle>

./100-full.sh -f 7
#$ unbound-expect kanod:kanod-in-a-bottle>

