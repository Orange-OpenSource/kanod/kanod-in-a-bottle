vi ~/kanod_lab/cluster/config.yaml

#$ wait 1000
#$ sendcharacter :
#$ sendcharacter 1
#$ sendcharacter ,
#$ sendcharacter $
#$ sendcharacter s
#$ sendcharacter /
#$ sendcharacter v
#$ sendcharacter 1
#$ sendcharacter .
#$ sendcharacter 2
#$ sendcharacter 4
#$ sendcharacter .
#$ sendcharacter 6
#$ sendcharacter /
#$ sendcharacter v
#$ sendcharacter 1
#$ sendcharacter .
#$ sendcharacter 2
#$ sendcharacter 5
#$ sendcharacter .
#$ sendcharacter 2
#$ sendcontrol m
#$ wait 500

#$ sendcharacter Z
#$ sendcharacter Z

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster add config.yaml

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster commit -m 'upgrade cluster'

#$ expect kanod:kanod-in-a-bottle> 

git -C ~/kanod_lab/cluster push origin master

#$ expect kanod:kanod-in-a-bottle> 
#$ wait 10000

VERBOSE=1 ./wait.sh

#$ unbound-expect kanod:kanod-in-a-bottle> 

KUBECONFIG=~/kanod_lab/kubeconfig/cluster1.kubeconfig kubectl get node -o wide

#$ expect kanod:kanod-in-a-bottle> 
