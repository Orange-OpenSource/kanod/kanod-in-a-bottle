#!/bin/bash

echo "Recording for scenario ${1:-regular}"
if [ "${1:-regular}" == "regular" ] ; then
  for sketch in 00-templates 01-projects 02-network 03-store 04-minions 05-bmc 06-service 07-lcm 10-bmh 11-meta 12-cluster 13-wait 14-scale 15-wait-ha 16-upgrade 17-pivot; do
    if ! [ -f "doc/source/asciinema/${sketch}.cast" ]; then
      echo "**** ${sketch} ****"
      asciinema-automation -aa "--rows 6 --cols 160" "doc/asciinema/scripts/${sketch}.sh" "doc/source/asciinema/${sketch}.cast"
    fi
  done
fi

if [ "${1:-regular}" == "broker" ] ; then 
  for sketch in 00-templates 01-projects 02-network 03-store 04-minions 05-bmc 06-service 07-lcm 08-broker 09-netdef 10-bmh 11-meta 12-cluster 13-wait 30-pivot; do
    if ! [ -f "doc/source/asciinema/broker_${sketch}.cast" ]; then
      echo "**** ${sketch} ****"
      asciinema-automation -aa "--rows 6 --cols 200" "doc/asciinema/scripts/${sketch}.sh" "doc/source/asciinema/broker_${sketch}.cast"
    fi
  done
fi

if [ "${1:-regular}" == "multi-cluster" ] ; then 
  for sketch in 00-templates 01-projects 02-network 03-store 04-minions 05-bmc 06-service 07-lcm 08-broker 09-netdef 10-bmh 11-meta 12-cluster 13-wait-mt 30-pivot-mt 14-scale-mt; do
    if ! [ -f "doc/source/asciinema/broker_${sketch}.cast" ]; then
      echo "**** ${sketch} ****"
      asciinema-automation -aa "--rows 6 --cols 200" "doc/asciinema/scripts/${sketch}.sh" "doc/source/asciinema/multi-cluster_${sketch}.cast"
    fi
  done
fi

if [ "${1:-regular}" == "hosts" ] ; then
  # shellcheck disable=SC2043
  for sketch in 40-hosts; do
    if ! [ -f "doc/source/asciinema/broker_${sketch}.cast" ]; then
      echo "**** ${sketch} ****"
      asciinema-automation -aa "--rows 6 --cols 200" "doc/asciinema/scripts/${sketch}.sh" "doc/source/asciinema/hosts_${sketch}.cast"
    fi
  done
fi
