.. _kiab-prerequisites:

=============
Prerequisites
=============

Resource needs
==============
KIAB requires a lot of resources to run successfully. We recommend at least
the following:

* 32 GB RAM
* 100 GB free Disk space.
* Any reasonnably powerful Intel or AMD CPU with nested virtualization
  enabled should be fine.

KIAB has been mainly tested on Ubuntu hosts. We recommend using a Jammy release.

Virtualization
==============
KIAB uses virtual machines to simulate real servers. But it also uses virtual
machines for the different functions of the life-cycle-manager.

Virtual machines are launched through ``libvirt`` using the ``qemu`` hypervisor.
An installation procedure for Ubuntu is described here: 
https://help.ubuntu.com/community/KVM/Installation
Virtual machines are configured with the companion tool ``virt-install``.
We recommend that you put your user account in the ``libvirt`` and ``kvm`` groups.

To manipulate vitual machine images, the auxiliary packages: ``libguestfs-tools``
is needed. The package ``libvirt-dev`` providing
development libraries for ``libvirt`` is also required.

.. admonition:: Warning

    Virtual machine disks are installed in the target folder used
    for the experiment (the lab folder) not in the standard libvirt image
    folder.

    Libvirt must be able to use images stored in this folder. This means
    that the parent folder is readable by the system user running the libvirt
    daemon (libvirt-qemu for example on Ubuntu Jammy). Home folders are
    usually not accessible to the user.

Python environment
==================
As most of our utilities are python packages, you need Python (at least a 3.8
version) , with ``pip`` and ``setuptools``.
``${HOME}/.local/bin`` must be added to the ``PATH`` variable for locally
installed packages.

.. admonition:: Warning

  ``pip`` must be updated to the last version to be able to use wheel
  versions of python libraries. Do not rely on package
  provided by the OS distribution. The typical symptom is libaries required
  to install Python packages that must be manually installed ::

      sudo pip3 install --upgrade pip

.. admonition:: Warning

  If the default python version does
  not meet the requirements (eg. on Ubuntu bionic), you MUST set the ``̂${PYTHON}``
  variable to the full path of a correct version (e.g. /usr/bin/python3.8).
  If you use a non default python, you must also modify the commands that follow
  to use the right pip installer: use ``${PYTHON} -m pip`` instead of ``pip3``.

The following python software are used:

* ``diskimage-builder`` is an Openstack project that is used to build OS images.
  It must be installed it in your environment to build the fist images (Nexus
  server and optionally Vault server) ::

      sudo pip3 install diskimage-builder
      sudo apt install debootstrap kpartx -y

* Virtual BMC is another Openstack project that simulates a baseboard management
  controller that can be managed using the IPMI protocol ::

      pip3 install virtualbmc --user

* Virtual Redfish BMC (Openstack project) is similar to Virtual BMC except that
  it uses Redfish protocol to access the bmc ::

      sudo apt install python3-wheel -y
      sudo apt install apache2-utils -y
      pip3 install sushy-tools --user

* As we try to avoid putting new ip routes on your hosts, we need to use socks5
  proxies that are not supported in Python by default with requests. ::

      pip3 install requests[socks] --user

Other utilities
===============
The commands ``kubectl`` and ``yq`` (available here: https://github.com/mikefarah/yq/ , minimum version: v4.35.2)
must be available in the environment.

The ``nfs-kernel-server`` package should be installed when using scenarios with kubevirt.

Proxy settings
==============
If you use a proxy, you should add the following machines to your ``${no_proxy}``
variable::

    192.168.133.0/24,.kanod.home.arpa

The CIDR range is mainly used by kubectl and go utilities whereas
``curl`` or ``wget`` should use symbolic names.

Cloning kanod in a bottle
=========================
You must clone the ``kanod-in-a-bottle`` project:

.. code-block:: shell

    git clone https://gitlab.com/Orange-OpenSource/kanod/kanod-in-a-bottle.git
    cd kanod-in-a-bottle

