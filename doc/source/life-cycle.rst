.. _kiab-life-cycle:

============================
Managing Cluster life-cycle
============================

.. _kiab-life-scaling:

Scaling a cluster
=================

The end-user can scale the cluster by
modifying the number of replicas in
worker definitions and commiting the change.

It is also possible to scale the control-plane
as long as keepalived is used and a odd number
of replicas is used.

In the following recording, the scale of the machine
deployment is changed to two replicas. The change is
comitted and the state of the deployment is observed
until it stabilizes.

.. asciinema:: asciinema/14-scale.cast
    :rows: 36
    :cols: 160
    :idle-time-limit: .5

.. admonition:: Warning

    All the operations performed directly on the master branch in this example
    should be performed on a development branch and merged on the master branch
    only after validation by external tools and reviews on a production cluster.


.. _kiab-life-upgrading:

Updating a cluster
==================
It is also possible to change the OS version
of the control-plane or of worker deployments
by changing the ``os_version`` or the ``os``
fields

It is also possible to upgrade the Kubernetes
version. This is a two step process:

* First you must upgrade the control-plane.
  Add a version tag to the ``controlPlane``
  block and set its value to the new version.
  Wait until the change is propagated.
* Then update the global version tag to the
  new version. If you have several worder
  deployments you can also update them individually.

Keep in mind that the constraint is that the
control-plane version must be equal or greater than the worker versions
and that the minor version can only differ of at most one unit.

In the following cast, we first deploy an high availability cluster (state ``ha: true`` in 
file ``params.yml``).

.. asciinema:: asciinema/15-wait-ha.cast
    :rows: 36
    :cols: 160
    :idle-time-limit: .5

Then the version of the cluster is changed we substitute the old version with a new
one. Kanod will follow the two step upgrade by keeping a set of labels to follow
the evolution of the cluster. Kanod could even perform an upgrade from a version
*n* to a version *n+2* in a single step if the infrastructure administrator has
registered a set of usable versions to perform upgrades (the systtem need to
internally perform an upgrade to *n+1* first).

.. asciinema:: asciinema/16-upgrade.cast
    :rows: 36
    :cols: 160
    :idle-time-limit: .5

.. admonition:: Note

    The recording ends as soon as the number of planned node in the right version
    is available but before the last worker node in the old version is 
    stopped.
