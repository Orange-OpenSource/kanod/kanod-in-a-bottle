.. _kiab-host-hybrid:

======================================
Hybrid Clusters with the Host Resource
======================================

In this scenario, we show how to deploy a Kubernetes cluster with machine
deployments on bare-metal, kubevirt virtual machines inside the life-cycle
manager and virtual machines on an external OpenStack cloud.

We wil use the capability to have different controllers for Hosts targeting
different compute resources.

This scenario requires an access to an OpenStack cloud. We deploy a "small"
virtual machine with a DevStack instance on the ``oknet`` network. We recommend
to have at least 128Gb memory and more than 150Gb disk available.

The following parameters must be used:

.. code-block:: yaml

    hybrid:
      enabled: true
      openstack: true
      kubevirt: true

``./04-minions.sh`` will deploy not only the baremetal servers but also the Openstack
virtual machine containing a DevStack single node instance. The dashboard should
be available at ``http://192.168.133.18``

The Kanod operator will deploy the necessary controllers on the life-cycle manager.
The cluster-api-baremetal provider is a custom version modified to handle hosts.
The new pods provide:

* Three different controllers for hosts are deployed. 
* An instance of Kubevirt with an NFS provisionner used for large virtual disk
  backed by persistent volumes.
* The openstack-resource-controller, a new project that provides Kubernetes
  resource definitions for the most important OpenStack resources (virtual machines,
  images, networks, routers, etc.)

.. code-block:: output

    host-kubevirt-operator-system       host-kubevirt-operator-controller-manager-76f99cf889-hcvql       2/2     Running     0          3d4h
    host-openstack-operator-system      host-openstack-operator-controller-manager-5bb856bb44-9qmtm      2/2     Running     0          3d4h
    host-operator-system                host-operator-controller-manager-75b78dbcd8-2rs9z                2/2     Running     0          3d4h
    kanod-operator-system               kanod-operator-controller-manager-6b886cffdd-lbjwc               2/2     Running     0          3d4h
    kubevirt                            virt-api-6744f4b799-8nhpn                                        1/1     Running     0          3d4h
    kubevirt                            virt-controller-7d66dfcff8-4fnz7                                 1/1     Running     0          3d4h
    kubevirt                            virt-controller-7d66dfcff8-xfjmt                                 1/1     Running     0          3d4h
    kubevirt                            virt-handler-k682s                                               1/1     Running     0          3d4h
    kubevirt                            virt-operator-7f7677cc56-v5dns                                   1/1     Running     0          3d4h
    kubevirt                            virt-operator-7f7677cc56-w6rrs                                   1/1     Running     0          3d4h
    nfs-provisioner                     nfs-client-provisioner-689c54486c-5bbqq                          1/1     Running     0          3d4h
    orc-system                          orc-controller-manager-54bdbf7f7f-jt5fr                          2/2     Running     0          3d4h

The cluster definition (with abbreviated server configuration as they are all the
same) is the following:

.. code-block:: yaml

    version: v1.24.15
    controlPlane:
      flavour: mono-dhcp
      replicas: 1
      kubernetes:
        network:
          domain: cluster1.local
      serverConfig:
        os: jammy
        os_version: '1.13.26'
        admin:
          passwd: 'secret'
      hostSelector:
        matchLabels:
          host.kanod.io/kind: baremetal
    workers:
      - name: md1
        flavour: default-dhcp
        serverConfig: ...
        replicas: 1
        hostSelector:
          matchLabels:
            host.kanod.io/kind: baremetal
      - name: mdk
        flavour: kubevirt-default-dhcp
        serverConfig: ...
        replicas: 1
        hostSelector:
          matchLabels:
            host.kanod.io/kind: kubevirt
      - name: mdo
        flavour: default-openstack
        serverConfig: ...
        replicas: 1
        hostSelector:
          matchLabels:
            host.kanod.io/kind: openstack
            host.kanod.io/openstack-cloud: osp1
            host.kanod.io/openstack-networks: network-1
            host.kanod.io/openstack-flavor: kanod
    addons:
      - calico

The main obvious difference between the three machine deployments is the
host selector. We also use different base templates, mainly to accomodate
different network configurations.

There is an additional file in the configuration folder of the cluster describing
resources for OpenStack. We will detail those resources in the OpenStack section.

After deployment, four nodes are available. The first one is an OpenStack
virtual machine, the second one is managed by Kubevirt whereas the last one
is an emulated baremetal server.

.. code-block:: output

    NAME                             STATUS   ROLES           AGE    VERSION    INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
    md-cluster1-mdo-3608091d-mxfrm   Ready    <none>          3d8h   v1.24.15   192.168.144.58    <none>        Ubuntu 22.04.4 LTS   5.15.0-97-generic   containerd://1.6.28
    vm-6hrwh8wddn                    Ready    <none>          3d8h   v1.24.15   192.168.192.50    <none>        Ubuntu 22.04.4 LTS   5.15.0-97-generic   containerd://1.6.28
    vmok-2                           Ready    control-plane   3d8h   v1.24.15   192.168.133.200   <none>        Ubuntu 22.04.4 LTS   5.15.0-97-generic   containerd://1.6.28
    vmok-3                           Ready    <none>          3d8h   v1.24.15   192.168.133.36    <none>        Ubuntu 22.04.4 LTS   5.15.0-97-generic   containerd://1.6.28

Launching the cluster will create new hosts: 

.. code-block:: output

    kanod:kanod-in-a-bottle> kubectl get host -A
    NAMESPACE   NAME                                   READY   ONLINE
    capm3       controlplane-cluster1-67522ee3-8vvzd   true    true
    capm3       md-cluster1-md1-67522ee3-gkzdk         true    true
    capm3       md-cluster1-mdk-5a7f9a2c-q8vns         true    true
    capm3       md-cluster1-mdo-3608091d-mxfrm         true    true

In the following section, we will look to which resources those hosts are
bound.

Baremetal Deployment
--------------------
This is the same deployment as in the multi cluster experiment. The
``host.kanod.io/kind`` label is set to ``baremetal``. The control-plane
and the ``md1`` deployments consume one baremetal server each. 

.. code-block:: output

    kanod:kanod-in-a-bottle> kubectl get bmh -A
    NAMESPACE   NAME     STATE         CONSUMER                               ONLINE   ERROR   AGE
    infrahost   vmok-1   available                                            false            3d8h
    infrahost   vmok-2   provisioned   controlplane-cluster1-67522ee3-8vvzd   true             3d8h
    infrahost   vmok-3   provisioned   md-cluster1-md1-67522ee3-gkzdk         true             3d8h
    infrahost   vmok-4   available  

KubeVirt Deployment
-------------------
The host selector only specify the value of ``host.kanod.io/kind`` label: ``kubevirt``.
Here we use the default values for the machine configuration but several parameters
can be set (number of vcpu, memory size, disk size) using labels.

.. code-block:: yaml

    apiVersion: kubevirt.io/v1
    kind: VirtualMachine
    metadata:
      name: vm-6hrwh8wddn
      namespace: capm3
    spec:
      dataVolumeTemplates:
      - metadata:
          creationTimestamp: null
          name: osdisk-template-vm-6hrwh8wddn
        spec:
          pvc:
            accessModes:
            - ReadWriteMany
            resources:
              requests:
                storage: 8Gi
            storageClassName: nfs-client
          source:
            http:
              certConfigMap: repo-ca
              url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/jammy-v1.24.15/1.13.26/jammy-v1.24.15-1.13.26.qcow2
      running: true
      template:
        metadata:
          creationTimestamp: null
        spec:
          architecture: amd64
          domain:
            cpu:
              cores: 2
            devices:
              disks:
              - disk:
                  bus: virtio
                name: osdisk
              - disk:
                  bus: virtio
                name: cloud-init
            machine:
              type: q35
            resources:
              requests:
                memory: 2Gi
          volumes:
          - dataVolume:
              name: osdisk-template-vm-6hrwh8wddn
            name: osdisk
          - cloudInitNoCloud:
              secretRef:
                name: vm-6hrwh8wddn-userdata
            name: cloud-init

We distinguish the main disk backed by a NFS permanent volume containing the OS.
It is provisioned using the Data Volume abstraction which merge a persistent
volume claim and a source description for the content of the volume.

.. code-block:: yaml

    apiVersion: cdi.kubevirt.io/v1beta1
    kind: DataVolume
    metadata:
      name: osdisk-template-vm-6hrwh8wddn
      namespace: capm3
    spec:
      pvc:
        accessModes:
        - ReadWriteMany
        resources:
          requests:
            storage: 8Gi
        storageClassName: nfs-client
      source:
        http:
          certConfigMap: repo-ca
          url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/jammy-v1.24.15/1.13.26/jammy-v1.24.15-1.13.26.qcow2

The second disk is a cloud-init config drive. Because Kubevirt controls the values of meta-data and does
not give access to user suplied meta-data, the user-data must be substituted at definition time.
The value of the userData field of the user data secret is the following (abbreviated):

.. code-block:: yaml

    ## template: jinja
    #cloud-config

    write_files:
    -   path: /etc/kanod-configure/configuration.yaml
        content: |
          admin: ...
          certificates: ...
          kubernetes:
            container_engine: containerd
            role: worker
          name: 'vm-6hrwh8wddn'
          network:
            ethernets:
              enp1s0:
                dhcp4: true
                nameservers:
                  addresses:
                  - 192.168.133.1
            version: 2
          network_checks: ...
          nexus: ...
          ntp: ...
          os: jammy
          os_version: 1.13.26
          proxy: ...
          vault: {}

    -   path: /run/kubeadm/kubeadm-join-config.yaml
        owner: root:root
        permissions: '0640'
        content: |
          ---
          apiVersion: kubeadm.k8s.io/v1beta3
          discovery:
            bootstrapToken:
              apiServerEndpoint: 192.168.133.200:6443
              caCertHashes:
              - sha256:e074c54f891091975be8149fb4ca144fdecf74a33c32d1c80939a73fa046d7e2
              token: 9tbeip.7ax7l79zhq1ocagq
          kind: JoinConfiguration
          nodeRegistration:
            imagePullPolicy: IfNotPresent
            kubeletExtraArgs:
              node-labels: ',metal3.io/uuid=61359c5f-dad0-433c-b31a-485471081081,cluster.kanod.io/part-of=md-cluster1-mdk'
              provider-id: metal3://61359c5f-dad0-433c-b31a-485471081081
            name: 'vm-6hrwh8wddn'
            taints:
            - effect: NoSchedule
              key: node.cluster.x-k8s.io/uninitialized
    -   path: /run/cluster-api/placeholder
        owner: root:root
        permissions: '0640'
        content: "This placeholder file is used to create the /run/cluster-api sub directory in a way that is compatible with both Linux and Windows (mkdir -p /run/cluster-api does not work with Windows)"
    runcmd:
      - "HOME=/root"
      - "kanod-runcmd"
      - kubeadm join --config /run/kubeadm/kubeadm-join-config.yaml  && echo success > /run/cluster-api/bootstrap-success.complete
      - "kanod-postcmd"

The naming of the NIC is slightly different from the other VMs. The provider-id and the token are directly substituted..

The virtual machine definition generates a virtual machine instance with an IP:

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get vmi -A
    NAMESPACE   NAME            AGE    PHASE     IP               NODENAME   READY
    capm3       vm-6hrwh8wddn   3d8h   Running   192.168.192.50   lcm        True

It is possible to connect to the virtual machine directly. For example

.. code-block:: bash

    virtctl console -n capm3 vm-6hrwh8wddn

OpenStack Deployment
--------------------

The selector is slightly more complex: we need to define 

* how to access the OpenStack cloud
* which flavour to use (the dimension parameters of the virtual machine)
* to which networks to connect the machine.

which gives the following specification:

.. code-block:: yaml

        hostSelector:
          matchLabels:
            host.kanod.io/kind: openstack
            host.kanod.io/openstack-cloud: osp1
            host.kanod.io/openstack-networks: network-1
            host.kanod.io/openstack-flavor: kanod

The host will trigger the generation of an OpenStack virtual machine described
by the following resource:

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get openstackserver -A
    NAMESPACE   NAME                             READY   ERROR   STATUS   OPENSTACKID
    capm3       md-cluster1-mdo-3608091d-mxfrm   True    False   Ready    a25f8d8b-1e98-4fc7-a8de-b14e40c259ba

It is possible to view the machine using OpenStack CLI.

.. code-block:: bash

    openstack --os-cloud openstack server list
    +--------------------------------------+--------------------------------+--------+--------------------------+----------------+--------+
    | ID                                   | Name                           | Status | Networks                 | Image          | Flavor |
    +--------------------------------------+--------------------------------+--------+--------------------------+----------------+--------+
    | a25f8d8b-1e98-4fc7-a8de-b14e40c259ba | md-cluster1-mdo-3608091d-mxfrm | ACTIVE | network-1=192.168.144.58 | img-fa52ae210b | kanod  |
    +--------------------------------------+--------------------------------+--------+--------------------------+----------------+--------+

The definition of the resource generated by the Host controller is the following:

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackServer
    metadata:
      name: md-cluster1-mdo-3608091d-mxfrm
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        configDrive: true
        flavor: kanod
        image: img-fa52ae210b
        metadata:
          cluster_endpoint: 192.168.133.200
          endpoint_host: 192.168.133.200
          endpoint_port: "6443"
          gateway: 192.168.133.1
          providerid: capm3/md-cluster1-mdo-3608091d-mxfrm/md-cluster1-mdo-3608091d-mxfrm
          ...
        name: md-cluster1-mdo-3608091d-mxfrm
        networks:
        - network: network-1
        userData: "... provider-id: metal3://{{ ds.meta_data.meta.providerid }}\n ..."

Even if k-orc let the user provide meta-data, it introduces a ``meta`` layer that must be
taken into account in the definition of user-data.

The glance image has been generated on the fly by the Host controller from the image URL:

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackImage
    metadata:
      name: img-fa52ae210b
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        containerFormat: bare
        diskFormat: qcow2
        method: web-download
        name: img-fa52ae210b
        webDownload:
          url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/jammy-v1.24.15/1.13.26/jammy-v1.24.15-1.13.26.qcow2

But the Host controller is limited to the generation of virtual machines. Networking
and a few additional parameters are not handled.

The additional resources necessary for this deployment specify the access to the cloud, the
OpenStack flavour and the network resources used by the virtual machine.
All the resources are `standard k-orc resources <https://k-orc.cloud>`_ and most of them are backed by
exactly one OpenStack resource. Additional resources could be defined (typically security group
configurations).

The first resource describes the authentication credentials to connect to the OpenStack
cloud. It is an encapsulated ``clouds.yaml``. 

.. code-block:: yaml

    apiVersion: v1
    kind: Secret
    metadata:
      name: openstack-clouds
      namespace: capm3
    stringData:
      clouds.yaml: |
        clouds:
          openstack:
            auth:
              auth_url: http://192.168.133.18/identity
              username: "admin"
              project_name: "admin"
              user_domain_name: "Default"
              password: secret
              domain_name: default
            region_name: "RegionOne"
            interface: "public"
            identity_api_version: 3
    type: Opaque
    ---
    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackCloud
    metadata:
      name: osp1
      namespace: capm3
    spec:
      cloud: openstack
      credentials:
        secretRef:
          key: clouds.yaml
          name: openstack-clouds
        source: secret

The flavour describes the size of the virtual machine. It is publicly named. 

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackFlavor
    metadata:
      name: kanod
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        disk: 6
        ephemeral: 0
        id: k1
        isPublic: true
        name: kanod
        ram: 4096
        swap: 0
        vcpus: 2

We define a network that will be used by our deployment.

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackNetwork
    metadata:
      name: network-1
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        name: network-1

and a subnet that describes IP allocations:

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackSubnet
    metadata:
      name: subnet-1
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        allocationPools:
        - end: 192.168.144.100
          start: 192.168.144.5
        cidr: 192.168.144.0/24
        ipVersion: IPv4
        name: subnet-1
        network: network-1

We also describe the public provider network so that we get a handle on it
as we need to connect our network.

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackNetwork
    metadata:
      name: public
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        external: true
        name: public
        providerPhysicalNetwork: public

Finally we define a router connecting the private network to the public one.

.. code-block:: yaml

    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackRouter
    metadata:
      name: r1
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        externalGatewayInfo:
          network: public
        name: r1
        ports:
        - p1
    ---
    apiVersion: openstack.k-orc.cloud/v1alpha1
    kind: OpenStackPort
    metadata:
      name: p1
      namespace: capm3
    spec:
      cloud: osp1
      resource:
        deviceOwner: network:router_interface
        fixedIPs:
        - ipAddress: 192.168.144.1
          subnet: subnet-1
        name: p1
        network: network-1
