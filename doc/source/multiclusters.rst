.. _kiab-multiclusters:

=====================================
Managing multiple clusters with Kanod
=====================================

Goals
=====

The redfish Broker (see :ref:`Multi Tenancy <kiab-broker>`) is a solution to manage
baremetalhosts resources for multiple clusters with one LCM.
For each cluster, the resources (ClusterAPIresources, baremetalhosts, baremetalpool)
describing the target cluster are in an isolated namespace.

For each target cluster, the pivoting process can be viewed as a copy of the all
the resources in this namespace from the LCM cluster to the target cluster.

The following scenario shows how to deploy, pivot and manage 2 clusters from one LCM.
(``[...]`` hides non-relevant information for the scenario understanding).

.. warning::

    We do not show the behaviour of brokernet yet.
    They will be given in a new scenario involving the Top-of-Rack switch.


Clusters description resources
==============================

For this scenario, 2 target clusters will be defined :

* cluster1:

  * cluster resources will be deployed in the namespace ``capm3-cluster1``
  * the cluster description is stored in the Git repository https://git.service.kanod.home.arpa/cluster_admin/config.git
    owned by the Git user ``cluster_admin`` :


    .. code-block:: yaml

        ---
        version: v1.24.6
        controlPlane:
          flavour: mono
          replicas: 1
          kubernetes:
            network:
              domain: cluster1.local
          serverConfig:
            [...]
          baremetalSelector:
            matchLabels:
              kanod.io/mode: vm
        workers:
          - name: md1
            flavour: default
            serverConfig:
              [...]
            replicas: 1
            baremetalSelector:
              matchLabels:
                kanod.io/mode: vm
        addons:
          - calico
        baremetalpools:
          - name: baremetalpool-1
            poolname: "pooldef1"
            brokerid: "default-broker"
            labelSelector:
              bmtype: vm-libvirt
            redfishschema: redfish
        [...]


* cluster2:

  * cluster resources will be deployed in the namespace ``capm3-cluster2``
  * the cluster description is stored in the Git repository https://git.service.kanod.home.arpa/cluster2_admin/config.git
    owned by the Git user ``cluster2_admin``. 

    Here is the ``baremetalpools`` section for the cluster2 description :

    .. code-block:: yaml

        [...]
        baremetalpools:
          - name: baremetalpool-2
            poolname: "pooldef2"
            brokerid: "default-broker-2"
            labelSelector:
              bmtype: vm-libvirt
            redfishschema: redfish


For each cluster, a clusterdef resources is defined on the infrastructure side
and is responsible of the deployment of argocd application for the cluster.


* clusterdef for cluster1:

  .. code-block:: yaml

      apiVersion: gitops.kanod.io/v1alpha1
      kind: ClusterDef
      metadata:
        name: "cluster1"
        namespace: capm3
      spec:
        source:
          repository: "https://git.service.kanod.home.arpa/cluster_admin/config.git"
          branch: master
        credentialName: "project-creds"
        configurationName: infra
        cidataName: "cluster1-cidata"
        brokerUserList:
          default-broker:
            username: "pool-user-1"
            address: https://broker.kanod.home.arpa/redfish-proxy
            brokerConfig: broker-ca
            brokerCredentials: broker-cred
        pivotInfo:
          pivot: false
          baremetalpoolList:
            - "baremetalpool-1"
          [...]
        multiTenant: true
        [...]

* clusterdef for cluster2:

  .. code-block:: yaml

      apiVersion: gitops.kanod.io/v1alpha1
      kind: ClusterDef
      metadata:
        name: "cluster2"
        namespace: capm3
      spec:
        source:
          repository: "https://git.service.kanod.home.arpa/cluster2_admin/config.git"
          branch: master
        credentialName: "project-creds-2"
        configurationName: infra
        cidataName: "cluster2-cidata"
        brokerUserList:
          default-broker-2:
            username: "pool-user-2"
            [...]
        pivotInfo:
          pivot: false
          baremetalpoolList:
            - "baremetalpool-2"
          [...]
        multiTenant: true
        [...]


In these clusterdef resources, we can observe that each cluster has its own resources for:

- the git repository (and the credentials) of the cluster description
- the barematalpool used for the cluster (this infirmation is used during the pivot process)
- the configuration of the cluster endpoint IP (via a configmap specified with the ``cidataName`` field)


Deployment of the target clusters
=================================

The script ``100-full.sh`` launches the deployment process of the 2 target clusters.

For this purpose, the initial configuration must be updated
with these variables declaration before launching the script ``100-full.sh``:

.. code-block:: yaml

    redfish_broker:
      broker_enabled: true
    bmc_protocol: redfish
    mode_multi_tenant: true
    mode_multi_cluster: true

These variables indicates that :

* the redfish broker is used
* each target cluster has its own namespace (``mode_multi_tenant``)
* 2 target clusters will be deployed (``mode_multi_cluster``)

We can observe the deployements of the clusters resources in the LCM
cluster :


.. asciinema:: asciinema/multi-cluster_13-wait-mt.cast
    :rows: 45
    :cols: 160
    :idle-time-limit: 1



Resources in the Life-Cycle-Manager
===================================

When we look at the resources deployed on the life-cycle manager,
we can see :

* cluster-def resources :

  For each cluster, a clusterdef resource is deployed with the address of the
  git repository storing the cluster description.

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get clusterdef -A
    NAMESPACE   NAME       PHASE                 REPOSITORY                                                      BRANCH   PIVOTSTATUS
    capm3       cluster1   ClusterProvisionned   https://git.service.kanod.home.arpa/cluster_admin/config.git    master   NotPivoted
    capm3       cluster2   ClusterProvisionned   https://git.service.kanod.home.arpa/cluster2_admin/config.git   master   NotPivoted



* argocd applications :

  For each cluster, a argocd application is launched and is responsible
  of the deployment of all the resources describing the cluster.

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get application -A
    NAMESPACE   NAME             SYNC STATUS   HEALTH STATUS
    argocd      cdef-cluster1    Synced        Progressing
    argocd      cdef-cluster2    Synced        Progressing
    argocd      kanod-projects   Synced        Healthy

* baremetalpools :

  2 baremetalpools are created, each one in the namespace of the associated cluster.

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get baremetalpool -A
    NAMESPACE        NAME              POOLNAME   STATUS
    capm3-cluster1   baremetalpool-1   pooldef1   Pool allocated
    capm3-cluster2   baremetalpool-2   pooldef2   Pool allocated

* baremetalhosts :

  4 baremetalhosts are created:

  * 2 baremetalhosts in the namespace ``capm3-cluster1``, created by the baremetalpool ``baremetalpool-1``
  * 2 baremetalhosts in the namespace ``capm3-cluster2``, created by the baremetalpool ``baremetalpool-2``

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get baremetalhost -A
    NAMESPACE        NAME                  STATUS   STATE         CONSUMER                               BMC                                                                            HARDWARE_PROFILE   ONLINE   ERROR   AGE
    capm3-cluster1   pooldef1-2mx66hn78n   OK       provisioned   controlplane-cluster1-3a996ad5-vn8qg   redfish:pooldef1-2mx66hn78n.broker.kanod.home.arpa/redfish/v1/Systems/vmok-1   unknown            true             9m53s
    capm3-cluster1   pooldef1-xmpdrfpnbp   OK       provisioned   md-cluster1-md1-3a996ad5-2ql4m         redfish:pooldef1-xmpdrfpnbp.broker.kanod.home.arpa/redfish/v1/Systems/vmok-6   unknown            true             9m48s
    capm3-cluster2   pooldef2-8ghwjnfcs9   OK       provisioned   controlplane-cluster2-3a996ad5-j5r8p   redfish:pooldef2-8ghwjnfcs9.broker.kanod.home.arpa/redfish/v1/Systems/vmok-4   unknown            true             9m57s
    capm3-cluster2   pooldef2-gxxzs2h9fv   OK       provisioned   md-cluster2-md1-3a996ad5-22vgr         redfish:pooldef2-gxxzs2h9fv.broker.kanod.home.arpa/redfish/v1/Systems/vmok-5   unknown            true             9m52s

* cluster resources :

  If we look at the ClusterAPI/Metal3 resources, we can see the resources associated with each cluster
  in their corresponging namespaces.


.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get Cluster -A
    NAMESPACE        NAME       PHASE         AGE     VERSION
    capm3-cluster1   cluster1   Provisioned   9m57s
    capm3-cluster2   cluster2   Provisioned   10m

    kanod:kanod-in-a-bottle> kubectl get KubeadmControlPlane -A
    NAMESPACE        NAME                    CLUSTER    INITIALIZED   API SERVER AVAILABLE   REPLICAS   READY   UPDATED   UNAVAILABLE   AGE     VERSION
    capm3-cluster1   controlplane-cluster1   cluster1   true          true                   1          1       1         0             9m57s   v1.24.6
    capm3-cluster2   controlplane-cluster2   cluster2   true          true                   1          1       1         0             10m     v1.24.6


    kanod:kanod-in-a-bottle> kubectl get MachineDeployment -A
    NAMESPACE        NAME                    CLUSTER    INITIALIZED   API SERVER AVAILABLE   REPLICAS   READY   UPDATED   UNAVAILABLE   AGE     VERSION
    capm3-cluster1   controlplane-cluster1   cluster1   true          true                   1          1       1         0             9m57s   v1.24.6
    capm3-cluster2   controlplane-cluster2   cluster2   true          true                   1          1       1         0             10m     v1.24.6


    kanod:kanod-in-a-bottle> kubectl get Metal3Machine -A
    NAMESPACE        NAME                                   AGE     PROVIDERID                                      READY   CLUSTER    PHASE
    capm3-cluster1   controlplane-cluster1-3a996ad5-vn8qg   9m55s   metal3://f599350b-fc6e-4396-87b6-b24ccf416c9f   true    cluster1   
    capm3-cluster1   md-cluster1-md1-3a996ad5-2ql4m         9m57s   metal3://abb142a1-1fb4-452a-9c1a-4c117ce02efa   true    cluster1   
    capm3-cluster2   controlplane-cluster2-3a996ad5-j5r8p   9m59s   metal3://761fa9e9-a074-4f48-a751-d2652467e1fd   true    cluster2   
    capm3-cluster2   md-cluster2-md1-3a996ad5-22vgr         10m     metal3://5bd563bc-644b-4fb1-b2cb-e8163388ad44   true    cluster2   


    kanod:kanod-in-a-bottle> kubectl get Machine -A
    NAMESPACE        NAME                               CLUSTER    NODENAME              PROVIDERID                                      PHASE     AGE     VERSION
    capm3-cluster1   controlplane-cluster1-qjw6f        cluster1   pooldef1-2mx66hn78n   metal3://f599350b-fc6e-4396-87b6-b24ccf416c9f   Running   9m56s   v1.24.6
    capm3-cluster1   md-cluster1-md1-866dc9985c-k8vmj   cluster1   pooldef1-xmpdrfpnbp   metal3://abb142a1-1fb4-452a-9c1a-4c117ce02efa   Running   9m58s   v1.24.6
    capm3-cluster2   controlplane-cluster2-z6s7h        cluster2   pooldef2-8ghwjnfcs9   metal3://761fa9e9-a074-4f48-a751-d2652467e1fd   Running   10m     v1.24.6
    capm3-cluster2   md-cluster2-md1-578b766797-zvsqv   cluster2   pooldef2-gxxzs2h9fv   metal3://5bd563bc-644b-4fb1-b2cb-e8163388ad44   Running   10m     v1.24.6

For each clusters, we have the following nodes :


.. code-block:: bash

    kanod:kanod-in-a-bottle> KUBECONFIG=~/kanod_lab/kubeconfig/cluster1.kubeconfig kubectl get node -o wide
    NAME                  STATUS   ROLES           AGE     VERSION   INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
    pooldef1-2mx66hn78n   Ready    control-plane   4m3s   v1.24.6   192.168.133.200   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18
    pooldef1-xmpdrfpnbp   Ready    <none>          70s    v1.24.6   192.168.133.133   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18

    kanod:kanod-in-a-bottle> KUBECONFIG=~/kanod_lab/kubeconfig/cluster2.kubeconfig kubectl get node -o wide
    NAME                  STATUS   ROLES           AGE     VERSION   INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
    pooldef2-8ghwjnfcs9   Ready    control-plane   4m8s   v1.24.6   192.168.133.210   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18
    pooldef2-gxxzs2h9fv   Ready    <none>          108s   v1.24.6   192.168.133.132   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18


Pivoting the target clusters
============================

The pivoting process  (see :ref:`Pivoting <kiab-pivot>`)
for a cluster in Kanod is launched after assigning the value ``true``
to the field ``.spec.pivotInfo.pivot`` in the clusterdef resource of this cluster.

For this scenario, the shell script `30-pivot-cluster.sh` is used to launch the pivoting operations
and wait until the end of the process.

The pivoting operations for the ``cluster1`` is launched with this script :

.. code-block:: bash

    CLUSTER_NAME=cluster1 ./30-pivot-cluster.sh

Once the ``cluster1`` cluster is pivoted, we can launch
the same operation of the ``cluster2`` cluster

.. code-block:: bash

    CLUSTER_NAME=cluster2 ./30-pivot-cluster.sh


We can observe the pivoting operation for the 2 clusters
in the following screencast: :


.. asciinema:: asciinema/multi-cluster_30-pivot-mt.cast
    :rows: 55
    :cols: 160
    :idle-time-limit: 1

At the end of the pivoting operations on the 2 cluster, the LCM cluster contains
only the clusterdef and  argocd resources :

.. code-block:: bash

    kanod:kanod-in-a-bottle> ./view-deployment.sh

    -------- LCM CLUSTER ----------
    -------- cluster --------------
    No resources found
    -------- kcp ------------------
    No resources found
    -------- md -------------------
    No resources found
    -------- m3m ------------------
    No resources found
    -------- machine --------------
    No resources found
    -------- bmh ------------------
    No resources found
    ----- Argo CD Application -----
    NAMESPACE   NAME             SYNC STATUS   HEALTH STATUS   REVISION
    argocd      cdef-cluster1    Synced        Healthy         5d2995afdca4a71b5b534e4ee6cf3edac6cea218
    argocd      cdef-cluster2    Synced        Healthy         b744e5ca523e0c8ddf9f8b5283d0a2a5e4ae474e
    argocd      kanod-projects   Synced        Healthy         5398bbfbb7050cea2b38f699d7e0cc04c4062f4a
    -------- ClusterDef -----------
    NAMESPACE   NAME       PHASE     REPOSITORY                                                      BRANCH   PIVOTSTATUS
    capm3       cluster1   Running   https://git.service.kanod.home.arpa/cluster_admin/config.git    master   ClusterPivoted
    capm3       cluster2   Running   https://git.service.kanod.home.arpa/cluster2_admin/config.git   master   ClusterPivoted
    -------- baremetalpool --------
    No resources found


Scaling a pivoted cluster
=========================

Scaling or upgrading operations can be achieved as usual
on a pivoted cluster by updating the cluster definition
in the corresponding the Git repository.

In this scenario, the cluster ``cluster1`` is scaled up.
The number of replicas for the machine deployment of the
worker nodes is updated (new value: ``2``) in the ``cluster1``
config file :

.. code-block:: yaml

  ---
  version: v1.24.6
  controlPlane:
    flavour: mono
    replicas: 1
  [...]
  workers:
    - name: md1
      flavour: default
      serverConfig:
        [...]
      replicas: 2
      baremetalSelector:
        matchLabels:
          kanod.io/mode: vm
  [...]


This config file is then commited and pushed to the ``cluster1`` git repository.


The following screencast shows the scaling process applied
on the cluster ``cluster1``.

.. asciinema:: asciinema/multi-cluster_14-scale-mt.cast
    :rows: 45
    :cols: 160
    :idle-time-limit: 5


We can check that 3 nodes are now deployed for ``cluster1`` :

.. code-block:: bash

    kanod:kanod-in-a-bottle> KUBECONFIG=~/kanod_lab/kubeconfig/cluster1.kubeconfig kubectl get node -o wide
    NAME                  STATUS   ROLES           AGE     VERSION   INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
    pooldef1-2mx66hn78n   Ready    control-plane   32m   v1.24.6   192.168.133.200   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18
    pooldef1-8kc52mmh2k   Ready    <none>          52s   v1.24.6   192.168.133.129   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18
    pooldef1-xmpdrfpnbp   Ready    <none>          29m   v1.24.6   192.168.133.133   <none>        Ubuntu 20.04.6 LTS   5.4.0-144-generic   containerd://1.6.18

