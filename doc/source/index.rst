.. Kanod in a Bottle documentation master file, created by
   sphinx-quickstart on Fri Sep  3 14:36:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: heavy
    :class: heavy

Kanod in a Bottle
=================

**Kanod in a Bottle** is a deployment of Kanod on a single computer using
virtual machines and emulated BMC to model the behavior of actual servers.
Kanod in a Bottle can (should) be used as a starting point for the deployment
of Kanod on real hardware.

.. card:: :ref:`Prerequisites <kiab-prerequisites>`
   :class-body: sd-bg-dark sd-text-white

   In this section we describe how to prepare a laptop/workstation to deploy
   Kanod-in-a-bottle

.. card:: :ref:`Initial Parameters <kiab-parameters>`

   ``params.yaml`` is a small file describing the scenario you want to run
   and the local environement of the server (proxys, etc.)
   We review the content of this file.

.. grid:: 2
    :gutter: 3
    :margin: 0
    :padding: 0

    .. grid-item-card:: :ref:`Standard Deployment <kiab-standard>`
        :class-body: sd-bg-dark sd-text-white

        You should begin with this simple scenario to understand how Kanod works.

    .. grid-item-card:: :ref:`Scaling and Upgrading <kiab-life-cycle>`

        Here we describe basic Day 1 operations: scaling and upgrading a cluster.

    .. grid-item-card:: :ref:`Security <kiab-security>`

        In this scenario, we review the different options to improve the security of
        your deployment with a Vault to hold all your credentials.

    .. grid-item-card:: :ref:`IPAM <kiab-ipam>`

        With an IP address manager in your cluster, you do not need to relie on
        DHCP to allocate addresses to  your nodes.

|

.. rubric:: :heavy:`Broker Mode`


.. grid:: 2
    :gutter: 3
    :margin: 0
    :padding: 0

    .. grid-item-card:: :ref:`Multi Tenancy <kiab-broker>`

        In this scenario, we show how the notion of baremetalpools and broker help
        you to share the computing resources of your data-center between independent
        non trusting clusters.

    .. grid-item-card:: :ref:`Pivoting <kiab-pivot>`

        To improve the resiliency of your deployment, you can use Cluster API pivot
        and transfer day to day management of your managed cluster from the life-cycle
        manager to the cluster itself.

    .. grid-item-card:: :ref:`Multi Clusters <kiab-multiclusters>`

        This scenario shows how to deploy, pivot and manage 2 clusters from the
        life-cycle management cluster with Kanod.

    .. grid-item-card:: :ref:`Network <kiab-network>`

        Configure network on the fly to isolate newly created clusters. This can also
        be used to create complex network interconnection between some of the servers
        of your cluster.

    .. grid-item-card:: :ref:`Autoscaling <kiab-autoscale>`

        Autoscale clusters and expand the servers they relie on.

|

.. rubric:: :heavy:`Host mode`

.. grid:: 2
    :gutter: 3
    :margin: 0
    :padding: 0

    .. grid-item-card:: :ref:`Host resource <kiab-host>`

        Use a host resource to launch a workload.
 
    .. grid-item-card:: :ref:`Multi-tenancy with Hosts <kiab-host-multicluster>`

        Use hosts to launch several independent clusters in a single LCM.

    .. grid-item-card:: :ref:`Hybrid clusters with Hosts <kiab-host-hybrid>`

        Use hosts to launch a cluster with bare-metal servers, kubevirt and
        OpenStack virtual machines.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:

   prerequisites
   initial
   deployment
   life-cycle
   tpm
   broker
   pivot
   multiclusters
   network
   autoscale
   ipam
   host-resource
   host-multicluster
   host-hybrid
