.. _kiab-standard:

===============================
Deployment of Kanod in a bottle
===============================

For the first deployment we will use a very simple parameter file that activates
only the mandatory functions. It should be close to the following file:

.. code-block:: yaml

    minions: 3
    root: path_to_folder_with_enough_available_storage

You may need to define a proxy and if you need mirrors, the associated variables
and certificates (if the mirrors ca are not known).

.. asciinema:: asciinema/00-templates.cast


Retrieving the initial projects
===============================

To bootstrap Kanod, you need a set of initial projects:

* to create a service machine hosting a Nexus repository, a git server and an
  Hashicorp Vault. The nexus repository will be populated with all the Kanod
  artifacts
* to launch the initial virtual machines

``kanod-boot`` is the project booting virtual machines using our declarative
config framework. It installs locally ``kanod-vm``, the utility that launch
instances of virtual machines.

``image-builder`` is the common framework for all operating system images.
It is a wrapper around ``diskimage-builder`` from the OpenStack consortium.
The main focus is integrating making cloud-init really declarative where all configuration
is defined by intents rather than through shell scripts expanded in
the ``runcmd`` command part of cloud-init. This means defining for each element
a specification part represented by a json schema and an activation part implemented
as a set of python functions.

``kanod-node`` is the ``image-builder`` specialization  for OS images
that host a Kubernetes node: it is preloaded with
a container runtime and kubeadm/kubelet/kubectl utilities.

Then the initial image is built. It  requires root privileges as the underlying tool
uses a chroot-ed environment to build the operating sytem image.
Building an OS image takes a while (Usually around ten minutes).

.. code-block:: bash

    01-basic-projects.sh

.. asciinema:: asciinema/01-projects.cast
    :rows: 8
    :idle-time-limit: 1.5


Network creation
================
..  figure:: images/architecture1.png
    :figwidth: 40%
    :align: right

The first step is to create a new libvirt network ``oknet``.

* NAT forwarding is enabled to access the internet
* There is no DHCP server on it as Kanod will manage address allocation.
* The local domain is `kanod.home.arpa`.
* The life-cycle manager `lcm`, the support machine `service` and the optional machine
  broker `broker` are registered in the local DNS.
* Options are used so that `*.service.kanod.home.arpa` is resolved as `service.kanod.home.arpa`
* The same applies to `*.broker.kanod.home.arpa`.

Servers on which the clusters are deployed will initially be created on this network.
In this very simple setting, IPMI (or Redfish), PXE and regular kubernetes traffic will
be on the same network.

After you run this simple command you should see your newly created network in the list of networks
handled by libvirt.

You can add the DNS associated to this network to resolve the kanod.home.arpa domain on your machine.
You need to modify `/etc/systemd/resolved.conf` and add the following:

.. code-block::

    [Resolve]
    DNS=192.168.133.1
    Domains=~kanod.home.arpa

Then you need to restart `systemd-resolved` service.

.. code-block:: bash

    02-mk-network.sh

.. asciinema:: asciinema/02-network.cast
    :rows: 8
    :idle-time-limit: 1.5

Storage creation
================

..  figure:: images/architecture2.png
    :figwidth: 40%
    :align: right

Local storage for support virtual machines and for baremetal servers
is handled in KIAB with a libvirt storage pool. Now you will create a libvirt
storage pool on which virtual machine can create volumes.
The default name of this storage pool is ``okstore`` (it can be configured
in the param.yaml file with the variable ``storage_pool``).

KIAB uses a very simple solution with file backed storage.
You can also use a storage pool backed by an LVM volume group.

The only volume created initially is used by the service machine. It is left
unformatted as the cloud-init will take care of this part.
.. code-block:: bash

    03-mk-store.sh

.. asciinema:: asciinema/03-store.cast
    :rows: 8
    :idle-time-limit: 1.5

Baremetal Servers Emulation
===========================

..  figure:: images/architecture3.png
    :figwidth: 40%
    :align: right

We need now to emulate some baremetal nodes with virtual machines. The
first step is to launch virtual machines with a disk but without other
resources. The command takes an argument which is the number of machines
wanted. If left unspecified five machines are created. The machines are
created but not started.

For the sake of simplicity, the servers are started in BIOS mode as UEFI
requires additional configuration in libvirt.

.. code-block:: bash

    04-minions.sh -s

.. asciinema:: asciinema/04-minions.cast
    :rows: 8
    :idle-time-limit: 1.5

Out of band management of Baremetal Servers
===========================================
IPMI or Redfish can be used to manage those baremetal servers
(depending of the value of  ``bmc_protocol`` in the Kanod configuration file).


By default, we simulate a BMC on each emulated server with python virtual BMC.
The shell script creates a complete configuration for the vbmc daemon and launch it. The
ports used start at 5001. We can list the declared BMC and their characteristic
if we set the VIRTUALBMC_CONFIG variable to the right value.

.. code-block:: bash

    05-bmc.sh

.. asciinema:: asciinema/05-bmc.cast
    :rows: 12
    :idle-time-limit: 1.5

With the Redfish protocol, things are a little different as there is one Redfish server
for each server.

Launching the service machine
=============================
.. admonition:: Do not use in production

    The deployment is only for testing purpose. The whole configuration is hosted
    within the cluster especially the Vault content and configuarion that
    is hosted in a Kubernetes secret. This should never be done in producion.

    There is no redundancy. A single failure will make you loose all your
    cluster definitions.

..  figure:: images/architecture4.png
    :figwidth: 40%
    :align: right

This step launches a first virtual machine with the configuration stored in
``${KANOD_LAB}/service``.

This machine is a single node Kubernetes cluster that holds various services:

* Nexus repository acting as both a docker registry and a maven repository
* Gitea git server holding the cluster definitions
* Hashicorp vault servers holding all the credentials.

It also hosts an NTP server used by all other instances. Kubernetes certificates
relie on tight time synchronization otherwise they may appear as "issued in the
future".

The kubeconfig associated with the cluster is stored in:
``${KANOD_LAB}/kubeconfig/service.kubeconfig``

``config.yaml`` defines the configuration of the virtual machine.
It is structured with the following parts:

.. code-block:: yaml

    name: service
    vm:
        # resources (memory, disk...) allocated to the VM
    disk_setup:
        # how the disk is partitioned
    filesystems:
        # definition of the partitions on the filesystem. They will become
        # persistent volumes in the cluster
    mounts:
        # where the partitions are mounted
    os: focal
    ntp:
        # machine act as NTP server for the domain
    kubernetes:
        # kubernetes configuration as single node cluster.
    network:
        # standard static network configuration.

.. topic:: Kubernetes admission webhook break declarativity promise

    When it is deployed. The script will launch the manifests holding the service
    definitions. There are several steps of deployments with waits between them

    Basically two or three layers are needed:

    * one layer where custome resources (certificates, argo workflows) are defined.
    * admission and mutating webhooks that are associated with resources 
      (certificates, vault injector). It could probably be merged with the previous one.
    * everything else.

    The main issue is that webhooks entrypoints MUST be stable before resources
    using them are loaded otherwise they are iremediately lost (admission) or
    corrupted (mutation). This issue jeopardises Kubernetes promise of being truly
    declarative.




.. code-block:: bash

    06-launch-service.sh -s


.. asciinema:: asciinema/06-service.asc
    :rows: 8
    :idle-time-limit: 1.5

Vault instance
--------------
The configuration file ``templates/vault-init.yaml``
defines the structure of the credential database and
the value of the credentials used by the test deployment.
Again this must not be used in production, at least for the secion
on the definition of credentials
as this means that the credentials exist outside the Vault instance.

There is a single real user ``admin`` with a policy giving him
administrative rights over the resources handled by Kanod. The default password is ``secret`` and
the Vault server is accessible at https://vault.service.kanod.home.arpa

We use several features of Hashicorp Vault :

* Kubernetes roles: Kubernetes clusters are registered withing vault, then we
  can use service account tokens to represent identities in Vault.
* Application roles: an application role identity is defined by two components
  role_id and secret_id. Both can be established by separate paths. Servers
  are usually associated with an application role. This server identity can then
  be used to register the cluster deployed on them.
* Key value secret storage (either version 1 or 2)
* PKI secret engine is used to generate the keys and certificates representing
  services. This is exposed inside clusters with the cert-manager which uses
  service account representing kubernetes roles.

Vault users (real, approles or kubernetes account) can access secrets depending

An application role (approle) is defined for each virtual machine.
There is also a ``boot`` application role used by the ``kanod-vm`` command:
it is used as soon as the end-user has entered the admin password
in place of the admin role which is too powerful.

Each application role has the following assocated domains:

* a space for creating associated certificates. The policy to restrict
  those certificates to the domains managed by the VM is outside the scope
  of the experiment
* a space for managing key/values with or without versioning
  (kv1 and kv2).

As we deploy clusters on our servers, approles are traded for Kubernetes
roles. Each cluster is defined with an authentication entry point and
service account are registered.

On the service machine, the following kubernetes roles are defined:

* cert used by Kubernetes certificate manager to create certificate/key pairs
  for the ingress
* gitea for accessing the Gitea credentials
* nexus for accessing the Nexus credentials
* workflow for the needs of the workflow that must rebuild the content of the
  Nexus
* tpm-auth for an extension that can be used to authenticate servers with their
  TPM.

On the life-cycle-manager only two roles are defined:

* updater that creates clusters and need to populate at least part of the
  approles of the servers.
* argocd that is in fact the argocd-vault-plugin.

.. admonition:: Deprecation warning

    We intend to replace the argocd vault plugin with external secrets that offer
    a better granularity over the scope of the resources in a cluster that can
    access a set of resources in Vault.

.. list-table:: Vault User Interface

  * - .. thumbnail:: images/vault-access.png
           :title: Vault access methods enable

    - .. thumbnail:: images/vault-engines.png
           :title: Vault secret engines enabled

Nexus
-----

The Nexus repository holds the artefacts used by Kanod:

* Container images
* OS images and associated JSON validation schemas.
* Deployment manifests of Kubernetes resources (eg. the Kanod stack used
  by the life-cycle manager).

The Nexus is visible at address https://maven.service.kanod.home.arpa. The
registry part is exposed at https://registry.service.kanod.home.arpa but can
be managed from the former address.

The defined users are a global administrator (admin) responsible for setting
policies and structuring the repository, a maven administrator (maven-admin) 
and a registry administrator (registry-admin).

.. list-table:: Nexus User Interface (Searching components)

  * - .. thumbnail:: images/nexus-maven.png
           :title: Viewing Maven repository contents on Nexus

    - .. thumbnail:: images/nexus-registry.png
           :title: Viewing registry contents on Nexus

Populating the Nexus with Argo
------------------------------
When the instance is created, there is no artefact and the instance can be
populated by rebuilding each project locally and publishing the result on
the Nexus. This process takes a while (usually more than an hour).

We use argo workflows to implement the internal CI that rebuilds
each component. With the argo command installed on your machine, you can
launch an argo ui to view the execution of the workflow with the following command.

.. code-block:: bash

    KUBECONFIG=${KANOD_LAB}/kubeconfig/service.kubeconfig argo server -n argo --auth-mode server --port 8443

The workflow is visible in your browser at ``https://localhost:8443`` in the `default` namespace.

The workflow is parameterized by a yaml file contained in ``templates/versions.yaml``. Although
the regular user does not need to modify this file, it is where developpers can change the
version of the components compiled.

..  thumbnail:: images/argo-workflow.png
    :width: 80%
    :align: center
    :title: Representation of the complete workflow in Argo UI

Gitea
-----

As Kanod promotes gitops, we need a git server to host our cluster
definitions. For this purpose Kanod in a bottle provides a simple
deployment of Gitea a fork of the Gogs project (Go Git Server)
that implements (in Go) a simple but efficient git server.

Gitea interface is visible at https://git.service.kanod.home.arpa

Gitea configuration is given in a file in the template folder
(gitea-config.yaml).


.. list-table:: Gitea User Interface

  * - .. thumbnail:: images/gitea-infra-admin.png
           :title: Infrastructure Admin page

    - .. thumbnail:: images/gitea-cluster-admin.png
           :title: Clusster Admin page

It pre-configures two users and three repositories. Real users definition are hosted in
Vault.  The users represent the Kanod administrator (or infrastructure administrator)
who defines which cluster can be deployed and a cluster administrator
representing a customer of the infrastructure who wants to deploy clusters.

Each project must have an owner who must be a defined user. The projects
contain:

* the baremetal servers configuration (mac address, ipmi credentials, etc)
* the list of clusters authorized to access the infrastructure and the
  templates used for their configuration
* an example of cluster definition

The first two projects are under the control of the infrastructure
administrator, the last one is owned by the customer.


Launching Kanod Life-Cycle-Manager
==================================

..  figure:: images/architecture5.png
    :figwidth: 40%
    :align: right

We launch the life-cycle manager as any other virtual machine. The
address of the machine is 192.168.133.10 and its dns name is lcm.kanod.home.arpa.

.. code-block:: bash

    07-launch-lcm.sh -s

Here are the relevant parts from the configuration file. First we
describe the location from which artifacts are fetched.

.. code-block:: yaml

    nexus:
        maven: 'https://maven.service.kanod.home.arpa'
        docker: 'registry.service.kanod.home.arpa'

Then we specify the version of kubernetes that will be deployed and
the underlying OS of the machine. It would be an Ubuntu bionic if left
unspecified.

.. code-block:: yaml

    os: focal
    kubernetes:
        version: v1.24.6
        role: mononode
        cluster_name: lcm

After a while we can extract the kubeconfig for the newly deployed
management cluster from the admin account on the virtual machine. This is
done automatically by the script and it is stored in
``${KANOD_LAB}/kubeconfig/lcm.kubeconfig`` Note
how we can use the key created in the configuration folder. We set the
kubeconfig environment variable to this newly downloaded file and we
retrieve the pods deployed so far. This is a basic cluster with Calico.

..  figure:: images/architecture6.png
    :figwidth: 40%
    :align: right

we have yet to deploy the stack on it. The configuration file for the server
describes the initial resource manifests deployed: the kanod_operator deployment
stored in the Nexus and its configuration inlined. The configuration is used
not only to describe the location of some services but also to control
the activation of most services as they can be provided externally or are simply
not needed in some scenarios.

After some time that we have compressed for this demo, the pods for the
complete stack are visible even if not deployed yet. We can view pods
for cluster-api, metal cube, ironic or argocd.

Finally our system stabilize and we can view the two argocd
applications deployed for our baremetal host project and our meta-project
containing cluster definition pointers.

.. asciinema:: asciinema/07-lcm.cast
    :rows: 24
    :idle-time-limit: 1.5

The endpoint is the address of the API of our newly created server. The
port is the default one: 6443. The role is an important entry because
it defines the node as a mononode Kubernetes cluster. The same image is
in fact used by target clusters workers or control nodes.

.. code-block:: yaml

    os: bionic
    kubernetes:
        endpoint: 192.168.133.10
        role: mononode
        version: v1.20.7

We specify the content of the Kanod stack which is the set of
applications managing the life-cycle of clusters. We can specify the
stack version. We must configure ironic, the low level application that
control baremetal servers. We must identify the interface on the host
that is used for PXE, the location of the ironic python agent and how
the dhcp is configured. The DHCP configuration let us define several
subnet which is useful in a context where the life-cycle manager
handles different clusters associated to different VLANs. The configuration of the DHCP relays is outside the scope of Kanod.
In our simple case, there is a single subnet which coincides with
``oknet``.

.. code-block:: yaml

    stack:
        ...
        ironic:
            ip: 192.168.133.10
            interface: ens3
            ipa_url: http://192.168.133.100/repository/kanod/kanod/ironic-python-agent/1.1.0/ironic-python-agent-1.1.0.tar
            dhcp:
              subnets:
                - prefix: 192.168.133.0
                  mask: 255.255.255.0
                  start: 192.168.133.128
                  end: 192.168.133.254

Finally also in the stack section, we configure our access to the git
repository we have deployed on gogs. We must specify the location of
the three kind of repositories used: baremetal host definitions,
cluster defition pointers and snapshots which host the clusters
synthesized by the stack from user requirements. For each, we specify
the user and the associated password. We must also provide the
self-signed certificate for the gogs server or other servers on
which the cluster definitions may be stored

.. code-block:: yaml

    stack:
        ...
        git:
        snapshots:
            url: http://192.168.133.101/snapshot.git
            username: infra_admin
            password: secret1
        baremetal:
            ...
        projects:
            ...
        certificates:
            gogs: |
                -----BEGIN CERTIFICATE-----
                ...
                -----END CERTIFICATE-----

Managing the Baremetal Hosts in Kanod
=====================================

We register the baremetal hosts. This command creates a new project
containing the definition of our virtual machines and register them on
the git project watched by the life-cycle manager. After a while the
changes are detected and node introspection is launched. Note that all
the nodes are viewed as a single application because of the
customization script at the root of the folder that enumerates the
resources used. After a while, enumeration is finished and the
baremetal hosts are declared ready.

.. code-block:: bash

    10-bmh.sh

.. asciinema:: asciinema/10-bmh.cast
    :rows: 8
    :idle-time-limit: 1.5

A host definition must mention the address of the IPMI connection.
Other protocols can be used like redfish with real servers but we are
limited to IPMI for Kanod in a bottle. We give the credentials in an
attached secret. The mac address of the interface doing the PXE boot is
mandatory to identify which server is linked to the BMC. Finally we use
the root device hints to identify the disk to erase and burn. The
default is sda but on a virtual machine we must use vda. Note the label
mode and its arbitrary value that will be used later in the definition
of our cluster.

.. code-block:: YAML

    apiVersion: metal3.io/v1alpha1
    kind: BareMetalHost
    metadata:
        name: vmok-1
        namespace: capm3-system
        labels:
            mode: vm
    spec:
        online: false
        bootMode: legacy
        bmc:
            address: ipmi://192.168.133.1:5001
            credentialsName: bmh-credentials-1
        bootMACAddress: 52:54:00:01:00:01
        rootDeviceHints:
            deviceName: /dev/vda

Managing Cluster Definition Templates
=====================================

the infrastructure administrator registers a first customer.
The project folder contains both a custom resource clusterdef which is
a pointer to a git project and a configmap giving the characteristics
of the node flavours accepted on this infrastructure. After a while the
cluster def is registered in the system and a new application is
created to watch for the cluster definition.

.. code-block:: bash

    11-meta.sh

.. asciinema:: asciinema/11-meta.cast
    :rows: 8
    :idle-time-limit: 1.5

.. code-block:: yaml

    apiVersion: gitops.kanod.io/v1alpha1
    kind: ClusterDef
    metadata:
    name: cluster1
    namespace: argocd
    spec:
    source:
        repository: https://192.168.133.101/cluster_admin/config.git
        branch: master
    credentialName: project-creds
    configurationName: infra

The configmap ``infra`` contains a ``config`` field whose content is a
yaml structure. We recommend the use of kustomize with a file and a
configmap generator to generate the map. It also simplifies the
generation of the secret.

.. code-block:: yaml

    apiVersion: kustomize.config.k8s.io/v1beta1
    kind: Kustomization
    resources:
    - cluster1.yaml

    configMapGenerator:
    - namespace: argocd
      name: infra
      files:
      - config=infra.yaml
    generatorOptions:
      disableNameSuffixHash: true

    secretGenerator:
    - name: project-creds
      namespace: argocd
      literals:
      - username=cluster_admin
      - password=secret2


We will now present the
content of the field. First we defne the namespace used. In the current
version, you must use capm3-system. Then we define the labels and
annotations from the baremetal hosts that will appear in the metadata
available for the definition of flavours. In our example we use the
mode label that we have arbitrarily used to distinguish virtual
machines from real servers. We then define some basic network settings
for Kubernetes. The port of the endpoint and the cidr of the
services and pods. We then provide information on the configuration of
our Maven and Docker repositories. This is similar to what we provided
when we launched the life cycle manager.

.. code-block:: yaml

    namespace: capm3-system
    baremetal:
        labels:
        - mode
    network:
        port: 6443
        services:
        - 10.96.0.0/12
        pods:
        - 192.168.192.0/18
    nexus:
        docker: 192.168.133.100:8443
        maven: https://192.168.133.100

The next step is to define flavours. Flavours are variants of
configuration that can be used by target clusters. There are three
kinds of flavours: for control nodes, for worker nodes and for
additional addons started at the launch of the cluster. For each kind
of flavour, the infrastructure administrator must define a default
variant. Here is an example default flavour for the control-plane
nodes. We first define the configuration of Kubernetes.

The role field and its value as control is mandatory for
a control-plane flavour. The endpoint is the address of the endpoint
and is retrieved through the meta-data. This field is also mandatory as
is. We can then define other fields as for the life-cycle manager node.
For brevity we have omitted the content. The important part is the
network whose configuration is completely dependent on your
infrastructure. It can be as simple as in okaas in a bottle or fairly
complex with multiple cards, bonds and vlans.

.. code-block:: yaml

    controlPlaneFlavours:
    - name: default
      kubernetes:
        role: control
        endpoint: '{{ds.meta_data.endpoint_host}}'
      ntp:
        ...
      admin:
        ...
      network:
        ...
      proxy:
        ...

The flavours for the workers are similar to the flavours for
control node. The main change is the role.

.. code-block:: yaml

    workerFlavours:
    - name: default
        kubernetes:
            role: worker
        ntp:
            ...
        network:
            ...
        proxy:
            ...
        admin:
            ...

This is the addons flavours. Here is a simple example for Calico. You
can access other resources like multus and sriov but this requires more
complex configuration to ensure the correct seqencing of deployment.
Note the use of envsubst, the definition of variables and the use of
predefined variables like NEXUS.

.. code-block:: yaml

    addonFlavours:
    - name: default
        kustomization:
        resources:
            - http://${NEXUS}/repository/okaas/com/orange/okaas/calico/${CALICO_VERSION}/calico-${CALICO_VERSION}.yaml
        variables:
        CALICO_VERSION: v3.16

Creating a cluster
==================

..  figure:: images/architecture7.png
    :figwidth: 40%
    :align: right

The end-user can now create his cluster.
The definition will use the templates
above.

.. code-block:: yaml

    ip: 192.168.133.200
    name: cluster1
    domain: foo.local
    version: v1.20.7
    controlPlane:
      os: bionic
      os_version: 1.3.5
      baremetal:
        matchLabels:
          mode: vm
    workers:
    - name: md1
      replicas: 1
      baremetal:
        matchLabels:
          mode: vm
      os: bionic
      os_version: 1.3.5

The first lines define the endpoint IP, the
name of the cluster and the domain.

The version specified is the Kubernetes version and is valid for all nodes
of the cluster.

We define one control-plane and one or several
worker nodes. In each group, the number of replicas must be specified. The OS version is the version of the image used. Flavours can
be explicitly specified with the ``flavour``
keyword.

The tag baremetal introduces selectors for
server. We can match on labels or annotations
of the servers.

The script commits the file ``config.yaml``
in the git project. This will trigger the reconciliation process in Kanod.

.. code-block:: bash

    12-cluster.sh

.. asciinema:: asciinema/12-cluster.cast
    :rows: 6
    :idle-time-limit: 1.5

We can now check the evolution of resources in the LCM cluster. During the
first period we look at the evolution of pods in the life-cycle manager.
As soon as the application is registered, the cluster is defined and the
availability of the cluster kubeconfig triggers the second part of the wait
where the state of baremetal hosts and cluster api resources are watched.

At the begining, baremetalhosts are registered in the system and inspected.
They will then become available. Cluster Api will then choose one of the
machines to install a control-plane. When it is deployed, the machine deployment
with a single worker is deployed. At the end of the process we can check on
the target cluster that the nodes are visible.

.. asciinema:: asciinema/13-wait.cast
    :rows: 36
    :cols: 160
    :idle-time-limit: 1

