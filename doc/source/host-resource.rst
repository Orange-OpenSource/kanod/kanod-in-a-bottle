.. _kiab-host:

=================
The Host Resource
=================

Host represent an abstract compute resource on which workloads can be configured.
The host controller allocates a real resource (for example a bare-metal server)
to the host.

A workload is defined by an OS image and a cloud-init configuration. In this
example we show how to define and launch a simple workload.

To enable the support of Hosts in Kanod, the following parameter must be defined 
in ``params.yml``:

.. code-block:: yaml

    hybrid:
        enabled: true

We assume that all the support resources have been deployed (scripts up to 
``06-launch-service.sh``).

We also launch the life-cycle manager and we populate the baremetal servers
in the ``infrahost`` namespace:

.. code-block:: bash

    ./07-launch-lcm.sh -s
    ./10-bmh.sh

After a while the BareMetalHost objects should appear as ready.

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get bmh -A
    NAMESPACE   NAME     STATE       CONSUMER   ONLINE   ERROR   AGE
    infrahost   vmok-1   available              false            3m9s
    infrahost   vmok-2   available              false            3m9s
    infrahost   vmok-3   available              false            3m9s
    infrahost   vmok-4   available              false            3m9s

We can now create a Host resource in the default namespace:

.. code-block:: bash

    kubectl apply -f - <<EOF
    apiVersion: kanod.io/v1alpha1
    kind: Host
    metadata:
    name: host
    spec:
    image:
        checksum: https://maven.service.kanod.home.arpa/repository/kanod/kanod/focal-v1.24.15/1.13.26/focal-v1.24.15-1.13.26.qcow2.md5
        checksumType: md5
        format: qcow2
        url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/focal-v1.24.15/1.13.26/focal-v1.24.15-1.13.26.qcow2
    userData:
        name: user-data-secret
    metaData:
        name: meta-data-secret
    online: false
    hostSelector:
      matchLabels:
        host.kanod.io/kind: baremetal
    EOF

We need to define two secrets. One for the ``user-data`` and another for the ``meta-data``.
First we define a regular user-data file. Note that we use ``kanod-configure`` but there is no support to
hide the way it is implemented :

.. code-block:: bash

    cat > /tmp/user-data <<EOF
    #cloud-config
    write_files:
    -   path: /etc/kanod-configure/configuration.yaml
        content: |
        admin:
            passwd: secret
        network:
            ethernets:
            ens3:
                dhcp4: true
            version: 2
    runcmd:
    - "kanod-runcmd"
    EOF

and the secret themselves can be quickly defined as :

.. code-block:: bash

    kubectl create secret generic meta-data-secret --from-literal=metaData=
    kubectl create secret generic user-data-secret \
      --from-file=value=/tmp/user-data --from-literal=format=cloud-config

The status of the Host resource should quickly evolve:

.. code-block:: bash

  status:
    addresses:
    - address: 192.168.133.34
      type: InternalIP
    - address: fe80::67e0:edc2:ab17:5eea%ens3
      type: InternalIP
    - address: localhost.localdomain
      type: Hostname
    - address: localhost.localdomain
      type: InternalDNS
    bootMACAddress: "52:54:00:01:00:01"
    conditions:
    - lastTransitionTime: "2024-03-11T10:51:46Z"
      status: "True"
      type: AssociateBMH
    lastUpdated: "2024-03-11T10:51:46Z"
    nics:
    - MAC: "52:54:00:01:00:01"
      name: ens3
    - MAC: "52:54:00:01:00:01"
      name: ens3

And one of the BareMetalHost is selected:

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get bmh -A
    NAMESPACE   NAME     STATE         CONSUMER   ONLINE   ERROR   AGE
    infrahost   vmok-1   provisioned   host       true             3h7m
    infrahost   vmok-2   available                false            3h7m
    infrahost   vmok-3   available                false            3h7m
    infrahost   vmok-4   available                false            3h7m

You should be able to log to the admin account at the address given in the
status of the Host resource:

.. code-block:: bash

    ssh admin@192.168.133.34

