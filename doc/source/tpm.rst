.. _kiab-security:

===============================================================================
Deploying kanod in a bottle with Vault and TPM based authentication of machines
===============================================================================

The first deployment did not se Vault on the life-cycle manager. We will
show how to reference Vault resources from the servers and from the clusters.
We will present two way to attach a policy (the right to see a defined set
of credentials on Vault) to a server:

* Using traditional Vault app role
* Using the TPM of the servers.

Kanod with Vault
================
It is possible to use a Vault instance to keep the credentials of Kanod
life cycle manager and of target clusters. In Kanod in a Bottle, the vault
instance is hosted on the service machine but any external Vault can be used
with Kanod.

When Vault is used, credentials do not appear in resource manifests any more,
instead we use "pointers" describing where the credential is located in the
Vault. This is different from solution like SOPS where encrypted credentials
are directly stored in manifests. We think that using pointers has several
advantages:

* Manifest are cleaner, there is less risk that an encrypted text is
  incorrectly modified. Key rotation is also easier.
* If an old key is compromised, it does not necessarily means that the attacker
  has access to the text values of credentials encrypted with this key.

There are two parts in the integration:

* Machine manifests for which we have developed a solution so that they can
  access Vault using appRoles. There are several ways to transmit the approle
  credentials to the servers.
* Kubernetes resource manifests where we relie on existing solutions such as
  external secrets and Kubernetes Vault plugin.

When new machines are launched, they are registered with the appRole
authentication method. When a cluster is launched, those initial credentials
are used to trade it against cluster authentication that will be used by pods.

Accessing Vault in servers
--------------------------
Authentication against Vault is performed by `kanod-configure` using
approle credentials associated to the machine. In the standard version, both
parts of the credential are provided either by `kanod-vm` or by the updater
pod in the life-cycle-manager that creates templates for Cluster-API.

For example the LCM contains the following part:

.. code-block:: yaml

    vault:
      url: https://vault.service.kanod.home.arpa
      role: lcm
      role_id: kanod-lcm-id
      ca: |-
        -----BEGIN CERTIFICATE-----
        ...

`kanod-vm` will add a `secret_id` field using the user credentials to access
the Vault engine and create the missing part.

When the server is launched, cloud-init will call the `kanod-configure` utility
embedded in all Kanod servers. The library is responsible for the inital connection to
Vault, it will replace speciffically identified values in the manifest with the
designated content stored in Vault before the  configuration manifest is
further interpreted.

This is one of the earliest step performed by `kanod-configure` but we still
need the network to access Vault so network configuration **cannot** use
Vault encrypted values.

Coding values in servers manifests
----------------------------------
Any field value begining with `@vault:` is interpreted as a reference to a value
stored in Vault. For example an admin password can be stored using the KeyValue
secret engine version 1 in the following way:

.. code-block:: yaml

    admin:
      passwd: '@vault:kv1:admin:password'

It is also possible to access the PKI secret engine and directly reference the
certificate authority certificate:

.. code-block:: yaml

    spec:
      ingress:
        key:
          value: '@vault:pki-key:lcm'
        certificate:
          value: '@vault:pki-chain:lcm'
        ...

Pki certificate contents are described in the Vault preamble of the machine
description. Note that the snippet above is coded in Yaml in a string which is a field of the LCM manifest. That is
why this value begins with ``@vault:yaml:`` so that the substitution performed by kanod-configure is
done recursively on the contents of this field.

Accessing Vault in Kubernetes
-----------------------------

Two methods are offered to access Vault inside the life-cycle manager cluster to populate manifests under the
responsability of the infrastructure manager. Both use Kubernetes authentication where registered service accounts
ar associated to Vault policies and can access the Vault system using their token as credential.

The first one uses the ArgoCD Vault plugin with a service account associated to the updater pod that synthesizes
clusters. Manifests values can use the ArgoCD vault syntax and the patterns in angle brackets are substituted when
the manifests are imported from git.

For example baremetalHost credentials are coded in the example with this approach. Note the use of annotations to
select the version of the KeyValue engine used.

.. code-block:: yaml

    secretGenerator:
    - name: bm-credentials-1
      literals:
      - username='<path:secret/baremetal/bmc/#username>'
      - password='<path:secret/baremetal/bmc/#password>'
      options:
        annotations:
          avp.kubernetes.io/kv-version: '1'

This solution is only used for the host and meta-project applications that are
under the control of the infra provider because only one set of credentials (service account)
can be used by the argocd plugins. It must not be used by clusters.

.. warning::

    Because of its limitations the method may be deprecated in the future 
    and replaced by the next solution.

The other solution is using the external-secrets operator (originally from GoDaddy).
A secret provider (here Vault) must first be configured. It describes the service
account used and how to access Vault. The certificate of the CA is stored at
a standardized place in a Kanod cluster. The provider also specify the path
and the version of secrets used.

.. code-block:: yaml

    apiVersion: external-secrets.io/v1beta1
    kind: SecretStore
    metadata:
      namespace: ns
      name: vault-backend
    spec:
      provider:
        vault:
          server: https://vault.service.kanod.home.arpa
          caProvider:
            type: ConfigMap
            namespace: kanod
            name: config
            key: vault_ca
          path: secret
          version: v1
          auth:
            kubernetes:
                mountPath: "kubernetes/lcm"
                role: k8sVaultRole
                serviceAccountRef:
                  name: serviceAccountName

Then the user of the provider can declare external secrets that will be
transformed into secrets by the operator. Entries in external secrets
describe a field of the secret: its name and where the value is stored in
the Vault tree.

.. code-block:: yaml

    apiVersion: external-secrets.io/v1beta1
    kind: ExternalSecret
    metadata:
      name: es-example
      namespace: ns
    spec:
      refreshInterval: "15s"
      secretStoreRef:
        name: vault-backend
        kind: SecretStore
      target:
        name: secret-example
      data:
      - secretKey: field1
        remoteRef:
          key: path
          property: field2

It will create a secret named `secret-example` in the same `ns` namespace that
contains a field `field1` whose value is extracted from the record at subpath 
`path` under the root designated in the `vault-backend` secret store. The field
accessed in the record is `field2`.

External secrets impose that Vault values are only used to create secrets but
this limitation also ensures that secrets are the only kind of manifests that
require a high level of protection.

.. note::

    This solution could be used with multiple service account and may be generalized
    in the future for values at the level of each cluster.

Goals of TPM based authentication
=================================

When Kanod uses Vault to store credentials, machine secrets are associated
with a Vault approle. Authentication for an approle requires two parts:

* a fixed role-id that can be queried
* a secret-id generated each time it is accessed on Vault.

For machines, the role-id is supplied by the cluster administration in the
cluster definition stored in git but the secret-id is generated by the
life-cycle manager. Access to the git is not enough to get access to machine
secrets but a compromised life cycle manager or a rogue infrastructure
administrator could give access to all the secrets of all the machines.

To mitigate this risk, it is necessary to give part of the authentication
material by another route and make it available only to the machine.
This is what is achieved with TPM based authentication.
The life-cycle manager only knows the role-id
which is in fact directly associated to the baremetal host. This role-id
is not known by the agent generating secret-ids: TPM based authentication
achieves a complete separation of the paths for authentication materials.

Implementation
==============
TPM based authentication
------------------------
A TPM is a small circuit present on all servers. It can perform cryptographic
operations (encryption, signature) and it contains a root key that
never leaves the chip. On the other hand, a TPM has very limited memory and
the user is expected to manage derived cryptographic materials by exporting
it from the TPM and importing it back. The TPM protects exported materials with
its own key so that it is impossible to extract sensitive information from those
exports.

The root key stored in the TPM is not directly usable but can be
used to derive primary keys. It is possible to deterministically generate
a primary key when needed but they cannot be used to directly sign or encrypt
an external data. Instead, the user must generate secondary keys. Those
keys cannot be deterministically generated but they can be reinstantiated
from exportable components that can only be used on the TPM circuit.

To perform a TPM based authentication, we will generate a pair of
secondary private/public key in the TPM and associate it with the machine in
a trusted (but not necessarily secured) database. To
check that a request comes from a given machine, a challenge (a text to sign)
is generated and given with the material needed to reconstruct the private
key. Only the machine can sign the request anb be verified with public key.

The agent performing the verification has the capability to generate and read the
secret-id part of the approle of a server but not the role-id that is only present
in the life-cycle manager and the server. If the TPM authentication succeeds, the
verification agent will generate and forward the secret-id to the server.

Evolution of approles and policies
----------------------------------
Roles are no more associated to clusters but directly to machines. This means
that policies are no more in a one to one corespondance with approles. Rather
there is a single policy for each cluster and the machine is associated to a
policy by the life-cycle manager when it is enroled in a cluster. The binding
is removed as soon as the baremetal host leaves the cluster.

registrar service
-----------------
The life-cycle manager hosts a controller known as the registrar:

* it is in charge of generating the key pair associated with a baremetal host
  and host the information on the Vault server.
* it watches baremetal host consumers and associates the baremetal approle with
  the right cluster.

When a new baremetal host is created, there is no labels related to TPM
associated with it. The registrar creates several labels:

* `tpm` represents the state of the registration it transitions from
  `registering` to `registered` state. A no-tpm value indicates the absence of
  support for TPM on this host.
* `tpm-role-id` is the role id part of the approle associated to the machine
* `tpm-key` is an internal data used by the registrar during the registration
  process to uniquely identify the machine.

The registrar will launch an OS image on the new baremetal host. This image
will create the primary and the secondary keys and send the exportable part
of the private key with the public key back to the registrar. The registrar
will store this material on Vault and create the approle. It will fetch the
role-id from Vault and store it as a label on the baremetal host. The registrar
MUST NOT be able to generate/access the secret-id associated to the approle.

tpm-auth service
----------------
The secret-id is no more generated by the
life-cycle manager but by a separate application hosted near the Vault service.
It could be modified to be a Vault plugin. In the current setting it is just
a regular application with the capability to access part of Vault hosting
key materials for the TPM and that can generate secret-id for the baremetal
hosts approles. In Kanod in a bottle this is the tpm-auth service on the
Vault machine. The way it is bootstrapped in Kanod in a bottle should not be
considered as production grade as we have chosen to store some sensitive
materials on the disk to simplify the deployment.

The tpm-auth service creates a challenge in response to an authentication service
and send it back with the exported private key material. If the machine replies
with a correct signature, the service creates a secret-id and send it back to
the machine. The tpm-auth service MUST NOT have access to the role-id of the
machine.

Evolution of the updater
------------------------
The updater will first ensure that baremetal host used for cluster creation
are in the registered state for the TPM. It will also use the Metal3Data
mechanism to transfer the role-id from the baremetal host to the cloud-init
configuration of the machine. It will be exposed as role-id.

The updater must also provides the entrypoint and certificate
for the tpm-auth service in the configuration of each node machine.

Deployment
==========
It is necessary to have an instance of libvirt with virtual TPM enabled. On
Ubuntu (it may change on 22.04) it is necessary to install manually the plugins:

* Install libtpms (https://github.com/stefanberger/libtpms.git) with openssl
  (`--with-openssl`) as in
  https://github.com/stefanberger/libtpms/wiki#build-a-package-on-ubuntu
* Install swtpm (https://github.com/stefanberger/swtpm.git) as in
  https://github.com/stefanberger/swtpm/wiki#build-deb-package-ubuntu-debian
* Install swtpm-tools

The procedure for Ubuntu can be automated with the script ``swtpm.sh``
Machines can now be created with virt-install `--tpm` option. In that case
devices `/dev/tpm0` and `/dev/tpmrm0` are visible in the guest machine.

The only change needed in the initial configuration is declaring the use
of the TPM:

.. code-block:: yaml

    vault:
      enabled: true
      tpm: true

The major changes are visible in the cluster where the tpm option replaces the
user specified role-id.

Note: base VM for nodes must have the virtual TPM enabled. It is necessary
to recreate those VMs if they were launched without the TPM option
(`04-minions.sh`).


After a while, the registrar is launched on the baremetal hosts. A provisionning is
triggered. The transient OS sends back to the registrar the following piece of
information that is stored as labels:

.. code-block:: bash

    kanod:~> kubectl get bmh -n capm3 vmok-1 -o "jsonpath={.metadata.labels}" | python3 -m json.tool
    {
        "app.kubernetes.io/instance": "baremetal-hosts",
        "cluster.x-k8s.io/cluster-name": "cluster1",
        "kanod.io/interface": "ens3",
        "kanod.io/mode": "vm",
        "kanod.io/tpm": "registered",
        "kanod.io/tpm-key": "aa149b30-41e9-4623-a2b7-6d113d170fe6",
        "kanod.io/tpm-role-id": "4343ee7c-4d0a-024b-7995-fac66ff475da"
    }

The TPM key is just a transient object used to establish trust between the OS image and
the registrar. The OS image is started with the following cloud-init:

.. code-block:: bash

    kanod:~> kubectl get secret -n capm3 userdata-vmok-1 -o 'jsonpath={.data.userData}' | base64 -d
    #cloud-config
    runcmd:
    - kanod-runcmd
    - echo $? > /etc/kanod-configure/status
    write_files:
    - content: |
        name: vmok-1
        tpm:
          ca: |
            -----BEGIN CERTIFICATE-----
            MIIDBzCCAe+gAwIBAgIUZPBbwFMYAqUYhcFNyQ44tiHjvQcwDQYJKoZIhvcNAQEL
            BQAwEzERMA8GA1UEAwwIS2Fub2QgQ0EwHhcNMjExMDAzMDY0NTA3WhcNMjYxMDAy
            MDY0NTA3WjATMREwDwYDVQQDDAhLYW5vZCBDQTCCASIwDQYJKoZIhvcNAQEBBQAD
            ggEPADCCAQoCggEBAOAk+AQc9ChEC9/chYx7WnePKYd06pDsumMjdZEDjJP9vueM
            M1hcoaZ5C6lOKD1gcLUb+8iANoz8oMdswVbTfyYk0OnI2qrsyBdzBrX6yYKBCq8G
            YBuvAXals7Sg1nZ5w9NS9LwHpzBnP3EsKASa1FJdw0YJR3rT8eMVMXAPEyFpcX0K
            fsZ/MTV4ksDPeDe9cRhBy/X4fzsAuwDVwCEyEURzEfYhyPIH87PTmfu9U13YsRWc
            g4T/Q3PQkvQPDgyZ8HmhCNDBhTHxvsSid05I3gz3xLGhs6YZj9F6jwBZ7rBhtIPt
            4Uun/rXS9lYj0ZHdJTT4oaQ6eag89hLohcONINECAwEAAaNTMFEwHQYDVR0OBBYE
            FL66gATZdSaajlk6QYNbX1OeeF7CMB8GA1UdIwQYMBaAFL66gATZdSaajlk6QYNb
            X1OeeF7CMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBANpHEfYM
            aoDxkA+6m7EPssRbLMLh/DbcMtNcrwgurG0HGgiDk6NpQZC0/spQ8s/VXm3a5WgN
            IpcTxkANBUrI52C9wMvJjNlZ0rsWektkGGH/jenmorMB5qav7mGq1JW39hcpAuLT
            K9/kF9btdRnNbL0fQdNLd6cjMdSQj9EYXGqEe54qOKXe9pjb8DyivhurRUrvlbxu
            sho6GmuzwRmOMBcEMPkejoZAfefeuIWKEawb1XO65YouZDokMAJU3LeaNwVpPc2z
            mCyBitLvzpqWH3t4mQPCbamZBquIffyjgUyamyXOChytp0VQ/EBUzhcKpM3QZYeS
            +TIiDjR0PVfnSFo=
            -----END CERTIFICATE-----
          key: aa149b30-41e9-4623-a2b7-6d113d170fe6
          url: https://192.168.133.11:443/register
      path: /etc/kanod-configure/configuration.

The TPM role-id is extracted by the registrar from the Vault. It will be used as half of
the key by the server and will be sent to its cloud-init through the meta-data with the
mechanism that let export BaremetalHost label to the running instance.

Here is how you can sign-in on the vault as an administrator and look at the values
stored by the registrar. Note that the private key is really encrypted with the TPM and
the credential cannot be used without it. Part of the exchange has been redacted for
readability.

.. code-block:: bash

    kanod:~> export VAULT_ADDR=https://vault.service.kanod.home.arpa
    kanod:~> export VAULT_CACERT=~/kanod-in-a-bottle/certs/ca.cert
    kanod:~> vault login --method=userpass username=admin
    Password (will be hidden):
    Success! You are now authenticated. The token information displayed below
    is already stored in the token helper. You do NOT need to run "vault login"
    again. Future Vault requests will automatically use this token.

    ...

    kanod:~> vault kv get -mount=kv machines/vmok-1
    ===== Secret Path =====
    kv/data/machines/vmok-1

    ======= Metadata =======
    Key                Value
    ---                -----
    created_time       2023-02-22T21:59:53.714759464Z
    custom_metadata    <nil>
    deletion_time      n/a
    destroyed          false
    version            1

    ====== Data ======
    Key         Value
    ---         -----
    key.ctxt    utzA3gAA...
    key.priv    AN4AIOpL...
    key.pub     ARYAAQAL...
    pub.pem     LS0tLS1C...

If you base64 decode `pub.pem`, you can extract a standard public key in PEM format. It is used by the `tpm-auth`
pod on the Service machine to authenticate the server.

.. code-block:: bash

    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx5djc+5Ox2pubxGy8uwW
    tkj2rrGUs71I+CAlCJ/J5zjN19LWQ3V8E1MKcxCJx/dDBY6qo2K6gJUCMwuZ6j7h
    H1FUUgSVsh1zu2HEpE6OcFkaL8jq53iOo1C//a4VzPLL10RinFDfS5d7BhRQy7eP
    hD4P/M/nvSHNeNJTH2HKMcDvK7pCuWc9FC+tJdcWM3ss+JnFQips2/biwgS5MQ5O
    kH66HHq4o88JsbvNbjXqBxZPrLNqMz5TCDdP0uPS0sm1zKJo1HFB2ECFOg4Wy91T
    XbTpQ3pdhB+z5NcfU2TIiJyhNFeDMpQh4h/lvzn3ep1mo0tlnnrob/aGGWRJ3SuS
    ZQIDAQAB
    -----END PUBLIC KEY-----

The next step occurs when the baremetalhost is associated to a cluster. This
event is watched by the registrar. As a result it modifies the policies associated
to the role of the baremetal host. Here are queries against a node not associated
to any cluster and another belonging to `cluster1`:

.. code-block:: bash

    kanod:~> vault read auth/approle/role/vmok-2/policies
    Key               Value
    ---               -----
    token_policies    []
    kanod:~> vault read auth/approle/role/vmok-1/policies
    Key               Value
    ---               -----
    token_policies    [cluster1]


Here are the policies associated to the `registrar` on the life-cycle-manager
and the `tpm-auth` on the service machine:

.. code-block:: bash

    kanod:~> vault policy read lcm-registrar
    path "kv/data/machines/*" {
      capabilities = ["read", "list", "create", "update"]
    }
    path "auth/approle/role/+/role-id" {
      capabilities = ["read"]
    }
    path "auth/approle/role/+/policies" {
      capabilities = ["read", "create", "update"]
    }
    path "auth/approle/role/+" {
      capabilities = ["create", "update"]
    }

    kanod:~> vault policy read service-tpm-auth
    path "kv/data/machines/*" {
        capabilities = ["read", "list"]
    }
    path "auth/approle/role/+/secret-id" {
      capabilities = ["create", "update"]
    }

The first component can register the material used by the TPM in the `kv/data/machines`
sub-tree of Vault. It can also create approles, their `role-id` and associate a
policy to them but it **cannot** create the `secret-id` associated to the approle.

This part is fullfilled by the `tpm-auth` that can read the TPM material to check that
it is communicating with the right server. It can also create the secret-id that it
will send back to the server after correct authentication.
