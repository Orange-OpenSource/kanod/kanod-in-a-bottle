.. _kiab-parameters:

============================================
Manifests initialization
============================================

00-mk-template.sh
=================

The configuration of each server in the *Kanod in a bottle* deployment is defined by
a Yaml configuration file. We must also define the templates for target
clusters and record the servers in use.

When we use *Kanod in a bottle*, a suitable set of configuration files can be generated
using the initial generator ``00-mk-template.sh`` with a YAML files containing
information about the environment. ::

    00-mk-template.sh <param-file>

If the file name is omitted, a file ``params.yaml`` is used as default. The
command generates files in the specified root folder but also a small parameter
file in ``${HOME}/.kiab_conf``. The location of this file can be overriden with
the ``KIAB_CONF`` shell variable. This file contains the definition of the
essential shell variables used by the scripts like the location of the root
folder.

``00-mk-template.sh`` generates only templates in a deterministic way. It can
be called several times with the same parameter file and will generate the same
templates. It can even be called with minor modifications in the parameter file
as long as the steps involving the modified parameters are launched again.

The parameter file
==================

This is a YAML file. the following entries are recognized:

.. glossary::
    root: `<path>`
      the root folder used for the deployment. It will contain the folders
      describing each virtual machine, the infrastructure and cluster templates
      but also all the disk images. If not defined, the default value is the
      ``lab`` folder in the location of the *Kanod in a bottle* scripts.
      In the following, we will assume that ``${KANOD_LAB}`` contains the
      location of this folder as if you sourced the definition in
      ``${KIAB_CONF}``.

    git: `<url>`
      the git url for the server (and group) holding the Kanod sources. It is a mandatory
      parameter.

    minions: `<number>`
      the number of servers to emulate.

    cluster-config: `<file path>`
      optional path to a yaml file that will replace ``config.yaml`` as definition
      of the cluster.

    bmc_protocol: `<protocol>`
      The protocol used to access the Virtual BMC of the emulated servers.
      Authorized values : ``ipmi`` or ``redfish`` or ``redfish-virtualmedia``.
      If not defined, the default value is ``ipmi``.

    redfish_broker
      this section contains the configuration of the redfish broker.

      .. glossary::

        broker_enabled: `<boolean>`
          a boolean controlling the use of the redfish broker


    pivot_enabled: `<boolean>`
      A boolean controlling the pivoting of the management functions to the target cluster.

    scaling_enabled: `<boolean>`
      A boolean controlling the scaling of the target cluster (number of worker nodes).

    autoscale: `<boolean>`
      A boolean controlling the use of autoscaler feature in broker mode.

    k8s_upgrade:
      this section contains the configuration of the upgrade of the kubernetes version for the target cluster

      .. glossary::

        k8s_upgrade_enabled: `<boolean>`
          a boolean controlling the upgrade of the kubernetes version for the target cluster

        k8s_upgraded_version: `<string>`
          The target kubernetes version for the upgrade.
          If not defined, the default value is the higher kubernetes version in the built OS images.

    mode_multi_tenant: `<boolean>`
      A boolean indicating if the target cluster will be deployed in a dedicated namespace
      (the name of this namespace is the contenation of 2 strings separated by a dash character :
      the clusterdef resource namespace and the name of the cluster)

    mode_multi_cluster: `<boolean>`
      If true, two target clusters (named `cluster1` and `cluster2``) will be deployed.

    storage_pool: `<string>`
      Name of the libvirt storage pool on which virtual machine can create volumes.

    burner_build: `<boolean>`
      A boolean controlling the build of the (non mandatory) burner-builder module.

    os_debug: `<boolean>`
      A boolean indicating if the debug mode is activated for the build of the OS in the nexus.

    switch: `<boolean>`
      A boolean indicating if the cluster is in an isolated network created with a virtual switch.

    ipam: `<boolean>`
      A boolean indicating if we use DHCP (false) or an IPAM in the management cluster (true) 
      to allocate IP addresses to servers.

    byoh_provider:
      this section contains the configuration of the Bring Your Own Host (byoh) support

      .. glossary::

        enabled: `<boolean>`
          a boolean controlling the support of the byoh support.
          The default value is ``false``

        cp: `<boolean>`
          a boolean indicating if the controlplane node(s) of the target cluster will be deployed with the byoh provider.
          The default value is ``false``

        worker: `<boolean>`
          a boolean indicating if the worker node(s) of the target cluster will be deployed with the byoh provider.
          The default value is ``true``

        byoh_in_pool: `<boolean>`
          a boolean indicating if the byoh host(s) are managed in a pool.
          The default value is ``false``


    kamaji_provider:
      this section contains the configuration of the Kamaji support

      .. glossary::

        enabled: `<boolean>`
          a boolean controlling the support of the Kamaji support.
          The default value is ``false``

    hybrid:
      this section contains the configuration of Host provider for hybrid cluster

      .. glossary::

        enabled: `<boolean>`
          a boolean controlling the support of the Host provider.
          The default value is ``false``

        kubevirt: `<boolean>`
          a boolean indicating if the Host kubevirt provider is enabled.
          The default value is ``false``

    vault
      this section contains Vault related configurations.

      .. glossary::

        enabled: `<boolean>`
          a boolean controlling the use of the proxy

        tpm:  `<boolean>`
          a boolean controlling the use of (emulated on VM) TPMs to authenticate
          servers with the Vault.

    proxy
      This section describes the configuration of proxies for the deployment.

      .. glossary::

        enabled: `<boolean>`
          a boolean controlling the use of the proxy

        http: `<url>`
          The value of ``http_proxy`` within the deployment

        https: `<url>`
          The value of ``https_proxy`` within the deployment

        no_proxy: `<list>`
          The value of ``no_proxy`` within the deployment

        git_proxy:
          Overrides ``http(s)_proxy`` for the external git server containing KIAB sources.

    ssh
      Parameters for SSH accounts on VMs

      .. glossary::

        default
          A boolean. If set to true, the end user default key in
          ``~/.ssh/id_rsa.pub`` is automatically included

        keys
          A list of public keys that will be accepted for the
          ``admin`` account on the virtual machines.

    certificates
      This element must be a map of certificates trusted by the
      virtual machines. The key is usually the qualified name
      of the machine owning the certificate and the value must
      be a certificate in PEM format (including headers and footers)

    ntp
      A list of NTP servers that will be used by the deployment.
      As a lot of certificates with strict validity dates are used, it is very important to have synchronized clocks between the different machines part of the deployment.

.. code-block:: yaml

  ---
  # Where we will store the definitions and all the artefacts if it is not ./lab
  root: /home/<your home dir>/kanod_lab

  # Git URL we use to access kanod code (mandatory)
  git: 'https://gitlab.com/Orange-OpenSource/kanod'
  # Number of emulated bare-metal hosts spawned for the lab
  minions: 5

  ## Protocol used to access BMC : "ipmi" or "redfish"
  bmc_protocol: ipmi

  ## target cluster deployed in a dedicated namespace ?
  mode_multi_tenant: false

  ## 2 target cluster deployed ?
  mode_multi_cluster: false

  ## libvirt storage pool name
  storage_pool: okstore

  ## do we build burner image ?
  burner_build: false

  ## OS built with debug mode ?
  os_debug: false

  ## target cluster in an isolated network ?
  switch: false

  vault:
    # Do we use vault ?
    enabled: true

  redfish_broker:
    # Do we use redfish broker ?
    broker_enabled: false

  ## do we build pivot management function to the target cluster ?
  pivot_enabled: true

  ## do we scale the target cluster ?
  scaling_enabled: true

  ## do we upgrade the target cluster ?
  k8s_upgrade:
    k8s_upgrade_enabled: false
    k8s_upgraded_version: v1.21.4

  # Proxy settings
  proxy:
    enabled: true
    http: http://my_proxy_url
    https: http://my_proxy_url
    # No proxy settings: probably the minimal one.
    no_proxy: 192.168.133.1,192.168.133.10,192.168.133.100,192.168.133.101,192.168.133.102,192.168.133.0/24
  # DNS servers (more or less mandatory)
  dns:
    addresses:
    - dns1_ip
    - dns2_ip
    search:
    - domain1
    - domain2
  ssh:
    # Do we copy our standard public ssh key
    default: true
    # Additional keys as a list
    keys:
    - ssh-rsa blah blah blah

  # Additional trusted certificates. For example your main git repo
  certificates:
    kanod-git-repo: |
      -----BEGIN CERTIFICATE-----
      ...
      -----END CERTIFICATE-----
  # NTP servers
  ntp:
  - server1
  - server2
