.. _kiab-autoscale:

=====================
Autoscaling clusters
=====================

In this scenario we show how cluster size can automatically evolve as a reaction
to requirements from running workloads.

We will use baremetalpools to allocate computing resources to the cluster.
Cluster autoscaling will relie on two mechanims:

* cluster-api autoscaler that scales the cluster in response to events from
  the workload (typically pods that cannot be created due to missing resources),
* kanod autoscaler that scales the underlying resources to fit the needs of
  the cluster.

The full scenario can be run by defining the following parameters:

.. code-block:: yaml

    autoscale: true
    scaling_enabled: true
    bmc_protocol: redfish
    redfish_broker:
        broker_enabled: true
    mode_multi_tenant: true
    minions: 6

The ``autoscale`` parameter defines a cluster supporting autoscaling whereas
``scaling_enabled`` triggers a scale action. 

Defining the cluster
====================

There is no modification to the control-plane. Here we use a simple non
redundant control-plane associated to the only baremetal pool in use:

.. code-block:: yaml

    controlPlane:
      replicas: 1
      pool: baremetalpool-1
    ...

We define two machine deployments associated to the same pool. Both deployments
will have between 1 and 4 nodes. We associate an extra label to the
nodes with a different value for each machine deployment.

.. code-block:: yaml

    workers:
    - name: md1
      replicasBetween:
        min: 1
        max: 4
      kubeletExtraArgs:
        node-labels: example.com/dep-label=md1
      pool: baremetalpool-1
      ...
    - name: md2
      replicasBetween:
        min: 1
        max: 4
      kubeletExtraArgs:
        node-labels: example.com/dep-label=md2
      pool: baremetalpool-1
      ...

There is nothing really specific to the baremetalpool but we do not specify
its size:

.. code-block:: yaml

    bareMetalPools:
    - name: baremetalpool-1
      poolName: "pooldef1"
      brokerId: "default-broker"
      labelSelector:
        bmtype: vm-libvirt
      redfishSchema: redfish-virtualmedia
      blockingAnnotations:
      - kea.kanod.io/configured

Resource Limits
===============

Nothing is changed on the broker or in the cluster declaration (Clusterdef).
Note that the size of the cluster will also be limited by restrictions imposed
on the baremetalpool. Typically, the user associated to the cluster has a limit
on the number of servers he can book.

.. code-block:: yaml

    apiVersion: broker.kanod.io/v1
    kind: PoolUser
    metadata:
      name: pool-user-1
    namespace: brokerdef-system
    spec:
      credentialName: user1-secret
      maxServers: 6

Limits can also be defined at the level of pools by the user. They can also
count specific labels representing specific resources on servers.

Scaling the cluster
===================

When autoscaling is activated, we do not modify the definition of the cluster as in the
ref:`life-cycle scenario <kiab-life-cycle>` but we modify the workload to trigger
a size change.

We deploy and scale a deployment
on the target cluster that will require two worker nodes (using antiaffinity)
belonging to a specific machine deployment (using node selector)

.. code-block:: yaml

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: test
    spec:
      replicas: 1
      selector:
        matchLabels:
        app: test-app
      template:
        metadata:
          labels:
            app: test-app
        spec:
          affinity:
            podAntiAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
              - labelSelector:
                  matchExpressions:
                  - key: app
                    operator: In
                    values:
                    - test-app
                  topologyKey: "kubernetes.io/hostname"
          nodeSelector:
            example.com/dep-label: md1
          containers:
          - ...

In the scenario the machine deployment is manually scaled: 

.. code-block:: shell

    kubectl --kubeconfig scale --replicas=2 deployment/test

But in a more typical scenario, a standard Kubernetes autoscaler deployed
in the target cluster would probably scale deployments according to some metrics.

After the scaling completes, only the machine deployment ``md1`` is modified.

.. code-block:: text

    -------- md -------------------
    NAMESPACE   NAME              CLUSTER    REPLICAS   READY   UPDATED   UNAVAILABLE   PHASE     AGE   VERSION
    capm3       md-cluster1-md1   cluster1   2          2       2         0             Running   17m   v1.24.15
    capm3       md-cluster1-md2   cluster1   1          1       1         0             Running   17m   v1.24.15

Here is a screenshot of the virtual machine activity using libvirt VMM. 

.. figure:: images/scaling-activity.png

We see the activity of the control-plane ``vmok-3`` and the two worker nodes 
``vmok-1`` and ``vmok-6`` already allocated (end of the allocation at the beginining
of the recorded activity). A node is
added ``vmok-2`` to the worker nodes. As the node was already in ready state,
only provisioning is trigerred.

Note that a new spare machine ``vmok-4`` is launched (the plateau shape after the
spike is typical of introspection).

.. code-block:: text

    -------- bmh ------------------
    NAMESPACE   NAME                  STATUS   STATE         CONSUMER                               BMC                                                                                         HARDWARE_PROFILE   ONLINE   ERROR   AGE
    capm3       pooldef1-4jd59cbnf6   OK       provisioned   md-cluster1-md2-29ed2ae1-f7277         redfish-virtualmedia:pooldef1-4jd59cbnf6.broker.kanod.home.arpa/redfish/v1/Systems/vmok-1   unknown            true             17m
    capm3       pooldef1-cvmnwgpf9v   OK       provisioned   md-cluster1-md1-29ed2ae1-kwwlm         redfish-virtualmedia:pooldef1-cvmnwgpf9v.broker.kanod.home.arpa/redfish/v1/Systems/vmok-2   unknown            true             17m
    capm3       pooldef1-mgm96mh27f   OK       provisioned   md-cluster1-md1-29ed2ae1-lng4m         redfish-virtualmedia:pooldef1-mgm96mh27f.broker.kanod.home.arpa/redfish/v1/Systems/vmok-6   unknown            true             17m
    capm3       pooldef1-tsszdpxktz   OK       provisioned   controlplane-cluster1-29ed2ae1-f8887   redfish-virtualmedia:pooldef1-tsszdpxktz.broker.kanod.home.arpa/redfish/v1/Systems/vmok-3   unknown            true             17m
    capm3       pooldef1-tvjdthnm4f   OK       available                                            redfish-virtualmedia:pooldef1-tvjdthnm4f.broker.kanod.home.arpa/redfish/v1/Systems/vmok-4   unknown            false            5m3s




Behind the scene
================
There is one clusterapi autoscaler per cluster on the life-cycle manager.
It uses the kubeconfig secret provided by cluster-api to connect to the target
cluster and monitor the resource needs of the cluster.

There is a single kanod autocluster that controls all the baremetalpools.

Links between machine deployments and pools and constraints are translated
as labels and annotations on the machine deployments:

.. code-block:: yaml

    apiVersion: cluster.x-k8s.io/v1beta1
    kind: MachineDeployment
    metadata:
        annotations:
            cluster.x-k8s.io/cluster-api-autoscaler-node-group-max-size: "4"
            cluster.x-k8s.io/cluster-api-autoscaler-node-group-min-size: "1"
            machinedeployment.clusters.x-k8s.io/revision: "1"
        labels:
            app.kubernetes.io/instance: cdef-cluster1
            cluster.x-k8s.io/cluster-name: cluster1
            kanod.io/baremetalpool: baremetalpool-1
            nodepool: standard-nodepool
        name: md-cluster1-md2
        namespace: capm3
    spec:
        replicas: 1
        ...
