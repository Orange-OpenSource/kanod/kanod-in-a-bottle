.. _kiab-pivot:

=========================================
Using Cluster API Pivot on Kanod clusters
=========================================

Principle
=========
In Cluster-Api, pivoting is the action of transfering the resources controlling
a deployed target cluster from the initial management cluster to the target
cluster.

In standard deployments of cluster-api, after pivoting, the management cluster
is no more "necessary" and can be destroyed.

To pivot a cluster, the cluster-api controllers must first be deployed on the
target cluster. The treatment of cluster-api resources is then stopped on the
initial management cluster so that they can be safely copied to the target
cluster where they are restarted. Pausing and restarting is done through
the placement and destruction of an annotation on the resources. As the
initial management cluster is usually destroyed, all the resources and
controllers that it hosts are also destroyed.

Role of Pivot in Kanod
======================
In Kanod, we have chosen to have a single management cluster to control several
target clusters. We also use a gitops approach where clusters manifests are
synthesized from definitions stored in git repositories. Some of them belong
to the cluster administrator, others are under the control of the infrastructure
administrator.

For these reasons, in Kanod, we only pivot cluster-api and low-level baremetal
and network resources. We do not pivot neither ClusterDef resources defining 
the origin of clusters definitions, nor the Argocd applications creating the
cluster-api resources. But we retarget the Argocd application to deploy the
resources on the target cluster.

The goal of pivoting in Kanod is not to remove the need for a management cluster
but to limit its role to the definition of the managed cluster, not to the
day to day operations like fixing faulty machines by spawning new servers or
controlling the rolling updates of machine deployments. It means that the cluster
can keep a high level of availability even if the links with the life-cycle manager
are severed.

Regarding cluster-api, pivoting in Kanod is similar to standard pivot operation 
but we do not destroy the life-cycle manager at the end of the process, only
the cluster-api resources associated with the old application.

Pivot and pools
===============
One of the main issue with pivoting is knowing which baremetal hosts should be
pivoted and which should remain in the life-cycle manager for other clusters.

With BaremetalPools this problem is easily solved: we do not pivot individual
baremetal hosts but baremetalpools that represent a capability to a given
number of servers with a current set of servers effectively assigned.

Pivot process
=============

With Kanod, the pivot process is under the responsability of the clusterdef controller.
The infrastructure administrator must change the ``pivotInfo.pivot`` field of
``Clusterdef`` to start the pivot operations.

During the pivot process, the operations are achived:

- the controllers (CAPI, baremetal-operator and baremetalpool) are paused
  on the LCM cluster. The ironic deployement is scaled down.
- the management stack (operator and CRD for CAPI, baremetal-operator/ironic
  and baremetalpool) is installed and configured on the target cluster
- the argocd application associated with target cluster is deleted
  (the target cluster is not impacted)
- the CAPI/baremetalhost resources to be moved are labelled
  as required by cluster-api/clusterctl
- these resources are moved to the target cluster (clusterctl move function)
- the baremetalpool ressources are moved from the LCM to the target cluster
- all the controllers (previously paused) are then unpaused
  and the ironic deployement on the LCM is scaled up.
- the argocd application for the target cluster is created with
  the target cluster as destination server.

At the end of the pivoting process, only the clusterdef and argocd resources
remains on the LCM custer.



Pivoting in Kanod in a bottle
=============================

The operation is triggered by a new parameter ``pivot_enabled``. It is recommended
to also use a high availability target cluster with a high availability control-plane
(3 nodes with keepalived virtual endpoint) and redundant workers (2).
This can be speciified with the ``ha`` parameter:

.. code-block:: yaml

    pivot_enabled: true
    ha: true

Once the target cluster is deployed (at the end of the 13-wait.sh script),
we can launch the pivoting process with the script :

.. code-block:: bash

    30-pivot-clusters.sh

Here is an screencast that performs the same modification of a small existing cluster.

We can now observe the evolution of resources in the LCM cluster and
the target cluster. The PIVOTSTATUS field in the status of the clusterdef resource
in the LCM cluster shows the different steps during the process.

Note that the clusterdef and argocd resources remains on the LCM custer.

.. warning::
    Such a small cluster is not manageable in practice because you have to stop it
    to upgrade it. But the scenario shows the basic mechanisms in place with 
    the application being stopped before control is transfered to the target cluster. 

.. asciinema:: asciinema/broker_30-pivot.cast
    :rows: 45
    :idle-time-limit: 3
