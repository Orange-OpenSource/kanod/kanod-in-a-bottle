.. _kiab-host-multicluster:

====================================================
Multi Tenancy for Cluster Api with the Host Resource
====================================================

In this scenario, we show how Host help us deploy several clusters described
in different namespaces. The important keys in params.yml are the following:

.. code-block:: yaml

    mode_multi_tenant: true
    mode_multi_cluster: true
    hybrid:
        enabled: true

As we do not depend on the BMC protocol with Host, we can either choose IPMI
or Redfish, Bios or Uefi. The full deployment of two clusters is shown in
the following cast. We will detail the resources used below.

.. asciinema:: asciinema/hosts_40-hosts.cast
    :rows: 24
    :cols: 160
    :idle-time-limit: 1

Here is the description of one of the clusters. The only striking differences
wit previous definitions are:

* the flag ``hybrid`` at the begining of the definition,
* the selector that defines a ``host.kanod.io/kind`` label.

.. code-block:: yaml

    hybrid: true
    version: v1.24.15
    controlPlane:
      flavour: mono-dhcp
      replicas: 1
      kubernetes:
        network:
          domain: cluster1.local
      serverConfig:
        os: focal
        os_version: '1.13.26'
        admin:
          passwd: 'secret'
      hostSelector:
        matchLabels:
          host.kanod.io/kind: baremetal
    workers:
      - name: md1
        flavour: default-dhcp
        serverConfig:
          os: focal
          os_version: '1.13.26'
          admin:
            passwd: 'secret'
        replicas: 1
        hostSelector:
          matchLabels:
            host.kanod.io/kind: baremetal
    addons:
      - calico

The effect of the ``hybrid`` flag is only to define a label on the Cluster
object. This is this label that trigers the mode of the cluster api metal3
controller to use Hosts instead of BareMetalHosts as targets.

.. code-block:: yaml

    apiVersion: cluster.x-k8s.io/v1beta1
    kind: Cluster
    metadata:
      annotations:
        kanod.io/use-host: "true"
        ...
      name: cluster1
      namespace: capm3-cluster1
      ...

Each cluster has its own namespace where all the resources are defined.
Hosts are defined for each of the Metal3 machine in the same namespace.

.. code-block:: bash

    kanod:kanod-in-a-bottle>  kubectl get m3m -A
    NAMESPACE        NAME                                   AGE   PROVIDERID                                                                                          READY   CLUSTER    PHASE
    capm3-cluster1   controlplane-cluster1-5ab7327e-rxft9   19m   metal3://capm3-cluster1/controlplane-cluster1-5ab7327e-rxft9/controlplane-cluster1-5ab7327e-rxft9   true    cluster1   
    capm3-cluster1   md-cluster1-md1-5ab7327e-d9nvb         19m   metal3://capm3-cluster1/md-cluster1-md1-5ab7327e-d9nvb/md-cluster1-md1-5ab7327e-d9nvb               true    cluster1   
    capm3-cluster2   controlplane-cluster2-5ab7327e-zxk89   19m   metal3://capm3-cluster2/controlplane-cluster2-5ab7327e-zxk89/controlplane-cluster2-5ab7327e-zxk89   true    cluster2   
    capm3-cluster2   md-cluster2-md1-5ab7327e-xqksp         19m   metal3://capm3-cluster2/md-cluster2-md1-5ab7327e-xqksp/md-cluster2-md1-5ab7327e-xqksp               true    cluster2

    kanod:kanod-in-a-bottle> kubectl get host -A
    NAMESPACE        NAME                                   AGE
    capm3-cluster1   controlplane-cluster1-5ab7327e-rxft9   19m
    capm3-cluster1   md-cluster1-md1-5ab7327e-d9nvb         14m
    capm3-cluster2   controlplane-cluster2-5ab7327e-zxk89   19m
    capm3-cluster2   md-cluster2-md1-5ab7327e-xqksp         14m

If we look at the detail of a host, we notice that its definition is very
close to the definition of a Metal3 machine, but its status is closer to
a BareMetalHost.

.. code-block:: yaml

    apiVersion: kanod.io/v1alpha1
    kind: Host
    metadata:
      annotations:
        metal3.io/BareMetalHost: infrahost/vmok-2
      creationTimestamp: "2024-03-11T14:38:31Z"
      finalizers:
      - host.kanod.io
      generation: 3
      labels:
        cluster.x-k8s.io/cluster-name: cluster1
      name: md-cluster1-md1-5ab7327e-d9nvb
      namespace: capm3-cluster1
      ownerReferences:
      - apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
        controller: true
        kind: Metal3Machine
        name: md-cluster1-md1-5ab7327e-d9nvb
        uid: 6ec434a4-9445-4b7a-940b-16da4acb4281
      resourceVersion: "7341"
      uid: 2c955e0f-192a-47de-8bc5-6badcabcdba7
    spec:
      consumerRef:
        apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
        kind: Metal3Machine
        name: md-cluster1-md1-5ab7327e-d9nvb
        namespace: capm3-cluster1
      hostSelector:
        matchLabels:
          host.kanod.io/kind: baremetal
          kanod.io/mode: vm
      image:
        checksum: https://maven.service.kanod.home.arpa/repository/kanod/kanod/focal-v1.24.15/1.13.26/focal-v1.24.15-1.13.26.qcow2.md5
        checksumType: md5
        format: qcow2
        url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/focal-v1.24.15/1.13.26/focal-v1.24.15-1.13.26.qcow2
      metaData:
        name: md-cluster1-md1-5ab7327e-d9nvb-metadata
        namespace: capm3-cluster1
      online: true
      userData:
        name: md-cluster1-md1-9txjp
        namespace: capm3-cluster1
    status:
      addresses:
      - address: 192.168.133.34
        type: InternalIP
      - address: fe80::c0e6:4b0a:e29a:753f%ens3
        type: InternalIP
      - address: localhost.localdomain
        type: Hostname
      - address: localhost.localdomain
        type: InternalDNS
      bootMACAddress: "52:54:00:01:00:02"
      conditions:
      - lastTransitionTime: "2024-03-11T14:38:31Z"
        status: "True"
        type: AssociateBMH
      hostUID: 1fce7470-7ada-41f2-af5a-b0c5e254baea
      lastUpdated: "2024-03-11T14:38:31Z"
      nics:
      - MAC: "52:54:00:01:00:02"
        name: ens3
      - MAC: "52:54:00:01:00:02"
        name: ens3

Each Host is associated to a BareMetalHost, but those resources are defined
in another namespace. The end-user does not need to have any access to the
BareMetalHost because the relevant parts of its status are available in the Host
resource.

The bare-metal hosts are in another namespace:

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get bmh -A
    NAMESPACE   NAME     STATE         CONSUMER                               ONLINE   ERROR   AGE
    infrahost   vmok-1   provisioned   controlplane-cluster1-5ab7327e-rxft9   true             24m
    infrahost   vmok-2   provisioned   md-cluster1-md1-5ab7327e-d9nvb         true             24m
    infrahost   vmok-3   provisioned   controlplane-cluster2-5ab7327e-zxk89   true             24m
    infrahost   vmok-4   provisioned   md-cluster2-md1-5ab7327e-xqksp         true             24m

The main difference with the traditional case is the kind of the consumer which
is a host and not a Metal3 machine.
