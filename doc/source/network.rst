.. _kiab-network:

===================================
Configuring the Network with Kanod
===================================

In this scenario, we show how Kanod can configure the underlay network to
isolate clusters from one another in a multi-tenant scenario.

Networks are defined by a technology providing isolation, a set of identifiers
representing the network identity for this technology (VNI, VLAN id) and a
set of parameters for basic services over those networks (gateways, DNS).

Networks are under the responsibility of the infrastructure provider who open
them to tenants. Tenants will associate BaremetalPools to those networks
meaning that all machines belonging to an associated pool will be connected
to the network.

In the scenario, we will use VLAN as the underlying technology.

* There is a single switch linked to the bridge hosting the Life cycle manager (LCM),
  the service VM, the broker VM and the main DNS server (the broker and the service
  machine use symbolic names to distinguish different roles on their respective HTTP
  servers)
* A single baremetal pool will be used by the cluster and associated to the
  network.
* The PXE/provisionning VLAN will be performed on VLAN 100 associated to IP
  network 192.168.100.0/24 with the switch as gateway at address 192.168.100.1.
  A DHCP relay agent on the switch will transfer DHCP request to the life cycle
  manager. Trafic will be untagged on the servers so that it can be easily
  used for PXE boot.
* The Kubernetes default network will use VLAN 144. Again the switch will be
  used as gateway for the associated IP network 192.168.144.0/24.

The switch is implemented as a standard Linux VM hosting a CLI that mimics
a real Sonic switch (unfortunately the performance of an official Sonic VM
were not good enough for a realistic deployment).

..  figure:: images/switch-network.svg
    :figwidth: 90%
    :align: center

    Network configuration of the scenario

Launching the scenario
======================

The following parameters must be defined in ``params.yml``:

.. code-block:: yaml

    bmc_protocol: redfish
    redfish_broker:
        broker_enabled: true
    mode_multi_tenant: true
    switch: true

The scenario proceeds as the :ref:`multi-tenancy scenario <kiab-broker>`.
Note that the network 192.168.144.0/24 is not directly accessible from the
host. You can either set a route on the host:

.. code-block:: bash

    ip route add 192.168.144.0/24 via 192.168.133.2

or you can define a socks proxy on the gateway and use it for launching kubectl
commands:

.. code-block:: bash

    ssh -D 22300 -f -C -q -N admin@192.168.133.2
    export https_proxy=socks5://localhost:22300

The second approach has been chosen in the scripts as it is less disruptive
but the route approach is necessary if one want to ssh to the cluster without
a jump host.

Principle of Operation
======================

..  figure:: images/broker-resources.svg
    :figwidth: 90%
    :align: center

    Kubernetes Resources involved in the configuration of servers and networks

In the life-cycle manager and in the same namespace as the cluster definitition
(ClusterApi and Metal3 ClusterApi provider resources), computing resources
(servers known as BareMetalHost) are grouped in set of identical machines
(BareMetalPool resource). Each BareMetalPool can be attached to a set of L2
networks (Network resource).

BareMetalPool and Network are in fact proxies
for implementations of the real resources in a different context (either other
namespaces in the life-cycle manager or as in the demonstration in a different
Kubernetes cluster):

* BareMetalPool are backed by computing resource brokers that handle definition
  of actual servers (BareMetalDef resources). The counterpart of the
  BareMetalPool is a PoolDef resource.
* Network are backed by NetworkDefinition object and the links established
  between pools and network are maintained between PoolDef and
  NetworDefinition.

On the back-end side, the actual implementation of the network is represented
by Switch object representing top of rack switches. Links between real servers
and the switches are implemented by NetworkAttachment objects. When a network
is implemented on the link, a network definition binding is added the
NetworkAttachment.

The right to create servers in the server broker is represented by PoolUser
resources capturing both the credentials and the limits on the number and kind
of servers that can be created.

New Resources in the Life-Cycle Manager
=======================================
Infrastructure
--------------
The infrastructure manager is responsible for associating a cluster to
users and networks at the infrastructure level.
We define the pointers to both the machine broker and the network broker.

.. code-block:: yaml

    kind: ClusterDef
    metadata:
      name: cluster1
      namespace: capm3
    spec:
      # ...
      poolUserList:
        default-broker:
          username: "pool-user-1"
          address: https://broker.kanod.home.arpa/redfish-proxy
          brokerConfig: broker-ca
          brokerCredentials: broker-cred
      networkList:
        default-network:
          networkdefName: "networkdefinition-vlan-144"
          address: https://broker.kanod.home.arpa/netdef
          brokerConfig: brokernet-ca
          brokerCredentials: brokernet-cred
      multiTenant: true

The credential used are the admin credentials for accessing the brokers.

.. code-block:: yaml

    apiVersion: kustomize.config.k8s.io/v1beta1
    kind: Kustomization
    # ...
    secretGenerator:
      - name: broker-cred
        namespace: cluster-def
        literals:
          - username=root
          - password=secret
      - name: brokernet-cred
        namespace: cluster-def
        literals:
          - username=root
          - password=secret

Cluster Definition
------------------
In the cluster definition, the cluster administrator must define the 
baremetalpools and the networks.

Note the presence for the baremetal pool of a ``blockingAnnotation`` field.
This is a generic mechanism that will prevent the baremetal host from being
active until an annotation is present in its definition. 
The main purpose is to ensure that network is configured before registration and
introspection starts.

.. code-block:: yaml

    bareMetalPools:
    - name: baremetalpool-1
      poolName: "pooldef1"
      brokerId: "default-broker"
      labelSelector:
        bmtype: vm-libvirt
      redfishSchema: redfish
      replicas: 2
      blockingAnnotations:
        - kanod.io/net-pxe-interface

The network definitions contain the list of baremetal pools that are
connected. The connection will use an abstract name for example ``pxe``
for the provisioning network and ``k8sNet`` for the Kubernetes network.

This is visible in the templates used by the cluster. In ``meta/infra.yaml``
we find the following network configuration in netplan format:

.. code-block:: yaml

    controlPlaneFlavours:
      - flavour: mono
        # ...
        serverConfig:
          # ...
          network:
            version: 2
            ethernets:
              '{{ds.meta_data.net_k8sNet_interface}}':
                dhcp4: false
            vlans:
              '{{ds.meta_data.net_k8sNet_type}}{{ds.meta_data.net_k8sNet_vnid}}':
                dhcp4: false
                addresses: ['{{ds.meta_data.endpoint_host}}/{{ds.meta_data.cidr}}']
                gateway4: '{{ds.meta_data.gateway}}'
                nameservers:
                  addresses:
                    - 192.168.133.2
                id: '{{ds.meta_data.net_k8sNet_vnid}}'
                link: '{{ds.meta_data.net_k8sNet_interface}}'

The definition uses the standard jinja access to cloud-init metadata that
contains kanod exported labels from baremetal host. Note that hypens are
replaced by underscores: ``net_k8sNet_vnid`` coresponds to 
``kanod.io/net-k8sNet-vnid`` annotation.

.. code-block:: yaml

    networkPools:
    - name: network-vlan-144
      networkId: "default-network"
      bareMetalPoolList:
        - name: baremetalpool-1
          bindingName: k8sNet
    - name: network-pxe
      networkId: pxe-network
      bareMetalPoolList:
        - name: baremetalpool-1
          bindingName: pxe


New Resources in the Broker
===========================
All those resources are defined in a new ArgoCD application given to the 
broker. The location is given in the ``Kanod`` resource in the field 
``.spec.argocd.network``.

.. code-block:: yaml

    apiVersion: config.kanod.io/v1
    kind: Kanod
    metadata:
      name: kanod
      namespace: kanod
    spec:
      # ...
      argocd:
        baremetal:
          url: https://git.service.kanod.home.arpa/infra_admin/hosts.git
          credentials: git-admin
        network:
          url: https://git.service.kanod.home.arpa/infra_admin/network.git
          credentials: git-admin


Network Declaration
--------------------

The following network is the provisioning network used by the Ironic Python
Agent to supply inspection data to Ironic and to fetch OS image from the service
machine.

* The DHCP server is the address of the LCM (kea is in ironic pod). A
  DHCP relay on the switch will transfer requests.
* We provide a domain name server so that we can use the abstract name
  of the nexus repository service. 
* Note the ``dhcpMode`` value ``pxe``
  that will ensure that the DHCP server provides the path to
  the ironic python agent image.
* The annotation ``kanod.io/untagged`` ensures that the vlan is untagged 
  when reaching the servers.
* The annotation ``kanod.io/underlay-network`` can be used to disambiguate
  the attachment to use when there are several network cards on a baremetal
  host

.. code-block:: yaml

    ---
    apiVersion: brokernet.kanod.io/v1
    kind: NetworkDefinition
    metadata:
      name: networkdefinition-vlan-pxe
      annotations:
        kanod.io/underlay-network: net10G
        kanod.io/untagged: 'true'
    spec:
      type: vlan
      vnid: "100"
      subnetPrefix: 192.168.100.0/24
      gatewayIp: 192.168.100.1/24
      dhcpServerIp: 192.168.133.10
      credentialName: cred-vlan-pxe
      subnetStart: 192.168.100.128
      subnetEnd: 192.168.100.254
      domainNameServers:
        - 192.168.133.2
      dhcpMode: pxe

This is the definition of the default Kubernetes network. The Dhcp server
will supply addresses but will not implement PXE boot.

.. code-block:: yaml

    ---
    apiVersion: brokernet.kanod.io/v1
    kind: NetworkDefinition
    metadata:
      name: networkdefinition-vlan-144
      annotations:
        kanod.io/underlay-network: net10G
    spec:
      type: vlan
      vnid: "144"
      subnetPrefix: 192.168.144.0/24
      gatewayIp: 192.168.144.1/24
      dhcpServerIp: 192.168.100.10
      credentialName: cred-vlan-144

Topology Definition
-------------------

Each baremetal has an associated network attachment defining the link to the
switch. The switch port is the most important field:

.. code-block:: yaml

    ---
    apiVersion: brokernet.kanod.io/v1
    kind: NetworkAttachment
    metadata:
      name: na-1
      annotations:
        kanod.io/underlay-network: net10G
    spec:
      hostName: baremetaldef-1
      switchName: oksonic
      hostInterface: ens3
      switchPort: Ethernet2


The switch is defined in another resource. When there is a single switch the
most important settings are the credentials used to access the switch and the
management address. The interpretation of the credentials
could be different for different kind of switches

.. code-block:: yaml

    ---
    apiVersion: brokernet.kanod.io/v1
    kind: Switch
    metadata:
      name: oksonic
      labels:
        kanod.io/master-switch: "true"
    spec:
      name: oksonic
      ipAddress: 192.168.133.2
      credentials: oksonic-secret

Demonstration highlights
========================

Network Resources
-----------------
From the settings given in the cluster configuration, Kanod synthesized the
following resources for the network:

.. code-block:: yaml

  apiVersion: netpool.kanod.io/v1
  kind: Network
  metadata:
    name: network-vlan-144
    namespace: capm3-cluster1
  spec:
    bareMetalPoolList:
    - bindingName: k8sNet
      name: baremetalpool-1
    brokerConfig: broker-ca
    brokernetAddress: https://broker.kanod.home.arpa/netdef
    dhcpServerIp: 192.168.133.10
    networkDefinitionCredentialName: 2a71875159-broker-secret
    networkDefinitionName: networkdefinition-vlan-144

BareMetalHost Annotations
-------------------------
The following labels and annotations are visible on the hosts.

.. code-block:: yaml
    
    apiVersion: metal3.io/v1alpha1
    kind: BareMetalHost
    metadata:
      annotations:
        kanod.io/baremetaldef_version: "17461"
        kanod.io/net-k8sNet-interface: ens3
        kanod.io/net-k8sNet-type: vlan
        kanod.io/net-k8sNet-vnid: "144"
        kanod.io/net-pxe-interface: ens3
        kanod.io/net-pxe-type: vlan
        kanod.io/net-pxe-vnid: "100"
      labels:
        bmtype: vm-libvirt
        cluster.x-k8s.io/cluster-name: cluster1
        kanod.io/baremetalpool: baremetalpool-1
        kanod.io/interface: ens3
        kanod.io/mode: vm
        kanod.io/poolname: pooldef1

All the useful information on the configured interface are provided as
annotations. Because we used abstract binding names, we can easily reuse
machine configurations with different network bindings.
