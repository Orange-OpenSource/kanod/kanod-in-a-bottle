.. _kiab-ipam:

======================================
Using Cluster API without DHCP servers
======================================

Principle
=========
The goal of this scenario is to show that it is possible to deploy a Kubernetes
cluster without any DHCP server either within the target infrastructure or in
the life-cycle manager.

A DHCP server is essentially a database that maintains the allocation of IP
addresses over a network. It can also advertise basic services (gateway, DNS,
etc.) to customer machines over the network.

Motivations
-----------
Removing the need for DHCP servers simplifies the deployment of clusters.

The main issues with DHCP servers are:

* It is rather difficult to have several DHCP servers on the same network. This
  requires that DHCP servers only answer on disjoint set of mac addresses.
* The address allocated to a server is usually useful within the life-cycle
  manager (LCM). This means that there is usually two databases of allocated IP
  addresses (the LCM resources and the DHCP server) and
  maintaining their coherence is hard.
* The DHCP server database is rather opaque there is no standard protocol to
  synchronize its content with another entity (either the LCM or another
  DHCP server when we transfer the responsability over a server).
* Modifying on the fly the content of the DHCP server state (for example the
  set of networks handled or the set of mac addresses recognized) is not
  supported by a a lot of implementations and the process is completely
  implementation dependent.
* The address of each server is known only relatively late during the deployment
  process and is not available while manifests are synthesized.

Some of these constraints directly impact the use of a DHCP server within Kanod
(especially during pivot), others are operational constraints that may make
infrastructure administrators reluctant to deploy yet another DHCP server
in their infrastructure.

Challenges
----------
There are at least two kinds of IP addresses allocated during the deployment
of a cluster:

* Boot time IP addresses used by the Ironic Python Agent for server inspection
  and OS image provisioning
* Node addresses that make them accessible for the Kubernetes network.

The second kind is handled out of the box by IPAM mechanisms provided either
by Metal3 or Cluster-API. An IPAM allocates IP addresses (integers in a
well defined range) within the management cluster.
A pool of IP addresses is abstracted as a resource (IPPool) and consumes through
a mechanism of IP address claims (request) and IP address resources (answer).
The result is available during the definition of a cluster through the metadata
with the help of Metal3 metadata mechanism.

Unfortunately there was no simple mechanism to provide a static network
configuration to the Ironic Python Agent (IPA).

The official approach is
to build a custom IPA image that can be provisionned as a virtualmedia (The BMC
is seen by the main CPU of the target server as a CDROM). The preprovisioning
image must be built by a custom controller and made available through an HTTP
endpoint. This solution has the following issues :

* A custom controller must be developed for building those images.
* A lot of network trafic is necessary to build and transmit those iso images.
* The life-span of the resources is not very clear because we are not in the
  context of Ironic.
* Some of the context is missing from the specification of the boot image. For
  example it is not clear if it must be built for bios or uefi.

But this process can be done in the Ironic controller and there is
already an option to provide network data in a custom format for an agent
running in the Ironic Python Agent (glean) in charge of network configuration.

The next challenge is providing a network configuration for each host. The approach
is to mimick what the Metal3 MetaData operator does for cluster. We define a custom
resource containing a template for the network configuration and pointers to ipPools.
Each instance of the bareMetalHost 

Implementation
==============

Providing network data to IPA
-----------------------------
In this scenario we use the Ironic option for providing the network configuration.
The following modifications to a standard Metal3 deployment are the following:

* It requires a custom baremetal operator that propagates the network data from
  the bareMetalHost resource to Ironic.
* It requires a custom Ironic Python Agent Image. This image contains glean for
  the network configuration and uses a custom label for finding the disk coresponding
  to the virtual cdrom. We use the scripts developed by Nordix for their Metal3 CI
  to build this image.
* Ironic must be modified to generate an iso with the right custom label. This is
  done by modifying directly the Metal3 ironic-image.

.. warning::

    We need a specific label because the default one used by glean (config-2) conflicts
    with the one used by cloud-init to label its config drive. When a server has
    already been configured once, the virtual cdrom and the config drive from a
    previous installation would have the same label and there is no guarantee 
    that priority will be given to the virtual media.

Only the first point is specific to our approach. We would probably use a modified
glean even if we built a custom preprovisioning image.

We use the ``preprovisioningNetworkDataName`` field of the BareMetalHost to supply
the network configuration as a secret. It was already used by the PreprovisioningImage
resource approach.

Building host specific network data
-----------------------------------

We introduce the BmhData custom resource. Each BareMetalHost using the
resource must be in the same namespace and have an annotation
``network.kanod.io/bmh-data`` set to the name of the resource.

The resource contains a template field and a list of ipPools to use.
The ``template`` field contains the name of the secret holding the text of
the template under its ``template`` key.
For each entry in the ``ipAddressesFromIPPool`` list
we define a key (field ``key``), the name of the ipPool (field ``name``) and
the kind/apiGroup of the pool (fields ``kind`` and ``apiGroup``. Only
metal3 ipPools are supported in the current release).

The template is interpreted as a go template with a context defined from the
IP pools.

* ``{{ .boot_mac_address }}`` is the Mac address of the interface used for boot on the
  server (as documented in the BareMetalHost custom resource)
* ``{{ .ip_key }}`` gives access to the generated IP for pool identified by key
  ``key`` Launching the scenario
* ``{{ .gateway_key }}`` gives access to the gateway address
* ``{{ .dns_key }}`` gives access to the list of DNS servers and can be used with
  a ``range`` operator.

The template expansion generates a secret for each host. This secret is refered
in the ``preprovisioningNetworkDataName`` field of the BareMetalHost. An
annotation ``network.kanod.io/bmh-data-version`` keep tracks of the version
of the BmhData object used.

When the expansion completes, if the annotation ``baremetalhost.metal3.io/paused``
is defined and set to ``network.kanod.io/bmh-data``, then it is removed (the value MUST match).
It is important to pause the bareMetalHost when it is defined because the
inspection process cannot succeed without correct network data. Even the
registration process *needs* the network data as it checks that it is correct.

.. warning::

    A baremetalHost stuck in the ``registering`` state is often the sign of
    incorrect network data for the Ironic Python Agent.

Managing IPPools in the definition of clusters
----------------------------------------------
An ipPools section is added to the cluster definition. Each entry is identified
by a key and either a direct ipPool definition or a reference to a Network
resource.

In the first case, the entries ``start``, ``end``,``prefix``, ``gateway``, ``dns``, ``preAllocations``
are similar to what would be used for the definition of the resource. The values
are not interpreted by jinja but it is possible to use the ``{{ds.meta_data.key}}``
syntax to expand a key ``key`` defined as static cloud-init metadata (cidata).

In the later case, the ``network`` field contains the name of the network.

Running the scenario
====================
The use of ipam is controlled by the ``ipam`` field in ``params.yml``.

.. warning::

    Changing this value to true requires a recompilation of packages
    on service machine. At least packages related to kanod-stack and
    ironic-python-agent must be recompiled.

Using IPAM can be done without or with the broker, and in the broker case
without or with the switch flag enabled (management of top of rack switch
by Kanod).

Static definition of baremetal hosts
------------------------------------
The main change is the annotation of bareMetalHost with
the ``network.kanod.io/bmh-data`` to point to an BmhData resource
defined in the ``bmh/bmh-data.yaml``. The coresponding IPPool and template
secret are defined in the same file. Here is the content of the template.
It is a JSON data structure following a
`data model <https://specs.openstack.org/openstack/nova-specs/specs/liberty/implemented/metadata-service-network-info.html>`_ 
proposed for OpenStack neutron.

.. code-block:: json

    {
      "links": [
        {"id": "ens3", "type": "phy", "ethernet_mac_address": "{{ .boot_mac_address }}"}
      ],
      "networks": [{
        "id": "network0",
        "type": "ipv4",
        "link": "ens3",
        "ip_address": "{{ .ip_pxe }}",
        "netmask": "255.255.255.0",
        "network_id": "network0",

        "routes": [{
          "network": "0.0.0.0",
          "netmask": "0.0.0.0",
          "gateway": "{{ .gateway_pxe }}"
        }]
      }],
      "services": [
        {"type": "dns", "address": "192.168.133.1"}
      ]
    }

The other change is the definition of the Metal3 paused annotation to
``network.kanod.io/bmh-data`` as explained above.

For the cluster, we need an IPPool. The fields use the data defined in the
cluster cidata by the infrastructure operator.

.. code-block:: yaml

    ipPools:
      - name: k8s-pool
        start: '{{ds.meta_data.ippool_start}}'
        end: '{{ds.meta_data.ippool_end}}'
        gateway: '{{ds.meta_data.gateway}}'
        prefix: '{{ds.meta_data.prefix}}'
        dns:
          - '192.168.133.1'
        preAllocations:
          endpoint: '{{ds.meta_data.cluster_endpoint}}'
    ipPoolsBindings:
      k8s: k8s-pool

Note that we distinguish resource names and keys (in ``ipPoolsBindings``).
Keys are used in metadata and are more constrained. IpPools can be used at
different places to generate different ips using different keys.

There are specific entries in the flavours for the IPAM case as the definition
is slightly different. For example the network configuration of a worker is
the following:

.. code-block:: yaml

  - flavour: default-ipam
    inherit: default
    serverConfig:
      network:
        version: 2
        ethernets:
          ens3:
            dhcp4: false
            addresses:
              - '{{ds.meta_data.ip_k8s}}/{{ds.meta_data.prefix_k8s}}'
            gateway4: '{{ds.meta_data.gateway_k8s}}'
            nameservers:
              addresses:
                - 192.168.133.1

Note the use of the entries ``{{ds_meta_data.<kind>_<pool>}}`` where ``<kind>``
is either ``ip``, ``gateway`` or ``dns`` and identifies the information extracted
and ``<pool>`` is a key defined in ``ipPoolsBindings`` and linked to an IPPool 
defined in the ``ipPools`` section.

Using a pool of hosts on a preconfigured network
------------------------------------------------
In this case, both the provisioning network and the regular cluster network
are defined as IPPools.

.. code-block:: yaml

    ipPools:
      - name: k8s-pool
        start: '{{ds.meta_data.ippool_start}}'
        end: '{{ds.meta_data.ippool_end}}'
        gateway: '{{ds.meta_data.gateway}}'
        prefix: '{{ds.meta_data.prefix}}'
        dns:
          - '192.168.133.1'
        preAllocations:
          endpoint: '{{ds.meta_data.cluster_endpoint}}'
      - name: pxe-pool
        start: '192.168.133.30'
        end: '192.168.133.79'
        prefix: 24
        gateway: '192.168.133.1'
        dns:
          - '192.168.133.1'
    ipPoolsBindings:
        k8s: k8s-pool

Internally, it is the responsability of the BareMetalPool operator to remove
the Metal3 paused annotation. It uses the ``network.kanod.io/bmh-data-version``
annotation as a barrier.

The baremetalpool definition will contain a description of the BmhData used
for the provisioning. The section ipPools defines the bindings between
keys and ipPool names whereas the bootNetwork field is the template used
to define the preprovisioning network configuration.

.. code-block:: yaml

    bareMetalPools:
      - name: baremetalpool-1
        ...
        bmhData:
          ipPools:
            pxe: pxe-pool
          bootNetwork: |
            {
              "links": [
                {"id": "ens3", "type": "phy", "ethernet_mac_address": "{{ .boot_mac_address }}"}
              ],
              "networks": [{
                "id": "network0",
                "type": "ipv4",
                "link": "ens3",
                "ip_address": "{{.ip_pxe}}",
                "netmask": "255.255.255.0",
                "network_id": "network0",

                "routes": [{
                  "network": "0.0.0.0",
                  "netmask": "0.0.0.0",
                  "gateway": "{{.gateway_pxe}}"
                }]
              }],
              "services":[
                {{range $index, $addr := .dns_pxe}}
                {{if $index}},{{end}}
                {"type": "dns", "address": "{{$addr}}"}
                {{end}}
              ]
            }

Using a pool of hosts with networks configured by Kanod
-------------------------------------------------------
In the later case, the Network resources are providing the necessary information
for the IPPools. Rather than manually defining the pools, we point to the network
resources.

.. code-block:: yaml

    ipPools:
      - key: k8s-pool
        network: network-vlan-144
        preAllocations:
          endpoint: '{{ds.meta_data.cluster_endpoint}}'
      - key: pxe-pool
        boot: true
        network: network-pxe
