.. _kiab-broker:

===============================================
Deploying kanod in a bottle with Redfish Broker
===============================================

Goals of the Redfish Broker
===========================

When deploying a target cluster on servers (baremetal or VM),
the LCM interacts with these servers via the baremetalhost custom resources.
These baremetalhost CR contains sensible information (like BMC address or BMC credentials)
which may lead to security problem in a multi cluster context especially if we want
to keep the ability to allocate a server between different clusters depending on the
load of those clusters.

This is clearly visible in the deployments because clusters and baremetalhosts must all share
a single namespace.

The redfish broker is a solution to solve this problem by abstracting the management of the baremetalhost :

- the ``broker`` cluster stores the characteristics of the baremetal servers available and the declaration of the pools
  for the baremetalhost creation/deletion. The broker exposes an API allowing to allocate a baremetal to a pool.
  A redfish proxy server is also launched for managing the redfish requests between the lcm and the real baremetal.
- in the ``lcm`` cluster, baremetalhost are created/deleted by a kubernetes operator (baremetalpool)
  via a custom resource. This operator interacts with the broker launched in the broker cluster.
  In the baremetalhost CR created by this operator, the real credentials and address are replaced
  by custom values associated with the pool. These values will translated to the real ones by the redfish proxy.

The broker and the LCM may be deployed in the same cluster or not. But now ClusterAPI descriptions of target clusters
are in an isolated namespace with the associated baremetalpool and the baremetalhosts spawned from the pool. If we pivot
the cluster definition to the target cluster, we can safely copy the contents of this namespace.

When we decrease the size of a cluster, some of the baremetalhost become unallocated. The baremetal pool size can be
reduced too and the idle baremetalhost will be destroyed in the namespace. Now the underlying resource can be allocated
by the broker to another cluster in another namespace or in another life-cycle manager.

This mechanism imporves the security for accessing the server BMC and allows to securally reuse a server
for another target cluster.


Deployment
==========

The script ``100-full.sh`` launches the deployment process of a target cluster.

In order to enable the use of the Redfish Broker, the initial configuration must be updated
with these variables declaration before launching the script ``100-full.sh``:

.. code-block:: yaml

    redfish_broker:
      broker_enabled: true
    bmc_protocol: "redfish"
    mode_multi_tenant: true

The main difference appears with the eighth step (08-launch-brokerdef.sh) that launches a new cluster. The service exposed
by this cluster are under the domain name ``broker.kanod.home.arpa``

.. code-block:: bash

    08-launch-brokerdef.sh

.. asciinema:: asciinema/broker_08-broker.cast
    :rows: 30
    :idle-time-limit: 1.5

Once the broker cluster is launched, the others steps are executed and we can oberve the
evolution of the resources in the LCM cluster (script 13-wait.sh) :

.. asciinema:: asciinema/broker_13-wait.cast
    :rows: 45
    :cols: 160
    :idle-time-limit: 1



Resources in the Life-Cycle-Manager
===================================

If we look at the applications deployed on the life-cycle manager, the application
managing the baremetal server definitions has disappeared:

.. code-block:: bash

    kanod:kanod-in-a-bottle> kubectl get application -A
    NAMESPACE   NAME             SYNC STATUS   HEALTH STATUS
    argocd      cdef-cluster1    Synced        Healthy
    argocd      kanod-projects   Synced        Healthy

If we look at the baremetal hosts. Only two have been provisionned. They are in a new
namespace specific to this cluster deployment. It is derived from the ``Clusterdef``
resource (This is the role of the multi-tenant parameter defined during the launch.
It triggers the addition of a multi-tenant field to the clusterdef resource)

Server names are machine generated and the BMC is now hosted on the broker machine.

.. code-block:: bash

    -------- bmh ------------------
    NAMESPACE        NAME                  STATUS   STATE         CONSUMER                               BMC                                                                            HARDWARE_PROFILE   ONLINE   ERROR   AGE
    capm3-cluster1   pooldef1-448q9xjmnd   OK       provisioned   md-cluster1-md1-3a996ad5-822s9         redfish:pooldef1-448q9xjmnd.broker.kanod.home.arpa/redfish/v1/Systems/vmok-3   unknown            true             33h
    capm3-cluster1   pooldef1-bw8gtj9zzv   OK       provisioned   controlplane-cluster1-3a996ad5-xqd4l   redfish:pooldef1-bw8gtj9zzv.broker.kanod.home.arpa/redfish/v1/Systems/vmok-1   unknown            true             33h

It is those machines that are used by the cluster. They have been defined from one resource: ``Baremetalpool``

.. code-block:: yaml

    apiVersion: bmp.kanod.io/v1
    kind: Baremetalpool
    metadata:
      name: baremetalpool-1
      namespace: capm3-cluster1
    spec:
      address: https://broker.kanod.home.arpa/redfish-proxy
      brokerConfig: broker-ca
      credentialName: df6766c7ac-broker-secret
      maxServers: -1
      poolname: pooldef1
      redfishschema: redfish
      size: 2
    status:
      bmhcount: 2
      bmhnameslist:
      - pooldef1-448q9xjmnd
      - pooldef1-bw8gtj9zzv
      status: Pool allocated

Note that the baremetalhost uses some credentials. They are created by the ClusterDef resource which is under the
control of the infrastructure administrator. He is ultimately responsible of the specification and the number of
servers that can be allocated to a deployment. Note that the use of ClusterDef is not required as those credentials
could be manually provided by the infrastructure administrator (or through another automatic process).

.. code-block:: yaml

    apiVersion: gitops.kanod.io/v1alpha1
    kind: ClusterDef
    metadata:
      name: cluster1
      namespace: capm3
    spec:
      brokerUserList:
        default-broker:
          address: https://broker.kanod.home.arpa/redfish-proxy
          brokerConfig: broker-ca
          brokerCredentials: broker-cred
          username: pool-user-1
      brokernetUserList:
        default-network:
          address: https://broker.kanod.home.arpa/netdef
          brokerConfig: brokernet-ca
          brokerCredentials: brokernet-cred
          networkdefName: networkdefinition-sample
      cidataName: cluster1-cidata
      configurationName: infra
      credentialName: project-creds
      multiTenant: true
      pivotInfo:
        kanodName: ""
        kanodNamespace: ""
        pivot: false
      source:
        branch: master
        repository: https://git.service.kanod.home.arpa/cluster_admin/config.git


Resources in the Broker cluster
===============================

On the broker we have a few services:

* `argocd` is deployed with an ingress and a metallb load balancer to access the service.
* `brokerdef` and `brokernet` are the core of the solution. They are respectively in charge of providing servers to pools and maintaining
  a correct network environment for those servers.

Among the application deployed, ``baremetal-hosts`` is responsible of the definition of hosts. 

.. code-block:: bash

    NAMESPACE   NAME                 SYNC STATUS   HEALTH STATUS
    argocd      baremetal-hosts      Synced        Healthy
    argocd      network-definition   OutOfSync     Healthy

It contains BaremetalDef resources that are very similar to BaremetalHosts but cannot be instantiated but It
contains the real credentials of the servers.

.. code-block:: yaml

    apiVersion: broker.kanod.io/v1
    kind: Baremetaldef
    metadata:
      name: baremetaldef-3
      namespace: brokerdef-system
    spec:
      baremetalcredential: bm-credentials-3-ckm6k2dkf7
      disablecertificateverification: true
      id: baremetaldef-3
      k8slabels:
        bmtype: vm-libvirt
        kanod.io/interface: ens3
        kanod.io/mode: vm
      macAddress: "52:54:00:01:00:03"
      rootDeviceHints:
        deviceName: /dev/vda
      url: https://192.168.133.1:5003/redfish/v1/Systems/vmok-3
    status:
      state: Created

The baremetal definition also contains 

.. code-block:: yaml

    apiVersion: broker.kanod.io/v1
    kind: Pooldef
    metadata:
      name: pooldef1
      namespace: brokerdef-system
    spec:
      maxServers: -1
      username: pool-user-1
    status:
      state: Created

BaremetalPools have a counterpart on the broker cluster: Pooldef. The pooldef
associates baremetaldef to baremetalhosts. When a server is selected, the
associate ``BaremetalDef`` is marked as in use with an annotation specifying
the ``PoolDef``

The credentials are associated to a notion of user:

.. code-block:: yaml

    apiVersion: broker.kanod.io/v1
    kind: PoolUser
    metadata:
      name: pool-user-1
      namespace: brokerdef-system
    spec:
      credentialName: user1-secret-m4d885dchh
      maxServers: 3

Users are also used to set global limits on the numbers and characteristics of
servers allocated to a given tenant without constraining the use in a given
project.

.. warning::

    We do not describe the behaviour of brokernet yet. 
    They will be given in a new scenario involving the Top-of-Rack switch.
