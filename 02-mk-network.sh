#!/bin/bash
#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

OKNET_ITF=${OKNET_ITF:-virbr1}

if [[ $(virsh nwfilter-list  | tail +3 | awk '{print $2}') =~ (^|[[:space:]])kanod-airgap-mode($|[[:space:]]) ]]; then
  echo 'Network filter already exists'
else
  virsh nwfilter-define /dev/stdin <<EOF
<filter name='kanod-airgap-mode' chain='ipv4' priority='-700'>
  <rule action='drop' direction='out' priority='400'>
    <ip match='no' direction='out' dstipaddr='192.168.133.0' dstipmask='255.255.255.0'/>
  </rule>
</filter>
EOF
fi

if [[ $(virsh net-list | tail +3 | awk '{print $1}') =~ (^|[[:space:]])oknet($|[[:space:]]) ]]; then
  echo 'Network already exists'
else
  virsh net-define /dev/stdin <<EOF
<network xmlns:dnsmasq='http://libvirt.org/schemas/network/dnsmasq/1.0'>
  <name>oknet</name>
  <domain name="kanod.home.arpa" localOnly="yes" />
  <dns>
    <host ip='192.168.133.1'>
      <hostname>gateway</hostname>
      <hostname>dns</hostname>
    </host>
    <host ip='192.168.133.10'>
      <hostname>lcm-vm</hostname>
    </host>
    <host ip='192.168.133.11'>
      <hostname>lcm</hostname>
    </host>
    <host ip='192.168.133.12'>
      <hostname>service</hostname>
    </host>
    <host ip='192.168.133.13'>
      <hostname>broker-vm</hostname>
    </host>
    <host ip='192.168.133.14'>
      <hostname>broker</hostname>
    </host>
    <host ip='192.168.133.18'>
      <hostname>openstack</hostname>
    </host>
  </dns>
  <dnsmasq:options>
    <dnsmasq:option value="local=/broker.kanod.home.arpa/"/>
    <dnsmasq:option value="local=/service.kanod.home.arpa/"/>
    <dnsmasq:option value="address=/service.kanod.home.arpa/192.168.133.12"/>
    <dnsmasq:option value="address=/broker.kanod.home.arpa/192.168.133.14"/>
  </dnsmasq:options>
  <forward mode='nat'/>
  <bridge name='${OKNET_ITF}' stp='on' delay='0'/>
  <ip address='192.168.133.1' netmask='255.255.255.0'>
  </ip>
</network>
EOF
  virsh net-start oknet
  virsh net-autostart oknet
  # try to avoid races with the resolvectl commands leading to weird behaviours.
  sleep 30
fi

sudo /usr/bin/resolvectl dns "${OKNET_ITF}" 192.168.133.1 
sudo /usr/bin/resolvectl domain "${OKNET_ITF}" '~kanod.home.arpa'
