#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

# shellcheck disable=SC1090,SC1091
source "${ROOT_DIR}/lib_sync.sh"

kanod-vm -c "${LAB_DIR}/lcm" -v

sync_status lcm 192.168.133.10
if [ "${status:-}" == "0" ]; then
    kc=${LAB_DIR}/kubeconfig/lcm.kubeconfig
    scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null  "admin@192.168.133.10:kubeconfig" "$kc"
fi

exit "${status:-0}"
