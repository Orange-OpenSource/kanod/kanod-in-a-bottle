#!/bin/bash

#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

synchronized=0

while getopts "hs?" opt; do
    case "$opt" in
    h|\?)
        echo "$0 [-sh]"
        echo '  -h this help'
        echo '  -s synchronized mode.'
        exit 0
        ;;
    s)
        synchronized=1
    esac
done

sync_status() {
    status=0
    if [ "$synchronized" == 1 ]; then
        echo "waiting for system boot up"
        sleep 60
        echo -n 'sync.'
        while true; do
            # shellcheck disable=SC2034
            if status=$(ssh -i "${LAB_DIR}/$1/key" \
                -o "BatchMode=yes" \
                -o "PasswordAuthentication=no" \
                -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
                "admin@$2" cat /etc/kanod-configure/status 2> /dev/null)
            then
                break
            fi
            sleep 10
            echo -n '.'
        done
        mkdir -p "${LAB_DIR}/logs"
        ssh -i "${LAB_DIR}/$1/key" \
            -o "BatchMode=yes" \
            -o "PasswordAuthentication=no" \
            -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
            "admin@$2" sudo cat /var/log/cloud-init-output.log > "${LAB_DIR}/logs/$1.log"
        echo
    fi
}
