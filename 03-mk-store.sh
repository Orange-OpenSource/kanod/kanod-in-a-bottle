#!/bin/bash
set -eu
#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

mkdir -p "${LAB_DIR}/${STORAGE_POOL}"

# shellcheck disable=SC2076
if ! [[ "$(virsh pool-list --name)" =~ "${STORAGE_POOL}" ]]; then
    virsh pool-define-as --type dir --name "${STORAGE_POOL}" --target "${LAB_DIR}/${STORAGE_POOL}"
    virsh pool-start "${STORAGE_POOL}"
    virsh pool-autostart "${STORAGE_POOL}"
fi

for vol in $(virsh vol-list "${STORAGE_POOL}" | tail +3 | awk '{print $1}'); do
    virsh vol-delete --pool "${STORAGE_POOL}" --vol "${vol}";
done

vol_create() {
    VOL="$1"
    SIZE="$2"
    if ! [ -f "${LAB_DIR}/${STORAGE_POOL}/${VOL}" ]; then
        virsh vol-create-as "${STORAGE_POOL}" "${VOL}" "${SIZE}" --format raw
        sudo virt-format -a "${LAB_DIR}/${STORAGE_POOL}/${VOL}" --filesystem=ext4
    fi
}

virsh vol-create-as "${STORAGE_POOL}" service "${SERVICE_SIZE:-30G}" --format raw
