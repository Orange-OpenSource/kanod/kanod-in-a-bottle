#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

# shellcheck disable=SC1090,SC1091
source "${ROOT_DIR}/lib_sync.sh"

if [ "$OPENSTACK" = 1 ]; then
    kanod-vm -c "${LAB_DIR}/openstack" -i "${LAB_DIR}/build/kanod-node/img.qcow2"
    sync_status openstack 192.168.133.18
fi

if [ "$KANOD_SWITCH" = 1 ]; then
    kanod-vm -c "${LAB_DIR}/switch" -i "${LAB_DIR}/build/kanod-switch/img.qcow2"
fi

echo "Launch data-center servers"
for vm in "${LAB_DIR}/vmok-"* ; do
    echo "*** ${vm} ****"
    kanod-vm -c "${vm}"
done

if [ "$KANOD_SWITCH" = 1 ]; then
    sync_status switch 192.168.133.2
    echo "Using a switch: setting a socks5 proxy to reach cluster without an explicit route"
    ssh -o "PasswordAuthentication=no" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -D 22300 -f -C -q -N admin@192.168.133.2
fi
