#!/bin/bash

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

CLUSTER_NAME="${CLUSTER_NAME:-cluster1}"

if [ "$KANOD_SWITCH" = 1 ]; then
    export http_proxy=socks5://localhost:22300
    export https_proxy=socks5://localhost:22300
    export HTTP_PROXY=$http_proxy
    export HTTPS_PROXY=$https_proxy
fi

kc2=${LAB_DIR}/kubeconfig/${CLUSTER_NAME}.kubeconfig

echo "Start at : $(date)"


yq -i  e '.version = env(K8S_UPGRADED_VERSION)' "${LAB_DIR}/cluster1/config.yaml"

nbCp=$(yq e '.controlPlane.replicas' "${LAB_DIR}/cluster1/config.yaml")
nbWorkers=$(yq e '.workers.[].replicas' "${LAB_DIR}/cluster1/config.yaml")
nbNodes=0
for nb in $nbCp $nbWorkers; do
    (( nbNodes=nbNodes+nb ))
done

if [ "${REDFISH_BROKER}" == 1 ] && [ "${AUTOSCALE:-0}" = 0 ]; then
    ((bmpool_size=nbNodes+1))
    BMPOOL_SIZE=${bmpool_size} yq -i e '.bareMetalPools.[].replicas = env(BMPOOL_SIZE)' "${LAB_DIR}/cluster1/config.yaml"
fi

pushd "${LAB_DIR}/cluster1" || { echo "error during cd ${LAB_DIR}/cluster1" ; exit; }
git commit -m "update" config.yaml
git push origin main
popd || exit

echo "Waiting for $nbNodes nodes ready after upgrade"
mkdir -p "${LAB_DIR}/logs"
count=0
while [ "$(kubectl --kubeconfig "${kc2}" --no-headers=true get nodes 2> /dev/null | wc -l)" != "${nbNodes}" ] || \
    [ "$(kubectl --kubeconfig "${kc2}" --no-headers=true get nodes 2> /dev/null | grep "${K8S_UPGRADED_VERSION}" | grep -v NotReady | grep -c Ready)" != "${nbNodes}" ]; do
    count=$((count+1))
    if [ "$count" -gt 1000 ]; then
        ./view-deployment.sh -l -t -n
        echo; echo 'Timout waiting for nodes'
        exit 1
    fi
    printf "\n\n------- %s --------\n" "$(date)" &>> "${LAB_DIR}/logs/deployment_upgrade.log"
    ./view-deployment.sh -l -t -n &>> "${LAB_DIR}/logs/deployment_upgrade.log"
    ironicNode=$(kubectl --kubeconfig "${kc2}" get pod  -n baremetal-operator-system -l name=ironic -o yaml | yq '.items.[].spec.nodeName')
    bmoNode=$(kubectl --kubeconfig "${kc2}" get pod  -n baremetal-operator-system -l control-plane=controller-manager -o yaml | yq '.items.[].spec.nodeName')
    echo "Pod ironic on node : ${ironicNode}" &>> "${LAB_DIR}/logs/deployment_upgrade.log"
    echo "BMO on node : ${bmoNode}" &>> "${LAB_DIR}/logs/deployment_upgrade.log"
    sleep 10
    echo -n '.'
done
echo
echo "End at : $(date)"
