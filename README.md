# KANOD IN A BOTTLE

*Kanod in a bottle* let you test the Kanod solution on a single machine.
*Kanod in a bottle* deploys Kanod on a set of virtual machines (at least two: one for the
life-cycle manager and another for the artefact repository).

The documentation is in the doc folder and is available on line at
https://orange-opensource.gitlab.io/kanod/reference/kiab/index.html
