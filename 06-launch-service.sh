#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck source=/dev/null
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

# shellcheck source=lib_sync.sh
source "${ROOT_DIR}/lib_sync.sh"
wfsync=$synchronized
synchronized=1

function get_service_logs() {
    mkdir -p "${LAB_DIR}/service_logs/pods"
    mkdir -p "${LAB_DIR}/service_logs/containers"
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.12 sudo tar -czhf - -C /var/log/containers . | tar -C "${LAB_DIR}/service_logs/containers" -xzf - .
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.12 sudo tar -czhf - -C /var/log/pods . | tar -C "${LAB_DIR}/service_logs/pods" -xzf - .
}

# Always synchronized because we deploy manifests on it
kanod-vm -c "${LAB_DIR}/service" -i "${LAB_DIR}/build/kanod-node/img.qcow2" -v -V "${LAB_DIR}/build/kanod-node/schema.yaml"
sync_status service 192.168.133.12
if [ "${status:-0}" != 0 ]; then
    echo 'Failed to launch Kubernetes on service machine'
    exit 1
fi

kc=${LAB_DIR}/kubeconfig/service.kubeconfig
scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null  "admin@192.168.133.12:kubeconfig" "$kc"

kubectl apply --kubeconfig "${kc}" -k "${LAB_DIR}/service/manifests/infra"
# There is a race between CRD and CR for argo workflows and another on cert manager. After the wait everything should be settled.
echo -n 'wait for cert-manager'
until [[ $(kubectl --kubeconfig "${kc}" get endpoints/cert-manager -n cert-manager -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]; do
    sleep 5; echo -n '.';
done
until [[ $(kubectl --kubeconfig "${kc}" get endpoints/cert-manager-webhook -n cert-manager -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]; do
    sleep 5; echo -n '.';
done
echo

kubectl apply  --kubeconfig "${kc}" -k "${LAB_DIR}/service/manifests/vault"
# Now we wait for vault readiness and the injector. If the injector is not ready pods are launched WITHOUT the intended modification.
echo -n 'wait for endpoints/vault'
until [[ $(kubectl --kubeconfig "${kc}" get endpoints/vault -n vault -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]; do
    sleep 5; echo -n '.';
done
echo -n 'wait for endpoints/vault-agent-injector-svc'
until [[ $(kubectl --kubeconfig "${kc}" get endpoints/vault-agent-injector-svc -n vault -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]; do
    sleep 5; echo -n '.';
done
echo

kubectl apply --kubeconfig "${kc}" -k "${LAB_DIR}/service/manifests"
sleep 5
kubectl apply --kubeconfig "${kc}" -f "${LAB_DIR}/service/manifests/workflow/workflow.yaml"

check_workflow_status() {
    status="$(kubectl --kubeconfig "${kc}" get workflow nexus-build -o 'jsonpath={.status.phase}'  || echo 'Running')"
    if [ "$status" == "" ]; then
        status='Running'
    fi
    echo "$status"
}

if [ "$wfsync" = "1" ]; then
    echo -n 'wait for workflow'
    until [ "$(check_workflow_status)" != 'Running' ]; do
        sleep 5; echo -n '.';
    done
    until [ "$(kubectl --kubeconfig "${kc}" get workflow nexus-build -o 'jsonpath={.status.finishedAt}'  || echo '<nil>')" != '<nil>' ]; do
        sleep 5; echo -n '.';
    done
    echo
    get_service_logs

    (kubectl --kubeconfig "${kc}" get workflow nexus-build -o yaml | yq '(.status.nodes | to_entries | map({"name": .value.name, "status": .value.phase}) | group_by(.status) | map({"status": .[0].status, "names": . | map(.name) }))' > "${LAB_DIR}/logs/workflow.log") || true
    (KUBECONFIG="$kc" argo get nexus-build -o yaml | yq '.status.nodes | map_values({"name": .displayName, "children": .children, "duration": (.finishedAt | to_unix) - (.startedAt | to_unix)})' > "${LAB_DIR}/logs/workflow_durations.yaml") || true
    status=$(kubectl --kubeconfig "${kc}" get workflow nexus-build -o 'jsonpath={.status.phase}')
    if [ "$status" = "Failed" ]; then
        echo 'Nexus build failed'
        exit 1
    fi
fi

# Configure memory allocation on service once the nexus build is done

echo 'adjust memory allocation on service machine'
virsh setmaxmem service 16G --config
virsh setmem service 16G --config

echo 'shutdown service machine'
virsh shutdown service

echo "waiting for service machine shutdown"
while [ "$(virsh list --state-shutoff  --name | grep '^service$')" != "service" ]; do
    echo -n "."
    sleep 10
done

echo
echo 'restart service machine'
virsh start service

echo "waiting for service machine available"
while ! curl -s -f -o /dev/null "http://maven.service.kanod.home.arpa/service/rest/v1/read-only"; do
    echo -n '.'
    sleep 5
done

while ! curl -s -f -o /dev/null "http://git.service.kanod.home.arpa/"; do
    echo -n '.'
    sleep 5
done
echo
