#!/bin/bash

set -eu

export PATH="${HOME}/.local/bin:${PATH}"
export no_proxy=172.20.18.34,192.168.133.0/24,.kanod.home.arpa
export NO_PROXY=${no_proxy}

BASE_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")

OPERATOR=1

export TEMPLATE=${TEMPLATE:-param.ci}
export SCENARIOS_PARAM=${SCENARIOS_PARAM:-scenarios.yaml}
export SKETCH=${SKETCH:-regular}

# update pip packages
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade setuptools


# check if sketch exists in scenario file
val=$(yq '. | has(env(SKETCH))' scenarios.yaml)
if [ "${val}" == "false" ] ; then
    echo "sketch ${SKETCH} missing in scenarios.yaml file "
    exit 1
fi

if [ "${SKETCH}" != "" ]; then
  for entry in $(yq -o j ".${SKETCH} // {} | to_entries" "${SCENARIOS_PARAM}" | jq -c '.[]'); do
    var=$(jq -r '.key' <<< "$entry");
    val=$(jq -r '.value' <<< "$entry");
    export "$var=$val";
  done
fi

echo "***********************************"
echo "Launching with variables : "
echo "TEMPLATE=${TEMPLATE}"
echo "SCENARIOS_PARAM=${SCENARIOS_PARAM}"
echo "SKETCH=${SKETCH}"
for var_name in HYBRID_MONO_NAMESPACE HYBRID HYBRID_KUBEVIRT KAMAJI BYOH BYOH_IN_POOL BYOH_CP REDFISH MULTI_CLUSTER ALL_ENGINES VAULT TPM MULTI_TENANT BROKER VIRTUALMEDIA OPERATOR PIVOTING SWITCH UPGRADING SCALING AUTOSCALING IPAM RUNNER_VM PATH BROKER_AUTH RKE2 TEMPLATE OS_DEBUG no_proxy; do
  if [[ -v "${var_name}" ]]; then
    echo ${var_name} = ${!var_name}
  else
    export "${var_name}=0"
  fi
done
echo "***********************************"
echo
cp "${TEMPLATE}" params.yml

if test -f "${BASE_DIR}/ci-script-custom.sh"; then
  # shellcheck disable=SC1090,SC1091
  source "${BASE_DIR}/ci-script-custom.sh"
fi


if [ "${ALL_ENGINES}" == 1 ]; then
  echo "** Scenario engine with all os version **"
  yq e '.ubuntu = true' -i params.yml
  yq e '.centos = true' -i params.yml
  yq e '.opensuse = false' -i params.yml
  yq e '.service_size = "40G"' -i params.yml
  echo "*  Installing pip/jinja"
  python3 -m pip install --upgrade pip
  python3 -m pip install jinja2-cli
fi

if [ "${VAULT}" == 1 ]; then
  echo '** WITH VAULT MODE **'
  yq e '.vault.enabled = true' -i params.yml
fi

if [ "${TPM}" == 1 ]; then
  echo '** WITH TPM MODE **'
  yq e '.vault.enabled = true' -i params.yml
  yq e '.vault.tpm = true' -i params.yml
fi

if [ "${MULTI_TENANT}" == 1 ]; then
  echo '** WITH MULTI_TENANT MODE **'
  yq e '.mode_multi_tenant = true' -i params.yml
fi

if [ "${BROKER}" == 1 ]; then
  echo '** WITH BROKER MODE **'
  yq e '.bmc_protocol = "redfish"' -i params.yml
  yq e '.redfish_broker.broker_enabled = true' -i params.yml
  if [ "${BROKER_AUTH}" == 1 ]; then
    echo '** WITH BROKER_AUTH MODE **'
    yq e ".redfish_broker.broker_auth = true" -i params.yml
  fi
else
  echo '** NO BROKER MODE **'
  yq e '.bmc_protocol = "ipmi"' -i params.yml
  yq e '.redfish_broker.broker_enabled = false' -i params.yml
fi

if [ "${OPERATOR}" == 1 ] || [ "${PIVOTING}" == 1 ]; then
  echo '** WITH OPERATOR MODE **'
  yq e '.operator = true' -i params.yml;
else
  echo '** WITH STACK MODE **'
  yq e '.operator = false' -i params.yml
fi

if [ "${PIVOTING}" == 1 ]; then
  echo '** WITH PIVOT CLUSTER MODE **'
  yq e '.minion_size = "14G"' -i params.yml
  yq e '.pivot_enabled = true' -i params.yml;
fi

if [ "${SWITCH}" == 1 ]; then
  echo '** SWITCH MODE **'
  yq e '.switch = true' -i params.yml
fi

if [ "${IPAM}" == 1 ]; then
  echo '** IPAM MODE **'
  yq e '.ipam = true' -i params.yml
fi

if [ "${SCALING}" == 1 ]; then
  echo '** WITH SCALE CLUSTER MODE **'
  yq e '.minions = 4' -i params.yml
  yq e '.scaling_enabled = true' -i params.yml
fi

if [ "${UPGRADING}" == 1 ]; then
  echo '** WITH UPGRADE CLUSTER MODE **'
  yq e '.ha = true' -i params.yml
  yq e '.minions = 6' -i params.yml
  yq e '.k8s_upgrade.k8s_upgrade_enabled = true' -i params.yml
fi

if [ "${AUTOSCALING}" == 1 ] && [ "${BROKER}" == 1 ]; then
  echo '** AUTOSCALING ENABLED **'
  yq e '.autoscale = true' -i params.yml
  yq e '.minions = 7' -i params.yml
fi

if [ "${RKE2}" == 1 ]; then
  echo '** WITH RKE2 CLUSTER MODE **'
  yq e '.minion_size = "14G"' -i params.yml
  yq e '.rke2 = true' -i params.yml
fi

if [ "${MULTI_CLUSTER}" == 1 ]; then
  echo '** WITH MULTI CLUSTER MODE **'
  yq e '.minions = 6' -i params.yml
  yq e '.mode_multi_cluster = true' -i params.yml
fi

if [ "${OS_DEBUG}" == 1 ]; then
  echo '** WITH OS_DEBUG MODE **'
  yq e '.os_debug = true' -i params.yml
fi

if [ "${REDFISH}" == 1 ]; then
  echo '** WITH REDFISH PROTOCOL **'
  yq e '.bmc_protocol = "redfish"' -i params.yml
fi

if [ "${VIRTUALMEDIA}" == 1 ]; then
  echo '** WITH VIRTUALMEDIA MODE **'
  yq e '.bmc_protocol = "redfish-virtualmedia"' -i params.yml
fi

if [ "${KAMAJI}" == 1 ]; then
  echo '** WITH KAMAJI PROVIDER **'
  yq e '.kamaji.enabled = true' -i params.yml
fi

if [ "${BYOH}" == 1 ]; then
  echo '** WITH BYOH PROVIDER **'
  yq e '.byoh_provider.enabled = true' -i params.yml
  yq e '.byoh_provider.nb_host = 2' -i params.yml
  yq e '.byoh_provider.worker = "true"' -i params.yml
fi

if [ "${BYOH_IN_POOL}" == 1 ]; then
  echo '** WITH BYOH IN POOL **'
  yq e '.byoh_provider.byoh_in_pool = true' -i params.yml
fi

if [ "${BYOH_CP}" == 1 ]; then
  echo '** WITH BYOH for controlplane **'
  yq e '.byoh_provider.cp = true' -i params.yml
fi

if [ "${HYBRID}" == 1 ]; then
  echo '** WITH hybrid mode **'
  yq e '.hybrid.enabled = true' -i params.yml

  if [ "${HYBRID_KUBEVIRT}" == 1 ]; then
    echo '** WITH hybrid mode and kubevirt **'
    yq e '.hybrid.kubevirt = true' -i params.yml
  fi
  
  if [ "${HYBRID_MONO_NAMESPACE}" == 1 ]; then
    echo '** WITH hybrid mode and mono_namespace **'
    yq e '.hybrid.mono_namespace = true' -i params.yml
  fi
fi



echo "***********************************"
echo '---- params.yml -----'
cat params.yml
echo '---------------------'
echo "***********************************"
echo


if [ "${TEMPLATE_ONLY:-0}" == "1" ]; then
  ./00-mk-templates.sh params.yml
  exit
fi

if [ "${1:-0}" != "0" ]; then
  ./00-mk-templates.sh params.yml
fi

if [ "${ALL_ENGINES}" == 1 ]; then
  echo "running ./60-lcm-test.sh "
  ./60-lcm-test.sh -f "${1:-0}"
else
  echo "running ./100-full.sh -f ${1:-0}"
  ./100-full.sh -f "${1:-0}"
fi

if [ "${VAULT}" == 1 ]; then
  echo '** try login on controlplane **'
  password="$(yq '.kv1[] | select(.path == "cluster1/admin").value.password' "${BASE_DIR}/templates/vault-init.yaml")"
  sshpass "-p$password" ssh  -o PreferredAuthentications=password -o PubkeyAuthentication=no -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"  admin@192.168.133.200 ls
fi

if [ "${SWITCH}" == 1 ]; then
  # kill ssh process launched in 04-minions.sh script
  echo "killing ssh process:"
  pgrep -x -a -f "ssh -o PasswordAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -D 22300 -f -C -q -N admin@192.168.133.2"
  pkill -f -x "ssh -o PasswordAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -D 22300 -f -C -q -N admin@192.168.133.2"
fi

exit $?

