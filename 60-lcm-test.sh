#!/bin/bash

#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu


############ Main script ################
from=0

while getopts "hdf:?" opt; do
    case "$opt" in
    h|\?)
        echo "$0 [-h][-f n]"
        echo '  -h this help'
        echo '  -f n starts from step n.'
        exit 0
        ;;
    f)
        from="${OPTARG}"
        ;;
    d)
        KANOD_DEPLOYED=1
        ;;
    esac
done

shift $(( OPTIND - 1))

ROOT_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")
export ROOT_DIR

params="${1:-params.yml}"
echo "Using parameters file ${params}"

if [ -v no_proxy ]; then
    no_proxy=192.168.133.0/24,.kanod.home.arpa,$no_proxy
fi

if [ -v NO_PROXY ]; then
    NO_PROXY=192.168.133.0/24,.kanod.home.arpa,$NO_PROXY
fi

export PATH="${HOME}/.local/bin:$PATH"

if [ "${KANOD_DEPLOYED:-0}" == '0' ]; then
    if [ "$from " -le 0 ]; then
        echo '*** Template generation'
        ./00-mk-templates.sh "${@}"
    fi
    if [ "$from " -le 1 ]; then
        echo '*** Base projects'
        ./01-basic-projects.sh
    fi
    if [ "$from " -le 2 ]; then
        echo '*** Networks'
        ./02-mk-network.sh
    fi
    if [ "$from " -le 3 ]; then
        echo '*** Stores'
        ./03-mk-store.sh
    fi
    if [ "$from " -le 4 ]; then
        echo '*** Configuration'
        # We change the nexus target
        set -a
        # shellcheck disable=SC1090,SC1091
        source "${KIAB_CONF:-${HOME}/.kiab_conf}"
        set +a

        echo -e "$(yq '.components | to_entries | map(.key + "=" + .value) | join("\n")' "${ROOT_DIR}/templates/engine-versions.yaml" )" > "${LAB_DIR}/service/manifests/workflow/versions.env"
        for section in ubuntu centos opensuse rke2 internalized; do
            yq -o json ".${section}" "${ROOT_DIR}/templates/engine-versions.yaml" > "${LAB_DIR}/service/manifests/workflow/${section}.json"
        done
        yq e -o json '."k8s_versions" | map({"release": .})' "${ROOT_DIR}/templates/engine-versions.yaml" > "${LAB_DIR}/service/manifests/workflow/k8s-release.json"


        echo '*** service'
        ./06-launch-service.sh -s || true
    fi
else
    echo '*** Configuration'
    set -a
    # shellcheck disable=SC1090,SC1091
    source "${KIAB_CONF:-${HOME}/.kiab_conf}"
    set +a
fi

mkdir -p "${LAB_DIR}/lcm_logs"

# We do not deploy the stack
yq e 'del(.kubernetes.manifests)' -i "${LAB_DIR}/lcm/config.yaml"
yq e 'del(.kubernetes.report)' -i "${LAB_DIR}/lcm/config.yaml"


# Get all the suitable OS image - exclude debug versions.
mapfile -t images <<< "$(curl -k -L \
    'https://maven.service.kanod.home.arpa/service/rest/v1/search/assets/?repository=kanod&group=kanod&maven.extension=qcow2' | \
    yq e '.items[].maven2.artifactId' - | grep '.*-v1\..*' | grep -v debug | sort -u)"

# Initialize report
echo '' > "${LAB_DIR}/result"
echo '---' > "${LAB_DIR}/result.yml"


failure() {
    echo "Scenario failed: ${os} - ${k8s} - ${engine} : $1" >> "${LAB_DIR}/result"
    yq e ".test.\"${os}\".\"${k8s}\".${engine} = false" -i "${LAB_DIR}/result.yml"
}

if [ "${#images[@]}" -eq 1 ] && [ "${images[0]}" == '' ]; then
    echo "No Image has been found. Abort."
    exit 1
fi

echo "Processing images : ${images[*]}"

for i in "${images[@]}"; do
    echo "*** Image $i"
    # shellcheck disable=SC2001
    k8s=$(sed 's/.*-\(v1\.[0-9][0-9]*\.[0-9][0-9]*\)/\1/' <<< "$i")
    # shellcheck disable=SC2001
    os=$(sed 's/\(.*\)-\(v1\.[0-9][0-9]*\.[0-9][0-9]*\)/\1/' <<< "$i")
    OS="$os" yq e '.os=strenv(OS)' -i "${LAB_DIR}/lcm/config.yaml"
    K8S="$k8s" yq e '.kubernetes.version = strenv(K8S)' -i "${LAB_DIR}/lcm/config.yaml"
    version=$(yq '.components.kanod-node' "${ROOT_DIR}/templates/engine-versions.yaml")
    VERSION=$version yq e '.version = strenv(VERSION)' -i "${LAB_DIR}/lcm/config.yaml"

    # update default route for centos
    if [[ ${os} =~ "-stream" ]]; then
        yq '.network.bridges.br0.routes[0].to = "0.0.0.0/0"' -i "${LAB_DIR}/lcm/config.yaml"
    fi

    yq e ".k8s.\"${k8s}\" = true" -i "${LAB_DIR}/result.yml"
    yq e ".os.\"${os}\" = true" -i "${LAB_DIR}/result.yml"
    # Test only containerd configuration of engines.
    engine="containerd"
    echo "*** Scenario $os $k8s $engine"
    ENGINE="${engine}" yq e '.kubernetes.container_engine = strenv(ENGINE)' -i "${LAB_DIR}/lcm/config.yaml"
    if ! ./07-launch-lcm.sh -s; then
        failure 'deployment'
        continue
    fi
    if [ ! -f "${LAB_DIR}/kubeconfig/lcm.kubeconfig" ] ; then
        failure "No kubeconfig file for lcm (${LAB_DIR}/kubeconfig/lcm.kubeconfig)"
        continue
    fi
    if ! status="$(kubectl --kubeconfig "${LAB_DIR}/kubeconfig/lcm.kubeconfig" get node -o wide)"; then
        failure 'Node status'
    fi
    while ! grep -wq 'Ready' <<< "$status"; do
        echo -n '+'
        sleep 5
        if ! status="$(kubectl --kubeconfig "${LAB_DIR}/kubeconfig/lcm.kubeconfig" get node -o wide)"; then
            echo 'connection problem'
        fi
    done
    echo "$status" >> "${LAB_DIR}/result"
    yq e ".test.\"${os}\".\"${k8s}\".${engine} = true" -i "${LAB_DIR}/result.yml"
    ssh -o "PasswordAuthentication=no" \
            -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
            admin@192.168.133.10 sudo cat /var/log/cloud-init-output.log > "${LAB_DIR}/lcm_logs/${os}_${k8s}_${engine}.cloud-init.log"
done

if type jinja2 &> /dev/null; then
    echo "Generating ${LAB_DIR}/result.html file"
    jinja2 --format yaml "${ROOT_DIR}/templates/engines.j2" "${LAB_DIR}/result.yml" > "${LAB_DIR}/result.html"
fi

