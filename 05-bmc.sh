#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

if [ "${BMC_PROTOCOL}" = "ipmi" ]; then
  vbmc_config="${LAB_DIR}/vbmc"

  if [ -f "${vbmc_config}/vbmc.pid" ]; then
    pid="$(cat "${vbmc_config}/vbmc.pid")"
    if [ "$pid" != '0' ]; then
      echo "Killing $pid"
      kill -9 "$pid" || true
      rm "${vbmc_config}/vbmc.pid"
    fi
  fi

  rm -rf "$vbmc_config"
  mkdir -p "$vbmc_config"
  cat > "$vbmc_config/virtualbmc.conf" <<EOF
[default]
config_dir: ${vbmc_config}
pid_file: ${vbmc_config}/vbmc.pid
server_port: 51000
[log]
logfile: ${vbmc_config}/log.txt
EOF

  echo 'Launching vbmc'
  # Why vbmc is using pyperclip ?
  export XDG_SESSION_TYPE="${XDG_SESSION_TYPE:-x11}"
  export VIRTUALBMC_CONFIG="$vbmc_config/virtualbmc.conf"
  vbmcd

  while ! vbmc list &> /dev/null; do
    echo -n .
    sleep 1
  done

  for ((i=1;i<=NB_VM;i++)) do
      port=$((5000+i))
      domain="vmok-$i"
      vbmc add --username root --password "${BMC_PASSWORD}" --port "${port}" "${domain}"
      vbmc start "${domain}"
  done

elif [ "${BMC_PROTOCOL}" = "redfish" ] || [ "${BMC_PROTOCOL}" = "redfish-virtualmedia" ]; then
  echo 'Launching Virtual Redfish BMC'

  vbmcredfish_config="${LAB_DIR}/vbmcredfish"

  for ((i=1;i<=NB_VM;i++)); do
    if [ -f "${vbmcredfish_config}/vbmcredfish-${i}.pid" ]; then
      pid="$(cat "${vbmcredfish_config}/vbmcredfish-${i}.pid")"
      if [ "$pid" != '0' ]; then
        echo "Stopping sushy-emulator with pid ${pid}"
        kill -9 "$pid" >& /dev/null || true
        rm -f "${vbmcredfish_config}/vbmcredfish-${i}.pid"
      fi
    fi
  done

  set +e
  pidlist=$(mktemp)
  pgrep -a sushy-emulator  > "${pidlist}"
  if [ -s "${pidlist}" ]; then
    echo "!!! WARNING !!!"
    echo "   Remaining sushy-emulator processus:"
    cat "${pidlist}"
  fi
  rm "${pidlist}"
  set -e

  rm -rf "${vbmcredfish_config}"
  mkdir -p "$vbmcredfish_config"

  openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
      -keyout  "${vbmcredfish_config}"/key.pem \
      -out "${vbmcredfish_config}"/cert.pem \
      -addext "subjectAltName = IP:${BMC_HOST}" \
      -subj "/C=--/L=-/O=-/CN=${BMC_HOST}" &> /dev/null

  for ((i=1;i<=NB_VM;i++)); do
    port=$((5000+i))
    vmName="vmok-$i"
    allowedInstances=$(virsh list --all --name  --uuid | awk -v name=${vmName} '$2==name {print $2 "," $1}')
    cat > "$vbmcredfish_config/emulator-${i}.conf" <<EOF
SUSHY_EMULATOR_LISTEN_IP = "${BMC_HOST}"
SUSHY_EMULATOR_LISTEN_PORT = "${port}"
SUSHY_EMULATOR_SSL_CERT = "${vbmcredfish_config}/cert.pem"
SUSHY_EMULATOR_SSL_KEY = "${vbmcredfish_config}/key.pem"
SUSHY_EMULATOR_AUTH_FILE = "${vbmcredfish_config}/htpasswd-${i}.txt"
SUSHY_EMULATOR_LIBVIRT_URI = u"qemu:///system"
SUSHY_EMULATOR_ALLOWED_INSTANCES = "${allowedInstances}"
EOF

    htpasswd -cbB "$vbmcredfish_config"/htpasswd-"${i}".txt root "${BMC_PASSWORD}" &> /dev/null

    if [ "${KANOD_SWITCH:-0}" = 1 ]; then
      echo "Launching virtual redfish bmc on ${BMC_HOST}:${port} for ${vmName} with proxy through switch"
      no_proxy=192.168.133.10,192.168.133.0/24,kanod.home.arpa,192.168.133.12 http_proxy=socks5://localhost:22300 https_proxy=socks5://localhost:22300 SUSHY_EMULATOR_CONFIG="${ROOT_DIR}/redfish/sushy_extension.py" sushy-emulator --config "${vbmcredfish_config}"/emulator-"${i}".conf &> "${vbmcredfish_config}"/sushy-"${i}".log &
    else
      echo "Launching virtual redfish bmc on ${BMC_HOST}:${port} for ${vmName}"
      SUSHY_EMULATOR_CONFIG="${ROOT_DIR}/redfish/sushy_extension.py" sushy-emulator --config "${vbmcredfish_config}"/emulator-"${i}".conf &> "${vbmcredfish_config}"/sushy-"${i}".log &
    fi
    # shellcheck disable=SC2181
    if [ $? -eq 0 ]; then
        echo $! > "${vbmcredfish_config}/vbmcredfish-${i}.pid";
    else
        echo "Error during sushy-emulator launch"
        exit 1
    fi
  done
else
    echo "unknown protocol for bmc : ${BMC_PROTOCOL}"
    exit 1
fi

if [ "$BMC_PROTOCOL" == "redfish-virtualmedia" ]; then
    # shellcheck disable=SC2076
    if ! [[ "$(virsh pool-list --name)" =~ "default" ]]; then
      echo "Create default libvirt pool for ironic with redfish-virtualmedia"
      sudo mkdir -p /var/lib/libvirt/images
      virsh pool-define-as --type dir --name default --target /var/lib/libvirt/images
      virsh pool-start default
      virsh pool-autostart default
    fi
fi
