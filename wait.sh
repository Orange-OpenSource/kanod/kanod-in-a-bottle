#!/bin/bash

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

function get_broker_logs() {
    mkdir -p "${LAB_DIR}/broker_logs/pods"
    mkdir -p "${LAB_DIR}/broker_logs/containers"
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.13 sudo tar -czvhf - -C /var/log/containers . | tar -C "${LAB_DIR}/broker_logs/containers" -xzvf - .
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.13 sudo tar -czvhf - -C /var/log/pods . | tar -C "${LAB_DIR}/broker_logs/pods" -xzvf - .
}

function get_lcm_logs() {
    mkdir -p "${LAB_DIR}/lcm_logs/pods"
    mkdir -p "${LAB_DIR}/lcm_logs/containers"
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.10 sudo tar -czvhf - -C /var/log/containers . | tar -C "${LAB_DIR}/lcm_logs/containers" -xzvf - .
    ssh -o "PasswordAuthentication=no" \
    -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
    admin@192.168.133.10 sudo tar -czvhf - -C /var/log/pods . | tar -C "${LAB_DIR}/lcm_logs/pods" -xzvf - .
    if [ "${REDFISH_BROKER:-}" == 1 ]; then
        get_broker_logs
    fi
    if [ "${KANOD_SWITCH:0}" == 1 ]; then
        mkdir -p "${LAB_DIR}/switch_logs"
        scp -q -o "PasswordAuthentication=no" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
        admin@192.168.133.2:/var/log/kanod-config.log "${LAB_DIR}/switch_logs/." 2> /dev/null || true
        scp -q -o "PasswordAuthentication=no" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
        admin@192.168.133.2:/var/log/dhcrelay*.log "${LAB_DIR}/switch_logs/." 2> /dev/null || true
    fi
}

export LIGHT_VIEW=${LIGHT_VIEW:-0}

if [ "$KANOD_SWITCH" = 1 ]; then
    export http_proxy=socks5://localhost:22300
    export https_proxy=socks5://localhost:22300
    export HTTP_PROXY=$http_proxy
    export HTTPS_PROXY=$https_proxy
fi

if [ "$MULTI_CLUSTER" == "1" ]; then
    CLUSTER_LIST="cluster1 cluster2"
else
    CLUSTER_LIST="cluster1"
fi

for CLUSTER_NAME in ${CLUSTER_LIST}; do
    CLUSTER_GIT_DIR="${CLUSTER_NAME}"

    multi_tenant=$(yq  '.spec.multiTenant // "false"' "${LAB_DIR}/meta/${CLUSTER_NAME}.yaml")
    cdef_name=$(yq '.metadata.name' "${LAB_DIR}/meta/${CLUSTER_NAME}.yaml")
    cdef_namespace=$(yq '.metadata.namespace' "${LAB_DIR}/meta/${CLUSTER_NAME}.yaml")
    if [ "${multi_tenant}" == "true" ]; then
        CLUSTER_NS="${cdef_namespace}-${cdef_name}"
    else
        CLUSTER_NS="${cdef_namespace}"
    fi


    kc1=${LAB_DIR}/kubeconfig/lcm.kubeconfig
    kc2=${LAB_DIR}/kubeconfig/${CLUSTER_NAME}.kubeconfig

    echo "Start at : $(date)"

    echo "Waiting for kubeconfig for target cluster ${CLUSTER_NAME}"
    count=0
    while ! kc2raw=$(kubectl --kubeconfig "${kc1}" get "secret/${CLUSTER_NAME}-kubeconfig" -n "${CLUSTER_NS}" -o jsonpath="{ .data.value }" 2> /dev/null) ; do
        count=$((count+1))
        if [ "$count" -gt 400 ]; then
            echo 'Timout waiting for kubeconfig'
            get_lcm_logs
            echo '=== Partial Deployment ==='
            ./view-deployment.sh
            exit 1
        fi
        if [ "${VERBOSE:-0}" = 1 ] && ! (( count % 6 )); then
            clear
            kubectl --kubeconfig "${kc1}" get pod -A
        fi
        sleep 5
        echo -n '.'
    done
    base64 -d > "${kc2}" <<< "$kc2raw"
    nbCp=$(yq e '.controlPlane.replicas' "${LAB_DIR}/${CLUSTER_GIT_DIR}/config.yaml")
    if [ "${KAMAJI:-0}" = "1" ]; then
        nbCp=0
        echo
        echo "Using Kamaji : no controlplane node for target cluster ${CLUSTER_NAME}"
    fi

    # The default value takes into account autoscaling machine deployment default.
    nbWorkers=$(yq e '.workers.[].replicas // 1' "${LAB_DIR}/${CLUSTER_GIT_DIR}/config.yaml")
    version=$(yq e '.version' "${LAB_DIR}/${CLUSTER_GIT_DIR}/config.yaml")
    nbNodes=0
    # It is possible to override the number of workers expected with NB_WORKERS
    for nb in $nbCp ${NB_WORKERS:-$nbWorkers}; do
        (( nbNodes=nbNodes+nb ))
    done

    echo
    echo "Waiting for $nbNodes nodes ready for target cluster ${CLUSTER_NAME}"
    mkdir -p "${LAB_DIR}/logs"
    count=0
    # We filter nodes with the right version that are in Ready state.
    # We compare with the sum of controlplane and workers. This will only work for a single cluster with a single MD
    while [ "$(kubectl --kubeconfig "${kc2}" get nodes 2> /dev/null | grep "$version" | grep -v NotReady | grep -c Ready)" != "${nbNodes}" ]; do
        count=$((count+1))
        if [ "$count" -gt 300 ]; then
            echo 'Timout waiting for nodes'
            get_lcm_logs
            echo '=== Partial Deployment ==='
            ./view-deployment.sh -ln
            exit 1
        fi
        if [ "${VERBOSE:-0}" = 1 ]; then
            clear
            ./view-deployment.sh
        fi
        printf "\n\n------- %s --------\n" "$(date)" >> "${LAB_DIR}/logs/deployment.log" 2>&1
        ./view-deployment.sh -l >> "${LAB_DIR}/logs/deployment.log" 2>&1
        sleep 10
        echo -n '.'
    done
done

if [ "${VERBOSE:-0}" = 1 ]; then
    clear
    ./view-deployment.sh
fi
echo
echo "End at : $(date)"


