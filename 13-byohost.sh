#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

# shellcheck disable=SC1090,SC1091
source "${ROOT_DIR}/lib_sync.sh"
export synchronized=1
temp=$(mktemp -d)

if [ "${BYOH}" != 1 ]; then
  echo "byoh provider not used"
  exit
fi

if [ "${BYOH_IN_POOL}" == 1 ]; then
  echo "byoh in pool used"
  exit
fi

kc1=${LAB_DIR}/kubeconfig/lcm.kubeconfig

# shellcheck disable=SC1090,SC1091
source "${ROOT_DIR}/lib_sync.sh"
export synchronized=1

echo "Waiting for namespace byoh-system to be available"
while ! kubectl --kubeconfig "${kc1}" get ns byoh-system >& /dev/null ; do  echo -n . ; sleep 5 ; done ;
echo; echo "namespace byoh-system is available"
sleep 10

echo "Waiting for byoh-controller-manager deployment to be ready"
kubectl --kubeconfig "${kc1}" wait --for condition=Available --timeout 600s -n byoh-system deployment byoh-controller-manager


echo "Waiting for ${TARGET_NAMESPACE} namespace to be ready"
while ! kubectl --kubeconfig "${kc1}" get ns "${TARGET_NAMESPACE}" >& /dev/null ; do  echo -n . ; sleep 5 ; done ;
echo; echo "namespace ${TARGET_NAMESPACE} is available"

# Create vm for byoh
i=1
while [ "$i" -le "${NB_BYOHOST}" ]
do
    echo "prepare enroll data for byohost-${i} "

    # Get bootstrap-kubeconfig for each byoh vm
    APISERVER=$(kubectl --kubeconfig "${kc1}" config view -ojsonpath='{.clusters[0].cluster.server}')
    CA_CERT=$(kubectl --kubeconfig "${kc1}" config view --flatten -ojsonpath='{.clusters[0].cluster.certificate-authority-data}')

    cat <<EOF > "${temp}/BootstrapKubeconfig-${i}.yaml"
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: BootstrapKubeconfig
metadata:
  name: bootstrap-kubeconfig-${i}
  namespace: default
spec:
  apiserver: "$APISERVER"
  certificate-authority-data: "$CA_CERT"
EOF

    kubectl --kubeconfig "${kc1}" apply -f "${temp}/BootstrapKubeconfig-${i}.yaml"
    sleep 10
    kubectl --kubeconfig "${kc1}" get bootstrapkubeconfig bootstrap-kubeconfig-"${i}" -n default -o=jsonpath='{.status.bootstrapKubeconfigData}' > "${temp}/bootstrap-kubeconfig-${i}.conf"

    bkdata=$(base64 < "${temp}/bootstrap-kubeconfig-${i}.conf")
    DATA=${bkdata} yq e '.byoh.bootstrap_kubeconfig = env(DATA)' -i "${LAB_DIR}/byohost-${i}/config.yaml"
    echo "Launching byohost-${i}"
    kanod-vm -c "${LAB_DIR}/byohost-${i}" -v
    ((i=i+1))
done

i=1
while [ "$i" -le "${NB_BYOHOST}" ]
do
    echo "waiting for byohost-${i}"
    ((addr_id=120+i))
    address_host="192.168.133.${addr_id}"
    sync_status "byohost-${i}" "${address_host}"
    ((i=i+1))
done


