#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_CONF:-${HOME}/.kiab_conf}"
set +a

if [ "$MULTI_CLUSTER" == "1" ]; then
    CLUSTER_LIST="cluster1 cluster2"
else
    CLUSTER_LIST="cluster1"
fi

CLUSTER_NAME="${CLUSTER_NAME:-cluster1}"
lcm_info=${LCM_INFO:-0}
nodes_info=${NODES_INFO:-0}
broker_info=${BROKER_INFO:-0}
target_info=${TARGET_INFO:-0}

while getopts "hntlbc?" opt; do
    case "$opt" in
    h|\?)
        echo "$0 [-hntlcb]"
        echo '  -h this help'
        echo '  -l display lcm cluster info'
        echo '  -n display nodes info'
        echo '  -t display target cluster info'
        echo '  -b display broker info'
        echo '  -c display less info'
        exit 0
        ;;
    l)
        lcm_info=1
        ;;
    n)
        nodes_info=1
        ;;
    t)
        target_info=1
        ;;
    b)
        broker_info=1
        ;;
    c)
	    LIGHT_VIEW=1
    esac
done

if [ "$lcm_info" == 0 ] && [ "$nodes_info" == 0 ]  && [ "$target_info" == 0 ]  && [ "$broker_info" == 0 ] ; then
    lcm_info=1
fi

kc_lcm="${LAB_DIR}/kubeconfig/lcm.kubeconfig"
kc_cluster="${LAB_DIR}/kubeconfig/${CLUSTER_NAME}.kubeconfig"
kc_broker="${LAB_DIR}/kubeconfig/broker.kubeconfig"
if [ "$lcm_info" == 1 ]; then
    echo
        if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo
        echo '-------------------------------';
    fi
    echo '-------- LCM CLUSTER ----------';
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo '-------- cluster --------------';
        kubectl --kubeconfig "${kc_lcm}" get -A cluster
        if [ "${KAMAJI:-0}" = "1" ]; then
            echo '-------- KamajiControlPlane ------------------';
            kubectl --kubeconfig "${kc_lcm}" get -A KamajiControlPlane
            echo '-------- tenantcontrolplanes --------------';
            kubectl --kubeconfig "${kc_lcm}" get -A tenantcontrolplanes
            echo '-------- deploy --------------';
            kubectl --kubeconfig "${kc_lcm}" get deploy -l "kamaji.clastix.io/project=kamaji" -A -o wide
        else
            echo '-------- kcp ------------------';
            kubectl --kubeconfig "${kc_lcm}" get -A kcp
        fi
        echo '-------- md -------------------';
        kubectl --kubeconfig "${kc_lcm}" get -A md
        echo '-------- machineset -------------------';
        kubectl --kubeconfig "${kc_lcm}" get -A machineset
        echo '-------- m3m ------------------';
        kubectl --kubeconfig "${kc_lcm}" get -A m3m
        if [ "${HYBRID:-}" == 1 ]; then
            echo '-------- Host --------------';
            kubectl --kubeconfig "${kc_lcm}" get -A Host -o wide
            echo '-------- HostQuota --------------';
            kubectl --kubeconfig "${kc_lcm}" get -A HostQuota -o wide
        fi
        if [ "${BYOH:-0}" == 1 ]; then
            for rsrc in byocluster byomachine byohosts ; do
                echo "-------- ${rsrc} --------------"
                kubectl --kubeconfig "${kc_lcm}" get ${rsrc} -A -o wide
            done
        fi
    fi
    echo '-------- machine --------------';
    kubectl --kubeconfig "${kc_lcm}" get -A machine
    if [ "${BYOH_IN_POOL:-0}" = "1" ]; then
        echo '-------- byohostpoolscaler ------------------';
        kubectl --kubeconfig "${kc_lcm}" get -A byohostpoolscaler
        echo '-------- byohostpool ------------------';
        kubectl --kubeconfig "${kc_lcm}" get -A byohostpool
    fi
    if [ "${KUBEVIRT:-}" == 1 ]; then
        echo '-------- VirtualMachine --------------';
        kubectl --kubeconfig "${kc_lcm}" get -A VirtualMachine -o wide
        echo '-------- VirtualMachineInstance --------------';
        kubectl --kubeconfig "${kc_lcm}" get -A VirtualMachineInstance -o wide
    fi
    echo '-------- bmh ------------------';
    kubectl --kubeconfig "${kc_lcm}" get -A bmh -o wide
    echo '----- Argo CD Application -----';
    kubectl --kubeconfig "${kc_lcm}" get -A Application -o wide
    echo '-------- ClusterDef -----------';
    kubectl --kubeconfig "${kc_lcm}" get -A ClusterDef -o wide
    if [ "${REDFISH_BROKER:-}" == 1 ]; then
        echo '-------- baremetalpool --------';
        kubectl --kubeconfig "${kc_lcm}" get -A Baremetalpool
        if [ "${LIGHT_VIEW:-0}" = "0" ]; then
            if [ "${KANOD_SWITCH}" == 1 ]; then
                echo '-------- network --------------';
                kubectl --kubeconfig "${kc_lcm}" get -A Network
            fi
        fi
    fi
fi


if [ "${REDFISH_BROKER:-}" == 1 ] && [ "$broker_info" == 1 ]; then
    echo ;
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo
        echo '-------------------------------';
    fi
    echo '-------- BROKER ---------------';
    echo '-------- baremetaldef ---------';
    kubectl --kubeconfig "${kc_broker}" get -A baremetaldef
    echo '-------- pooldef --------------';
    kubectl --kubeconfig "${kc_broker}" get -A pooldef
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo '-------- pooluser --------------';
        kubectl --kubeconfig "${kc_broker}" get -A pooluser
        if [ "${KANOD_SWITCH}" == 1 ]; then
            echo '-------- switch ---------------';
            kubectl --kubeconfig "${kc_broker}" get -A switch
            echo '-------- networkattachment ----';
            kubectl --kubeconfig "${kc_broker}" get -A networkattachment
            echo '-------- networkdefinition ----';
            kubectl --kubeconfig "${kc_broker}" get -A networkdefinition
        fi
    fi
fi

# use socks5 proxy to access target cluster with switch scenario
if [ "$KANOD_SWITCH" = 1 ]; then
    export http_proxy=socks5://127.0.0.1:22300
    export https_proxy=socks5://127.0.0.1:22300
    export HTTP_PROXY=$http_proxy
    export HTTPS_PROXY=$https_proxy
fi


if [ "$nodes_info" == 1 ]; then
    echo
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo
        echo '-----------------------------------------------';
    fi
    for cluster in ${CLUSTER_LIST}; do
        kubeconfig_cluster="${LAB_DIR}/kubeconfig/${cluster}.kubeconfig"
        echo "-------- NODES (for ${cluster}) -----------";
        kubectl --kubeconfig "${kubeconfig_cluster}" get node -o wide
    done

fi

if [ "$target_info" == 1 ]; then
    echo
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo
        echo '-------------------------------';
    fi
    echo "-------- TARGET CLUSTER (${CLUSTER_NAME}) -------";
    if [ "${LIGHT_VIEW:-0}" = "0" ]; then
        echo '-------- cluster --------------';
        kubectl --kubeconfig "${kc_cluster}" get -A cluster
        echo '-------- kcp ------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A kcp
        echo '-------- md -------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A md
        echo '-------- machineset -------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A machineset
        echo '-------- m3m ------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A m3m
        if [ "${HYBRID:-}" == 1 ]; then
            echo '-------- Host --------------';
            kubectl --kubeconfig "${kc_cluster}" get -A Host -o wide
            echo '-------- HostQuota --------------';
            kubectl --kubeconfig "${kc_cluster}" get -A HostQuota -o wide
        fi
        if [ "${BYOH:-0}" == 1 ]; then
            for rsrc in byocluster byomachine byohosts ; do
                echo "-------- ${rsrc} --------------"
                kubectl --kubeconfig "${kc_cluster}" get ${rsrc} -A -o wide
            done
        fi
    fi
    echo '-------- machine --------------';
    kubectl --kubeconfig "${kc_cluster}" get -A machine 2> /dev/null
    if [ "${BYOH_IN_POOL:-0}" = "1" ]; then
        echo '-------- byohostpoolscaler ------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A byohostpoolscaler
        echo '-------- byohostpool ------------------';
        kubectl --kubeconfig "${kc_cluster}" get -A byohostpool
    fi
    echo '-------- bmh ------------------';
    kubectl --kubeconfig "${kc_cluster}" get -A bmh -o wide 2> /dev/null
    echo
    if [ "${REDFISH_BROKER:-}" == 1 ]; then
        echo '-------- baremetalpool --------';
        kubectl --kubeconfig "${kc_cluster}" get -A baremetalpool 2> /dev/null
        if [ "${LIGHT_VIEW:-0}" = "0" ]; then
            if [ "${KANOD_SWITCH}" == 1 ]; then
                echo '-------- network --------------';
                kubectl --kubeconfig "${kc_cluster}" get -A Network
            fi
        fi
    fi
    echo "-------- NODES (for ${CLUSTER_NAME}) ----------";
    kubectl --kubeconfig "${kc_cluster}" get node -o wide 2> /dev/null
fi
echo
